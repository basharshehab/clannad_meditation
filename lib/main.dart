import 'package:clannad/app/data/locale/app_locale.dart';
import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/durations.dart';
import 'package:clannad/app/services/caching_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'app/routes/app_pages.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await GetStorage.init();
  final FirebaseFirestore db = FirebaseFirestore.instance;
  db.settings = const Settings(persistenceEnabled: true);
  const Duration transitionDuration = Durations.transitionDuration;
  const Transition transition = Transition.rightToLeftWithFade;

  String? userLocaleStr = CachingService.getAppLanguage();
  Locale? userLocale =
      userLocaleStr != null ? Locale(userLocaleStr) : Get.deviceLocale;
//Setting SysemUIOverlay
  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      systemNavigationBarColor: AppColors.transparent,
      systemNavigationBarDividerColor: AppColors.transparent,
      systemNavigationBarIconBrightness: Brightness.dark,
      systemNavigationBarContrastEnforced: false,
      statusBarIconBrightness: Brightness.dark,
    ),
  );

  //Setting SystmeUIMode
  SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge, overlays: [
    //  [SystemUiOverlay.top, SystemUiOverlay.bottom
  ]);

  runApp(
    GetMaterialApp(
      translations: AppLocale(),
      locale: userLocale,
      fallbackLocale: const Locale(
        'en',
      ),
      title: "Clannad",
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
      defaultTransition: transition,
      transitionDuration: transitionDuration,
      theme: ThemeData(
        fontFamily: Assets.fontFamilies.alegreyaF,
        primaryColorLight: AppColors.green,
        scaffoldBackgroundColor: AppColors.white,
        textSelectionTheme:
            const TextSelectionThemeData(cursorColor: AppColors.green),
        primaryColor: AppColors.green,
        backgroundColor: AppColors.white,
        // canvasColor: AppColors.white,
        inputDecorationTheme: const InputDecorationTheme(
          border: UnderlineInputBorder(
            borderSide: BorderSide(color: AppColors.green),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: AppColors.green),
          ),
          errorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: AppColors.green),
          ),
          focusedErrorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: AppColors.green),
          ),
        ),
      ),
    ),
  );
}
