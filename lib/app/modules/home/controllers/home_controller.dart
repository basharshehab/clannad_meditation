import 'package:clannad/app/data/constants/durations.dart';
import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/modules/home/controllers/music_controller.dart';
import 'package:clannad/app/modules/splash/controllers/super_controller.dart';
import 'package:clannad/app/routes/app_pages.dart';
import 'package:clannad/app/services/navigate.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

class HomeController extends GetxController {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  // final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  late User? firebaseUser = _firebaseAuth.currentUser;

  late final GlobalKey<ScaffoldState> scaffoldKey;
  int currentIndex = 0;

  late PageController pageController;

  late Super superController;

  late MusicController musicController;

  @override
  void onInit() {
    super.onInit();
    superController = Get.find<Super>();
    musicController = Get.find<MusicController>();
    pageController = PageController();
    scaffoldKey = GlobalKey<ScaffoldState>();
  }

  // @override
  // void onReady() {
  //   super.onReady();
  // }

  @override
  void onClose() {
    super.onClose();
    pageController.dispose();
  }

  /// Updates the index of the PageView and animates to it.
  void updateIndex(int newIndex) {
    currentIndex = newIndex;
    pageController.animateToPage(newIndex,
        curve: Curves.easeInOut, duration: Durations.animationDurationLong);
    update();
  }

  /// Updates the index without animating to it
  /// used when swiping to the page, not when clicking the icon.
  void updateIndexNoMove(int newIndex) {
    currentIndex = newIndex;
    update();
  }

  /// Logs out of the application.
  void logout() async {
    if (!await InternetConnectionChecker().hasConnection) {
      Get.snackbar(
          LocaleKeys.noConnection.tr, LocaleKeys.checkInternetConnection.tr);
      return;
    }
    try {
      await _firebaseAuth.signOut();
      Navigate.getOffAll(Routes.SIGNIN, null);
    } on Exception catch (_) {
      Get.snackbar(LocaleKeys.couldntLogout.tr, LocaleKeys.tryAgainLater.tr);
    }
  }
}
