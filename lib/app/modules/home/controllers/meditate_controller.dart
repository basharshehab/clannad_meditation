import 'dart:async';
import 'package:clannad/app/data/constants/collections.dart';
import 'package:clannad/app/data/constants/getx_ids.dart';
import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/data/models/user_model.dart';
import 'package:clannad/app/modules/home/components/finished_meditating_bottom_sheet.dart';
import 'package:clannad/app/modules/home/controllers/welcome_controller.dart';
import 'package:clannad/app/modules/splash/controllers/super_controller.dart';
import 'package:clannad/app/services/layout_helper.dart';
import 'package:clannad/app/services/parse_for_firestore.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

class MeditateController extends GetxController
    with GetTickerProviderStateMixin {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  /// Returns [true] if user selected a feeling on this meditation session
  /// used to update [FinishedMeditating]
  bool didSelectFeelingAfter = false;

  /// the selected feeling after this meditation session
  late String feelingAfter;

  /// appropriate response to the feeling after this meditation session
  late String feelingsReponseAfter;

  /// Returns current year
  String thisYear = DateTime.now().year.toString();

  /// Returns current month
  String thisMonth = DateTime.now().month.toString();

  /// Returns current day in month [1-31]
  String thisDay = DateTime.now().day.toString();

  /// Stop watch to record meditation duration
  late Stopwatch _stopwatch;

  /// starts with [_stopWatch] to update UI on every second
  late Timer _timer;

  /// Current meditation session ID
  late String _currentMeditationId;

  /// Start time of current meditation session
  late String _currentMeditatingFrom;

  /// Instance of [WelcomeController]
  late WelcomeController welcomeController;

  Super get superController => Get.find<Super>();

  @override
  void onInit() {
    super.onInit();
    _stopwatch = Stopwatch();
  }

  @override
  void onReady() {
    super.onReady();
    welcomeController = Get.find<WelcomeController>();
  }

  // @override
  // void onClose() {
  //   super.onClose();

  // }

  /// Updates user [FeelingHistoryAfter], current Feeling, and Meditation History.
  void updateFeelingHistoryAfterAndFeeling(String feeling) async {
    if (!await InternetConnectionChecker().hasConnection) {
      Get.snackbar(
          LocaleKeys.noConnection.tr, LocaleKeys.checkInternetConnection.tr);
      return;
    }
    // add the new feeling to feelingsHistoryAfter
    superController.userModel.feelingsHistoryAfter.addAll({
      thisYear: {
        thisMonth: {
          thisDay: FeelingsHistory(
              feeling: ParseForFirestore.feelingsFromLocale(feeling)),
        },
      },
    });

    try {
      // update the user data with the new history.
      await _firestore
          .collection(Collections.users)
          .doc(_firebaseAuth.currentUser!.uid)
          .update(superController.userModel.toJson());

      /// Update [didSelectFeelingAfter] in [this] and in [WelcomeController]
      didSelectFeelingAfter = welcomeController.didSelectFeelingToday = true;

      /// Update [feelingAfter] in [this] and [feelingToday] in [WelcomeController]
      feelingAfter = welcomeController.feelingToday = feeling;

      /// Update [feelingsReponseAfter] in [this] and [feelingsReponseToday] in [WelcomeController]
      feelingsReponseAfter = welcomeController.feelingsResponseToday =
          LayoutHelper.generateFeelingsResponse(welcomeController.feelingToday);

      // update user meditation history and updates
      await _updateUserMeditateHistory(
          _currentMeditatingFrom, _currentMeditationId, feelingAfter);

      // trigger UI update for [GetBuilder]s under [WelcomeController]
      welcomeController.update();
      update([GetxIds.selectedFeelingAfter]);
    } catch (_) {
      /// Shows snackbar ot inform user that something went wrong.
      Get.snackbar(
          LocaleKeys.couldntSyncFeelings.tr, LocaleKeys.tryAgainLater.tr);
      superController.userModel.feelingsHistoryAfter[thisYear]![thisMonth]!
          .removeWhere((key, value) {
        if (key == thisDay) {
          return true;
        }
        return false;
      });
      // welcomeController.selectedFeelingToday = false;
      welcomeController.update();
      // _checkSelectedFeelingToday();
    }
  }

  bool getStopwatchIsRunning() {
    return _stopwatch.isRunning;
  }

  Duration getStopwatchDuration() {
    return _stopwatch.elapsed;
  }

  void startStopwatch() {
    _stopwatch = Stopwatch();
    _stopwatch.start();

    _timer = Timer.periodic(const Duration(seconds: 1), (newTimer) {
      update([GetxIds.stopwatch]);
    });
    update([GetxIds.stopwatch]);
  }

  void stopStopwatch(BuildContext context) async {
    _currentMeditatingFrom = Timestamp.now().millisecondsSinceEpoch.toString();
    _stopwatch.stop();
    _timer.cancel();
    update([GetxIds.stopwatch]);
    await _updateUserStreak();
    _currentMeditationId = Timestamp.now().millisecondsSinceEpoch.toString();
    await _updateUserMeditateHistory(
      _currentMeditatingFrom,
      _currentMeditationId,
      null,
    );
    showModalBottomSheet(
            context: context,
            // shape: BoxShape.circle,
            builder: (context) => FinishedMeditating(stopwatch: _stopwatch))
        .whenComplete(() async {
      if (didSelectFeelingAfter) {
        return;
      }

      updateFeelingHistoryAfterAndFeeling(LocaleKeys.feelingOkay.tr);
    });
  }

  /// Returns the stopwatch duration in MM:SS format
  String getDurationMinutesSeconds() {
    return LayoutHelper.durationToMinutesSecondsDoubleDigits(
        _stopwatch.elapsed);
  }

  Future<void> _updateUserMeditateHistory(
      String fromTimestamp, String meditationId, String? feeling) async {
    if (!await InternetConnectionChecker().hasConnection) {
      Get.snackbar(
          LocaleKeys.noConnection.tr, LocaleKeys.checkInternetConnection.tr);
      return;
    }
    feeling != null
        ? feeling = ParseForFirestore.feelingsFromLocale(feeling)
        : null;
    UserModel newUserModel = superController.userModel.copyWith(
        lastStreakUpdateTimestamp:
            Timestamp.now().millisecondsSinceEpoch.toString());

    if (newUserModel.meditationHistory[thisYear] == null) {
      newUserModel.meditationHistory.addAll(
        <String,
            Map<String, Map<String, Map<String, MeditationHistoryDetails>>>>{
          thisYear: {
            thisMonth: {
              thisDay: {
                meditationId: MeditationHistoryDetails(
                  toTimestamp: meditationId,
                  fromTimestamp: fromTimestamp,
                  feeling: feeling,
                ),
              },
            }
          },
        },
      );
    } else if (newUserModel.meditationHistory[thisYear]![thisMonth] == null) {
      newUserModel.meditationHistory[thisYear]!.addAll(
        <String, Map<String, Map<String, MeditationHistoryDetails>>>{
          thisMonth: {
            thisDay: {
              meditationId: MeditationHistoryDetails(
                toTimestamp: meditationId,
                fromTimestamp: fromTimestamp,
                feeling: feeling,
              ),
            },
          },
        },
      );
    } else if (newUserModel.meditationHistory[thisYear]![thisMonth]![thisDay] ==
        null) {
      newUserModel.meditationHistory[thisYear]![thisMonth]!.addAll(
        <String, Map<String, MeditationHistoryDetails>>{
          thisDay: {
            meditationId: MeditationHistoryDetails(
              toTimestamp: meditationId,
              fromTimestamp: fromTimestamp,
              feeling: feeling,
            ),
          },
        },
      );
    } else {
      newUserModel.meditationHistory[thisYear]![thisMonth]![thisDay]!.addAll(
        <String, MeditationHistoryDetails>{
          meditationId: MeditationHistoryDetails(
            toTimestamp: meditationId,
            fromTimestamp: fromTimestamp,
            feeling: feeling,
          ),
        },
      );
    }

    try {
      await _firestore
          .collection(Collections.users)
          .doc(_firebaseAuth.currentUser!.uid)
          .update(newUserModel.toJson());
      // LayoutHelper.generateFeelingsResponse(welcomeController.feelingToday);
      // superController.userModel = newUserModel;
      // welcomeController.update();
    } catch (_) {
      Get.snackbar(
          LocaleKeys.couldntSyncFeelings.tr, LocaleKeys.tryAgainLater.tr);
    }
  }

  Future<void> _updateUserStreak() async {
    if (!await InternetConnectionChecker().hasConnection) {
      Get.snackbar(
          LocaleKeys.noConnection.tr, LocaleKeys.checkInternetConnection.tr);
      return;
    }

    if (superController.userModel.meditationHistory[thisYear]?[thisMonth]
            ?[thisDay] !=
        null) {
      return;
    }
    // int newStreak = int.parse(superController.userModel.currentStreak) + 1;

    // UserModel newUserModel = UserModel(
    //   name: superController.userModel.name,
    //   emailAddress: superController.userModel.emailAddress,
    //   feelingsHistoryBefore: superController.userModel.feelingsHistoryBefore,
    //   feelingsHistoryAfter: superController.userModel.feelingsHistoryAfter,
    //   meditationHistory: superController.userModel.meditationHistory,
    //   currentStreak: newStreak.toString(),
    //   maxStreak: newStreak > int.parse(superController.userModel.maxStreak)
    //       ? newStreak.toString()
    //       : superController.userModel.maxStreak,
    //   createdAt: superController.userModel.createdAt,
    //   profileQuote: superController.userModel.profileQuote,
    //   imageUrl: superController.userModel.imageUrl,
    //   streakEndedAt: superController.userModel.streakEndedAt,
    // );

    try {
      await _firestore
          .collection(Collections.users)
          .doc(_firebaseAuth.currentUser!.uid)
          .update({
        "current_streak": superController.userModel.currentStreak + 1,
        "max_streak": superController.userModel.currentStreak + 1 >
                superController.userModel.maxStreak
            ? superController.userModel.currentStreak + 1
            : superController.userModel.maxStreak,
      });
      // superController.userModel = newUserModel;
      // welcomeController.update();
    } catch (_) {
      Get.snackbar(
          LocaleKeys.couldntSyncFeelings.tr, LocaleKeys.tryAgainLater.tr);
    }
  }
}
