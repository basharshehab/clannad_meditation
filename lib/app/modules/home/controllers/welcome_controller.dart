import 'package:clannad/app/data/constants/collections.dart';
import 'package:clannad/app/data/constants/getx_ids.dart';
import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/data/models/course_model.dart';
import 'package:clannad/app/data/models/user_model.dart';
import 'package:clannad/app/modules/home/controllers/meditate_controller.dart';
import 'package:clannad/app/modules/splash/controllers/super_controller.dart';
import 'package:clannad/app/services/caching_service.dart';
import 'package:clannad/app/services/int_to_date_service.dart';
import 'package:clannad/app/services/layout_helper.dart';
import 'package:clannad/app/services/parse_for_firestore.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:get/get.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:url_launcher/url_launcher.dart';

class WelcomeController extends GetxController {
  bool didSelectFeelingToday = false;
  bool selectedFeelingAfter = false;
  late String feelingToday;
  late String feelingsResponseToday;

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  String thisYear = DateTime.now().year.toString();
  String thisMonth = DateTime.now().month.toString();
  String thisDay = DateTime.now().day.toString();

  MeditateController get meditateController => Get.find<MeditateController>();
  Super get superController => Get.find<Super>();

  List<CourseModel> courses = [];
  @override
  void onInit() {
    super.onInit();
    _checkSelectedFeelingToday();
    _getCoursesFromFirebaseOrCache();
  }

  // @override
  // void onReady() {
  //   super.onReady();
  // }

  // @override
  // void onClose() {
  //   super.onClose();
  // }

  Future<void> sendUserToYouTube(String url) async {
    bool canLaunch = await canLaunchUrl(Uri.parse(url));
    if (canLaunch) {
      launchUrl(
        Uri.parse(url),
        mode: LaunchMode.externalNonBrowserApplication,
      );
    } else {
      Get.snackbar(
          LocaleKeys.couldntOpenCourse.tr, LocaleKeys.tryAgainLater.tr);
    }
  }

  Future<void> _getCoursesFromFirebaseOrCache() async {
    List<Map<String, dynamic>> courseData;
    List<Map<String, dynamic>>? s = await CachingService.getCachedCourses();
    if (s != null) {
      courseData = s;
    } else {
      QuerySnapshot<Map<String, dynamic>> query =
          await _firestore.collection(Collections.courses).get();
      List<QueryDocumentSnapshot<Map<String, dynamic>>> snapshots = query.docs;

      courseData = Converter.querySnapshotToMap(snapshots);

      await CachingService.saveCourses(courseData);
    }

    for (Map<String, dynamic> item in courseData) {
      courses.add(CourseModel.fromJson(item));
    }

    update([GetxIds.courseAdsList]);
  }

  void _checkSelectedFeelingToday() {
    didSelectFeelingToday = superController
                .userModel
                .feelingsHistoryBefore[thisYear]?[thisMonth]?[thisDay]
                ?.feeling !=
            null
        ? true
        : false;
    selectedFeelingAfter = superController
                .userModel
                .feelingsHistoryAfter[thisYear]?[thisMonth]?[thisDay]
                ?.feeling !=
            null
        ? true
        : false;
    feelingToday = selectedFeelingAfter == true
        ? ParseForFirestore.feelingsToLocale(superController.userModel
            .feelingsHistoryAfter[thisYear]![thisMonth]![thisDay]!.feeling!)
        : didSelectFeelingToday == true
            ? ParseForFirestore.feelingsToLocale(superController
                .userModel
                .feelingsHistoryBefore[thisYear]![thisMonth]![thisDay]!
                .feeling!)
            : "";
    feelingsResponseToday = LayoutHelper.generateFeelingsResponse(feelingToday);
  }

  void updateFeelingHistoryBeforeAndFeeling(String feeling) async {
    if (!await InternetConnectionChecker().hasConnection) {
      Get.snackbar(
          LocaleKeys.noConnection.tr, LocaleKeys.checkInternetConnection.tr);
      return;
    }
    superController.userModel.feelingsHistoryBefore.addAll({
      thisYear: {
        thisMonth: {
          thisDay: FeelingsHistory(
              feeling: ParseForFirestore.feelingsFromLocale(feeling)),
        },
      },
    });

    try {
      await _firestore
          .collection(Collections.users)
          .doc(_firebaseAuth.currentUser!.uid)
          .update(superController.userModel.toJson());
      didSelectFeelingToday = true;
      feelingToday = feeling;
      feelingsResponseToday =
          LayoutHelper.generateFeelingsResponse(feelingToday);
      update();
    } catch (_) {
      Get.snackbar(
          LocaleKeys.couldntSyncFeelings.tr, LocaleKeys.tryAgainLater.tr);
      superController.userModel.feelingsHistoryBefore[thisYear]![thisMonth]!
          .removeWhere((key, value) {
        if (key == thisDay) {
          return true;
        }
        return false;
      });
      update();
    }
  }
}
