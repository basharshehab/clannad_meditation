import 'dart:async';
import 'dart:math';

import 'package:clannad/app/data/constants/collections.dart';
import 'package:clannad/app/data/constants/durations.dart';
import 'package:clannad/app/data/constants/getx_ids.dart';
import 'package:clannad/app/data/models/music_list_model.dart';
import 'package:clannad/app/modules/home/components/music_player_components/audio_bar.dart';
import 'package:clannad/app/modules/home/components/music_tile.dart';
import 'package:clannad/app/modules/home/components/playlist_widget.dart';
import 'package:clannad/app/services/caching_service.dart';
import 'package:clannad/app/services/int_to_date_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:just_audio/just_audio.dart';

import '../views/music_player_view.dart';

class MusicController extends GetxController
    with GetSingleTickerProviderStateMixin, WidgetsBindingObserver {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  /// The list of widgets which will populate the AnimatedList
  List<Widget> musicTilesWidgets = [];

  /// The key to the AnimatedList, used to add widgets to the list (and be animated)
  final GlobalKey<AnimatedListState> animatedPlaylistKey =
      GlobalKey<AnimatedListState>();

  /// Slide animation Tween, to slide the MusicTiles in place.
  Tween<Offset> slideAnimationTween =
      Tween(begin: const Offset(1, 0), end: const Offset(0, 0));

  /// Fade animation Tween, to fade the items in as they slide
  Tween<double> fadeAnimationTween = Tween(begin: 0.0, end: 1.0);

  late AnimationController animationController;

  late Animation<double> frontRotation;
  late Animation<double> backRotation;

  /// Contains list of [MusicModel] of unshuffled music tracks
  List<MusicModel> unShuffledMusicData = [];

  /// Contains list of [MusicModel] of shuffled music tracks
  List<MusicModel> _shuffledMusicData = [];

  /// Contains list of [MusicModel] of shuffled music tracks
  List<MusicModel> _inUseMusicData = [];

  /// indicates the current progress of the audio playing
  int currentlyAt = 0;

  /// indicates if shuffle is enabled
  bool shuffleOn = false;

  /// indicates if repeat is set to All
  bool repeatAll = false;

  // indicates of repeat is set to One
  bool repeatOne = false;

  // indicates if the floating player should be visible.
  bool floatingPlayerUp = false;

  /// Need to populate this list with AudioBars.
  /// This decides how many bars will be in the progress indicator
  RxList<AudioBar> bars = <AudioBar>[].obs;

  /// tracks how much the user drags on the progress indicator
  double _draggingTracker = 0;

  /// [AudioPlayer] used to control audio.
  late AudioPlayer audioPlayer;
  late Rx<MusicModel> currentTrack;
  late MusicModel _currentTrackBeforeUpdate;
  bool justSkipped = false;
  late StreamSubscription<Duration> positionStream;
  @override
  void onInit() {
    super.onInit();
    audioPlayer = AudioPlayer();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void onReady() async {
    super.onReady();

    await _getMusicFromFirebaseOrCache();
    // UI
    _resetAndAddMusicTiles();
    _initAnimations();

    // Audio Player
    _setAudioSource(_inUseMusicData[0]);
    _initAudioListeners();
    _initCurrentListenersStream();
    // _updatePreviousIndex(0);
  }

  @override
  void onClose() {
    super.onClose();

    audioPlayer.dispose();
    positionStream.cancel();
    animationController.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.resumed:
        _addCountFromCurrentlyListeningOnAppResumed();
        break;

      /*
      AppLifecycleState.paused only gets
      called if the phone is slow. Since platforms don't
      allow apps to live longer than the user wants, it shuts the app down
      as soon as it can. The ultimate solution to keeping a live count
      would be Firebase Functions, but I'm too poor to afford
      that at the moment.
      As a result, the phone will execute this if there's enough time
      between the moment the user closes the app/sends it to the background,
      and the app actually shutting down.
      Otherwise, it will be executed the next time the user launches the app.
      I consider this "Good enough" for the resources I have right now.
      If any expert is reading this, I'd love to hear about better solutions
      than what I'm doing here.
        */
      case AppLifecycleState.paused:
        _removeCountFromCurrentlyListeningOnAppPaused();
        break;
      // case AppLifecycleState.detached:
      //   _removeCountFromCurrentlyListeningOnAppPaused();
      //   break;
      default:
        break;
    }
  }

  /// Populates the Music [List], from which the songs, art, duration, etc...
  /// would be taken
  void _resetAndAddMusicTiles() {
    // List songsList = songsData.locale[neededLocale]!;
    // music = [];
    Future ft = Future(() {});
    for (int i = -1; i < unShuffledMusicData.length; i++) {
      ft = ft.then((_) {
        return Future.delayed(Durations.animationDurationShort, () {
          i == -1
              ? musicTilesWidgets.add(const PlaylistWidget())
              : musicTilesWidgets.add(
                  MusicTile(
                    index: i,
                  ),
                );
          animatedPlaylistKey.currentState?.insertItem(i + 1);
        });
      });
    }
  }

  /// initialising the varialbes needed for the Play/Pause button flip animation
  /// taken from :
  /// https://stackoverflow.com/questions/54958897/flutter-flip-animation-flip-a-card-over-its-right-or-left-side-based-on-the
  void _initAnimations() {
    animationController = animationController = AnimationController(
        duration: Durations.animationDurationLong, vsync: this);

    frontRotation = TweenSequence(
      <TweenSequenceItem<double>>[
        TweenSequenceItem<double>(
          tween: Tween(begin: 0.0, end: pi / 2)
              .chain(CurveTween(curve: Curves.linear)),
          weight: 50.0,
        ),
        TweenSequenceItem<double>(
          tween: ConstantTween<double>(pi / 2),
          weight: 50.0,
        ),
      ],
    ).animate(animationController);

    backRotation = TweenSequence(
      <TweenSequenceItem<double>>[
        TweenSequenceItem<double>(
          tween: ConstantTween<double>(pi / 2),
          weight: 50.0,
        ),
        TweenSequenceItem<double>(
          tween: Tween(begin: -pi / 2, end: 0.0)
              .chain(CurveTween(curve: Curves.linear)),
          weight: 50.0,
        ),
      ],
    ).animate(animationController);
  }

  Future<void> _getMusicFromFirebaseOrCache() async {
    List<Map<String, dynamic>>? s = await CachingService.getCachedMusic();

    if (s != null) {
      List<Map<String, dynamic>> tmp = s;
      _resetAndFillMusicData(tmp);
    } else {
      List<QueryDocumentSnapshot<Map<String, dynamic>>> tmp =
          (await _firestore.collection(Collections.music).get()).docs;

      List<Map<String, dynamic>> music = Converter.querySnapshotToMap(tmp);
      await CachingService.saveMusic(music);
      _resetAndFillMusicData(music);
    }
    _inUseMusicData = List.from(unShuffledMusicData);
    currentTrack = _inUseMusicData[0].obs;
    _currentTrackBeforeUpdate = currentTrack.value;
  }

  /// Seek current audio depending on the index of the audio bar clicked
  void jumpToOnClick(int index) {
    if (audioPlayer.duration == null) {
      return;
    }
    // update CurrentlyAt index to update the AudioBars later
    currentlyAt = index;

    // update get the duration corresponding to the index of the bar clicked.
    double audioMilliseconds =
        _getAudioMillisecondsFromBarIndex(index, bars, audioPlayer);
    // seek to that location
    audioPlayer.seek(Duration(milliseconds: audioMilliseconds.toInt()));
    // update AudioBars
    update([GetxIds.audioBar]);
  }

  /// Seek current audio depending on how much
  /// the user drags the progress indicator
  void jumpToOnDrag(DragUpdateDetails value) {
    Offset offset = value.delta;

    _draggingTracker += offset.dx.abs() * 0.15;
    if (_draggingTracker < 1) {
      return;
    }
    int min = currentlyAt - _draggingTracker.toInt();
    int max = currentlyAt + _draggingTracker.toInt();

    if (offset.dx < 0) {
      if (min < 0) min = 0;
      // resetProgressTo(min);
      jumpToOnClick(min);
    } else {
      if (max > bars.length) max = bars.length;
      // resetProgressTo(max);
      jumpToOnClick(max);
    }
    _draggingTracker = 0;

    // update();
  }

  /// Gets the percentage of the currently playing audio file
  /// from the index clicked
  double _getAudioMillisecondsFromBarIndex(
      int barIndex, List<AudioBar> bars, AudioPlayer audioPlayer) {
    double barProgressPercent = barIndex / bars.length;
    double audioPercent =
        audioPlayer.duration!.inMilliseconds * barProgressPercent;
    return audioPercent;
  }

  void _resetAndFillMusicData(List<Map<String, dynamic>> tmp) {
    unShuffledMusicData = [];

    for (Map<String, dynamic> item in tmp) {
      // Map<String, dynamic> musicTrack = item.data();

      MusicModel musicModel = MusicModel.fromJson(item);

      unShuffledMusicData.add(musicModel);
    }
  }

  /// Sets the AudioSource using [LockCachingAudioSource].
  /// This caches the audio file, so we're not calling Cloud Storage
  /// everytime we launch the app.
  // This makes repeat, shuffle, and seeking more difficult
  // since it's not automated by [AudioPlayer]
  Future<void> _setAudioSource(MusicModel track) async {
    try {
      await audioPlayer
          .setAudioSource(LockCachingAudioSource(Uri.parse(track.url)));
    } catch (_) {
      debugPrint(_.toString());
    }
  }

  /// Initialises the position, index, and current duration streams
  /// to update UI accourdingly
  void _initAudioListeners() async {
    // audioPlayer.positionStream.listen();
    positionStream = audioPlayer
        .createPositionStream(
          minPeriod: const Duration(milliseconds: 500),
          maxPeriod: const Duration(seconds: 1),
          steps: 1,
        )
        .listen(_updateProgressBar);
    currentTrack.listen((event) async {
      await _setAudioSource(event);
      // _updatePreviousIndex(event);
      update([GetxIds.musicPlayer]);

      await _updateCurrentlyListening();
    });
    await _updateCurrentlyListening(first: true);

    audioPlayer.durationStream.listen((event) {
      update([GetxIds.maxSongDuration]);
    });
  }

  /// called from [audioPlayer.positionStream], updates UI accourding
  /// to the current position in the audio file
  void _updateProgressBar(Duration newDuration) {
    // debugPrint("HIII $newDuration");
    if (audioPlayer.duration != null) {
      if (newDuration.inMilliseconds >
          (audioPlayer.duration?.inMilliseconds as int)) {
        skipAhead();
      }
    }

    double barPercent = _getBarPercentFromAudio(newDuration, bars, audioPlayer);
    // update index
    currentlyAt = barPercent.toInt();
    // and UI
    update([GetxIds.audioBar, GetxIds.currnetSongProgressDuration]);
    // }
  }

  /// Gets the bar percentage corresponding
  /// to the current position of the audio
  double _getBarPercentFromAudio(
      Duration currentAudio, List<AudioBar> bars, AudioPlayer audioPlayer) {
    if (audioPlayer.duration == null) {
      return 0;
    }
    double audioProgressPercent =
        currentAudio.inMilliseconds / audioPlayer.duration!.inMilliseconds;
    double barPercent = audioProgressPercent * bars.length;
    return barPercent;
  }

  void _initCurrentListenersStream() {
    _firestore.collection(Collections.music).snapshots().listen((event) {
      List<QueryDocumentSnapshot<Map<String, dynamic>>> data = event.docs;

      List<Map<String, dynamic>> music = Converter.querySnapshotToMap(data);
      _resetAndFillMusicData(music);
      update([GetxIds.currentlyListening]);
    });
  }

  void tileTapped(int index, BuildContext context) {
    // if (index != currentIndex.value) {
    //   _setAudioSource(index);
    //   currentIndex(index);
    //   // _updatePreviousIndex(newCurrent)
    // }
    currentTrack(unShuffledMusicData[index]);
    // currentTrack(_getTrackFromIndex(index));
    floatingPlayerUp ? null : popUpMusicPlayer(context);
  }

  // MusicModel _getTrackFromIndex(int index) {
  //   return inUseMusicData[index];
  // }

  MusicModel _getNextTrack() {
    int currentIndex =
        _inUseMusicData.indexWhere((element) => element == currentTrack.value);
    int nextIndex;
    if (currentIndex == _inUseMusicData.length - 1) {
      nextIndex = 0;
    } else {
      nextIndex = currentIndex + 1;
    }
    return _inUseMusicData[nextIndex];
  }

  MusicModel _getPreviousTrack() {
    int currentIndex =
        _inUseMusicData.indexWhere((element) => element == currentTrack.value);
    int previousIndex;
    if (currentIndex <= 0) {
      previousIndex = _inUseMusicData.length - 1;
    } else {
      previousIndex = currentIndex - 1;
    }
    return _inUseMusicData[previousIndex];
  }

  int _getIndexFromMusicModel(MusicModel model, List<MusicModel> listOfMusic) {
    return listOfMusic.indexOf(model);
  }

  /// Toggle Shuffle in [AudioPlayer] and update UI
  /// Builder Id: [GetxIds.shuffle]
  void toggleShuffle() {
    shuffleOn = !shuffleOn;
    if (shuffleOn) {
      _generateShuffledMusicDataList();
      _inUseMusicData = List.from(_shuffledMusicData);
    } else {
      // _generateUnshuffledMusicDataList();
      _inUseMusicData = List.from(unShuffledMusicData);
    }
    update([GetxIds.shuffle]);
  }

  void _generateShuffledMusicDataList() {
    _shuffledMusicData = List.from(unShuffledMusicData);
    _shuffledMusicData.shuffle(Random());

    // int currentTrackInShuffledList =
    //     _getIndexFromMusicModel(currentTrack.value, _shuffledMusicData);

    // MusicModel a, b;
    // a = _shuffledMusicData[0];
    // b = _shuffledMusicData[currentTrackInShuffledList];

    // _shuffledMusicData.removeAt(0);
    // _shuffledMusicData.insert(0, b);
    // _shuffledMusicData.removeAt(currentTrackInShuffledList);
    // _shuffledMusicData.insert(currentTrackInShuffledList, a);

    // int newIndex = 0;
    // // previousIndex = _shuffledMusicData.length - 1;
    // currentIndex(newIndex);
  }

//   /// Generates a shuffled music list and updates [currentIndex]
//   /// and [previousIndex] accourding to the new list.
//   void _generateUnshuffledMusicDataList() {
//     int? currentTrackInUnshuffledListIndex;
//     for (int i = 0; i < unShuffledMusicData.length; i++) {
//       if (unShuffledMusicData[i].id ==
//           _shuffledMusicData[currentIndex.value].id) {
//         currentTrackInUnshuffledListIndex = i;
//         break;
//       }
//     }
//     currentTrackInUnshuffledListIndex ??= 0;

//     currentIndex(currentTrackInUnshuffledListIndex);
//     // if (currentIndex.value == unShuffledMusicData.length - 1) {
//     //   previousIndex == 0;
//     // } else {
//     //   previousIndex = currentIndex.value - 1;
//     // }
//   }

  // /// Toggle Repeat in [AudioPlayer] and update UI
  // /// Builder Id: [GetxIds.repeat]
  // /// asserts that repeatOne and repeatAll can not be enabled at the same time.
  void toggleRepeat() {
    if (repeatAll) {
      repeatAll = false;
      repeatOne = true;
    } else if (repeatOne) {
      repeatOne = false;
      repeatAll = false;
    } else {
      repeatAll = true;
    }
    assert(repeatAll != true || repeatOne != true);
    update([GetxIds.repeat]);
  }

  /// Toggle Play in [AudioPlayer] and update UI
  /// Builder Id: [GetxIds.play]
  void togglePlay() {
    // start/pause audio
    if (audioPlayer.playing) {
      audioPlayer.pause();
      animationController.reverse();
    } else {
      audioPlayer.play();
      animationController.forward();
    }
    floatingPlayerUp = true;
    update([GetxIds.floatingPlayer, GetxIds.play]);
  }

  /// Seek to the next item, or if there is no next item,
  /// stop the audio player and reset.
  void skipAhead() async {
    if (repeatOne) {
      _setAudioSource(currentTrack.value);
      // return;
    } else if (repeatAll) {
      currentTrack(_getNextTrack());
    } else {
      if (_getIndexFromMusicModel(currentTrack.value, _inUseMusicData) ==
          _inUseMusicData.length - 1) {
        _audioFinished();
      } else {
        currentTrack(_getNextTrack());
      }
    }
  }

//   /// Seek to the previous item, of if there's no previous item,
//   /// seek to the start of the current audio.
  void skipBack() async {
    if (repeatOne) {
      _setAudioSource(currentTrack.value);
      // return;
    } else if (repeatAll) {
      currentTrack(_getPreviousTrack());
    } else {
      if (_getIndexFromMusicModel(currentTrack.value, _inUseMusicData) == 0) {
        _audioFinished();
      } else {
        currentTrack(_getPreviousTrack());
      }
    }
  }

  /// If all audio files are finished, stop the player and reset.
  _audioFinished() async {
    audioPlayer.playing == true ? togglePlay() : null;
    jumpToOnClick(0);
    // previousIndex = currentIndex.value;
    currentTrack(_inUseMusicData[0]);
    await audioPlayer.stop();
    // try {
    //   _setAudioSource(0);
    // } catch (e) {
    //   debugPrint(e.toString());
    // }
  }

  void floatingPlayerDismissed(_) {
    // pause audio
    if (audioPlayer.playing) {
      audioPlayer.pause();
      animationController.reverse();
    } // isPlaying = false;
    floatingPlayerUp = false;
    update([GetxIds.floatingPlayer]);
    // _removeCountFromCurrentlyListening();
  }

  /// Generates the [AudioBar]s which will populate the progress indicator
  void generateAudioBars(List<double> listOfHeights, Color playedColor,
      Color unplayedColor, double playerWidth) {
    bars = <AudioBar>[].obs;
    double barWidth = playerWidth / listOfHeights.length;
    barWidth = barWidth - playerWidth * 0.0075;
    for (int i = 0; i < listOfHeights.length; i++) {
      bars.add(
        AudioBar(
          index: i,
          height: listOfHeights[i],
          playedColor: playedColor,
          unplayedColor: unplayedColor,
          width: barWidth,
          playerWidth: playerWidth,
        ),
      );
    }
  }

  /// Pops up the music player.
  void popUpMusicPlayer(context) async {
    // await playerController.preparePlayer(path);

    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (context) {
          return const MusicPlayerView();
        });
  }

//   void _updatePreviousIndex(int newCurrent) {
//     if (newCurrent == 0) {
//       previousIndex = inUseMusicData.length - 1;
//     } else {
//       previousIndex = newCurrent - 1;
//     }
//     // debugPrint("HIII currentIndex is ${currentIndex.value}");
//     // debugPrint("HIII previousIndex is $previousIndex");
//   }

  Future<void> _updateCurrentlyListening({bool first = false}) async {
    await _firestore.runTransaction((transaction) async {
      if (!first) {
        transaction.update(
            _firestore
                .collection(Collections.music)
                .doc(_currentTrackBeforeUpdate.id),
            {
              "current_listeners": FieldValue.increment(-1),
            });
        _currentTrackBeforeUpdate = currentTrack.value;
      }
      transaction.update(
          _firestore.collection(Collections.music).doc(currentTrack.value.id), {
        "current_listeners": FieldValue.increment(1),
      });

      return transaction;
    });
  }

  Future<void> _removeCountFromCurrentlyListeningOnAppPaused() async {
    await _firestore
        .collection(Collections.music)
        .doc(currentTrack.value.id)
        .update({
      "current_listeners": FieldValue.increment(-1),
    });

    return;
  }

  Future<void> _addCountFromCurrentlyListeningOnAppResumed() async {
    await _firestore
        .collection(Collections.music)
        .doc(currentTrack.value.id)
        .update({
      "current_listeners": FieldValue.increment(1),
    });

    // return true;
  }
}
