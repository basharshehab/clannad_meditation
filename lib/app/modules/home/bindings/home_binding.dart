import 'package:clannad/app/modules/home/controllers/meditate_controller.dart';
import 'package:clannad/app/modules/home/controllers/music_controller.dart';
import 'package:clannad/app/modules/home/controllers/welcome_controller.dart';
import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
    Get.lazyPut<WelcomeController>(
      () => WelcomeController(),
    );
    Get.lazyPut<MusicController>(
      () => MusicController(),
    );
    Get.lazyPut<MeditateController>(
      () => MeditateController(),
    );
    // Get.lazyPut<PlayerController>(
    //   () => PlayerController(),
    // );
  }
}
