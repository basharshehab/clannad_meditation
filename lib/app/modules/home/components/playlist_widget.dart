import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/data/constants/paddings.dart';
import 'package:clannad/app/data/general_components/button_builder.dart';
import 'package:clannad/app/data/general_components/text_builder.dart';
import 'package:clannad/app/modules/home/controllers/music_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PlaylistWidget extends GetWidget<MusicController> {
  const PlaylistWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: Paddings.vertical(context) * 7,
      ),
      margin: EdgeInsets.only(
        bottom: Paddings.vertical(context) * 5,
      ),
      child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              image: DecorationImage(
                image: AssetImage(
                  Assets.backgrounds.widgetBackgrounds.backgroundPlayWidget,
                ),
                fit: BoxFit.cover,
              ),
            ),
            width: AppSizes.width(context),
            height: AppSizes.height(context) * 0.24,
          ),
          Container(
            padding: EdgeInsets.only(
              top: Paddings.vertical(context) * 6,
              left: Paddings.horizontal(context) * 2,
              right: Paddings.horizontal(context) * 2,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextBuilder(
                  maxWidth: AppSizes.width(context),
                  text: LocaleKeys.relaxMeditate.tr,
                  fontFamily: Assets.fontFamilies.alegreyaF,
                  maxLines: 3,
                  fontWeight: FontWeight.normal,
                  textColor: AppColors.white,
                  maxHeight: AppSizes.height(context) * 0.05,
                ),
                TextBuilder(
                  maxWidth: AppSizes.width(context),
                  text: LocaleKeys.relaxMeditateSubtitle.tr,
                  fontFamily: Assets.fontFamilies.alegreyaF,
                  maxLines: 3,
                  fontWeight: FontWeight.normal,
                  textColor: AppColors.white,
                  maxHeight: AppSizes.height(context) * 0.05,
                ),
                // Spacer(),
                Container(
                  padding:
                      EdgeInsets.only(top: AppSizes.height(context) * 0.02),
                  child: ButtonBuilder(
                    onPressed: () => controller.popUpMusicPlayer(context),
                    backgroundColor: AppColors.white,
                    padding: Paddings.horizontalPadding(context),
                    borderRadius: 10.0,
                    icon: const Icon(
                      Icons.play_circle_fill_outlined,
                      color: AppColors.black,
                      size: 15,
                    ),
                    text: TextBuilder(
                        textAlign: TextAlign.center,
                        maxWidth: AppSizes.width(context) * 0.25,
                        text: LocaleKeys.startListening.tr,
                        fontFamily: Assets.fontFamilies.alegreyaSansF,
                        maxLines: 1,
                        fontWeight: FontWeight.w500,
                        textColor: AppColors.black,
                        maxHeight: AppSizes.height(context) * 0.023),
                    iconPadding: const EdgeInsets.all(5),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
