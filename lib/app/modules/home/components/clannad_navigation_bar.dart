import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/constants/getx_ids.dart';
// import 'package:clannad/app/data/constants/paddings.dart';
import 'package:clannad/app/modules/home/components/music_player_components/music_player_controls.dart';
import 'package:clannad/app/modules/home/components/navigation_bar_button.dart';
import 'package:clannad/app/modules/home/controllers/home_controller.dart';
import 'package:clannad/app/modules/home/controllers/music_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ClannadNavigationBar extends GetView<HomeController> {
  const ClannadNavigationBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: controller,
      builder: (context, child) => SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            GetBuilder<MusicController>(
              id: GetxIds.floatingPlayer,
              builder: (controller) => controller.floatingPlayerUp
                  ? Dismissible(
                      key: const ValueKey(GetxIds.floatingPlayer),
                      onDismissed: controller.floatingPlayerDismissed,
                      direction: DismissDirection.down,
                      child: const MusicPlayerControls(
                        playButtonSize: null,
                      ),
                    )
                  : const SizedBox(),
            ),
            Container(
              height: AppSizes.bottomBar(context),
              color: AppColors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  BottomNavigationBarButton(
                    index: 0,
                    assetActive: Assets.bottomBar.bottomBarHomeActive,
                    assetInActive: Assets.bottomBar.bottomBarHomeInActive,
                    iconHeight: AppSizes.height(context) * 0.04,
                  ),
                  BottomNavigationBarButton(
                    index: 1,
                    assetActive: Assets.bottomBar.bottomBarMusicActive,
                    assetInActive: Assets.bottomBar.bottomBarMusicInActive,
                    iconHeight: AppSizes.height(context) * 0.03,
                  ),
                  BottomNavigationBarButton(
                    index: 2,
                    assetActive: Assets.bottomBar.bottomBarMeditateActive,
                    assetInActive: Assets.bottomBar.bottomBarMeditateInActive,
                    iconHeight: AppSizes.height(context) * 0.03,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
