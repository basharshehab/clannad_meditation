import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/constants/getx_ids.dart';
import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/data/constants/paddings.dart';
import 'package:clannad/app/data/general_components/text_builder.dart';
import 'package:clannad/app/modules/home/controllers/music_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MusicTile extends GetView<MusicController> {
  // final String songTitle;
  // final String currentListeners;
  // final String songDuration;
  // final String timeUnit;
  // final String songArt;
  // final String url;
  final int index;

  const MusicTile({
    Key? key,
    required this.index,
    // required this.songTitle,
    // required this.currentListeners,
    // required this.songDuration,
    // required this.songArt,
    // required this.timeUnit,
    // required this.url,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String localTimeUnit = controller.unShuffledMusicData[(index)].timeUnit;
    switch (localTimeUnit) {
      case "second":
        localTimeUnit = LocaleKeys.seconds.tr;
        break;
      case "minute":
        localTimeUnit = LocaleKeys.minutes.tr;
        break;
      case "hour":
        localTimeUnit = LocaleKeys.hours.tr;
        break;
    }
    return GestureDetector(
      onTap: () => controller.tileTapped(index, context),
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: Paddings.vertical(context) * 3,
          horizontal: Paddings.horizontal(context),
        ),
        color: AppColors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  height: AppSizes.width(context) * 0.17,
                  width: AppSizes.width(context) * 0.17,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    image: DecorationImage(
                      image: CachedNetworkImageProvider(
                        controller.unShuffledMusicData[(index)].art,
                      ),
                    ),
                  ),
                  // child: Image.network(songArt,
                  //     height: AppSizes.height(context) * 0.075
                  //     // scale: ,
                  //     ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: Paddings.horizontalPadding(context),
                      child: TextBuilder(
                        maxWidth: AppSizes.width(context) * 0.3,
                        text: controller.unShuffledMusicData[(index)].title,
                        fontFamily: Assets.fontFamilies.alegreyaSansF,
                        maxLines: 2,
                        fontWeight: FontWeight.w500,
                        textColor: AppColors.black,
                        maxHeight: AppSizes.height(context) * 0.03,
                      ),
                    ),
                    GetBuilder<MusicController>(
                      id: GetxIds.currentlyListening,
                      builder: (_) => Container(
                        padding: Paddings.horizontalPadding(context),
                        child: TextBuilder(
                          maxWidth: AppSizes.width(context) * 0.4,
                          text:
                              "${controller.unShuffledMusicData[(index)].currentListeners} ${LocaleKeys.listening.tr}",
                          fontFamily: Assets.fontFamilies.alegreyaSansF,
                          maxLines: 1,
                          fontWeight: FontWeight.w500,
                          textColor: AppColors.subtitle3,
                          maxHeight: AppSizes.height(context) * 0.02,
                        ),
                      ),
                    ), // TextBuilder(
                    //   width: width,
                    //   maxWidth: maxWidth,
                    //   text: text,
                    //   fontFamily: fontFamily,
                    //   maxLines: maxLines,
                    //   fontWeight: fontWeight,
                    //   textColor: textColor,
                    //   height: height,
                    //   maxHeight: maxHeight,
                    // ),
                  ],
                ),
              ],
            ),
            AutoSizeText(
              "${controller.unShuffledMusicData[(index)].duration} $localTimeUnit",
              style: TextStyle(
                fontFamily: Assets.fontFamilies.alegreyaSansF,
                color: AppColors.description,
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
