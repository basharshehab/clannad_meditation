import 'package:cached_network_image/cached_network_image.dart';
import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/data/constants/paddings.dart';
import 'package:clannad/app/data/general_components/button_builder.dart';
import 'package:clannad/app/data/general_components/text_builder.dart';
import 'package:clannad/app/modules/home/controllers/welcome_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CourseWidget extends GetView<WelcomeController> {
  final String courseTitle;
  final String courseDescription;
  final String coursePicture;
  final String url;
  const CourseWidget({
    Key? key,
    required this.courseTitle,
    required this.courseDescription,
    required this.coursePicture,
    required this.url,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        bottom: Paddings.horizontal(context),
        top: Paddings.horizontal(context),
      ),
      height: AppSizes.height(context) / 4.5,
      margin: EdgeInsets.only(
        bottom: Paddings.horizontal(context),
      ),
      decoration: BoxDecoration(
        color: AppColors.courseBackground,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            padding: EdgeInsets.only(
              left: Paddings.vertical(context) * 5,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextBuilder(
                      maxWidth: AppSizes.width(context) * 0.4,
                      text: courseTitle,
                      fontFamily: Assets.fontFamilies.alegreyaF,
                      maxLines: 1,
                      fontWeight: FontWeight.normal,
                      textColor: AppColors.darkGreen,
                      maxHeight: AppSizes.height(context) * 0.05,
                    ),
                    TextBuilder(
                      maxWidth: AppSizes.width(context) * 0.45,
                      text: courseDescription,
                      fontFamily: Assets.fontFamilies.alegreyaSansF,
                      maxLines: 3,
                      fontWeight: FontWeight.w500,
                      textColor: AppColors.description,
                      maxHeight: AppSizes.height(context) * 0.07,
                    ),
                  ],
                ),
                ButtonBuilder(
                  backgroundColor: AppColors.green,
                  padding: EdgeInsets.symmetric(
                      // vertical: Paddings.vertical(context),
                      horizontal: Paddings.horizontal(context)),
                  borderRadius: 10.0,
                  text: TextBuilder(
                    maxWidth: AppSizes.width(context) * 0.3,
                    text: LocaleKeys.watchNow.tr,
                    fontFamily: Assets.fontFamilies.alegreyaSansF,
                    maxLines: 1,
                    fontWeight: FontWeight.normal,
                    textColor: AppColors.white,
                    maxHeight: AppSizes.height(context) * 0.022,
                  ),
                  icon: const Icon(
                    Icons.play_circle_rounded,
                    color: AppColors.white,
                    size: 15,
                  ),
                  iconPadding: const EdgeInsets.all(5),
                  onPressed: () => controller.sendUserToYouTube(url),
                ),
              ],
            ),
          ),
          Flexible(
            child: CachedNetworkImage(
              imageUrl: coursePicture,
            ),
            // child: Image.asset(Assets.courseBackground1),
          )
        ],
      ),
    );
  }
}
