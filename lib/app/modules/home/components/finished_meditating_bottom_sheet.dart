import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/constants/durations.dart';
import 'package:clannad/app/data/constants/getx_ids.dart';
import 'package:clannad/app/data/constants/paddings.dart';
import 'package:clannad/app/data/general_components/rich_text_builder.dart';
import 'package:clannad/app/data/general_components/text_builder.dart';
import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/modules/home/components/feeling_boxes.dart';
import 'package:clannad/app/modules/home/controllers/meditate_controller.dart';
import 'package:clannad/app/services/layout_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FinishedMeditating extends GetView<MeditateController> {
  const FinishedMeditating({
    Key? key,
    required Stopwatch stopwatch,
  })  : _stopwatch = stopwatch,
        super(key: key);

  final Stopwatch _stopwatch;

  @override
  Widget build(BuildContext context) {
    List<String> splitTime = LayoutHelper.durationToHumanReadableDoubleDigits(
      _stopwatch.elapsed,
    ).split(" ");
    controller.didSelectFeelingAfter = false;
    return Wrap(children: [
      Container(
        padding: EdgeInsets.only(
            top: kToolbarHeight,
            left: Paddings.horizontal(context),
            right: Paddings.horizontal(context)),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: AppColors.white,
        ),
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: Paddings.verticalPadding(context),
                child: TextBuilder(
                  text: "${LocaleKeys.youMeditatedFor.tr}..",
                  maxWidth: AppSizes.width(context),
                  maxLines: 2,
                  // textAlign: TextAlign.left,
                  maxHeight: AppSizes.height(context) * 0.04,
                  // height: AppSizes.height(context) * 0.01,
                  fontFamily: Assets.fontFamilies.alegreyaSansF,
                  fontWeight: FontWeight.normal,
                  textColor: AppColors.subtitle1,
                ),
              ),
              Container(
                padding: Paddings.verticalPadding(context),
                child: RichTextBuilder(
                  texts: [
                    TextSpan(
                      text: "${splitTime[0]} ",
                      style: const TextStyle(color: AppColors.green),
                    ),
                    TextSpan(
                      text: "${splitTime[1]} ",
                    ),
                    TextSpan(
                      text: "${splitTime[2]} ",
                      style: const TextStyle(color: AppColors.green),
                    ),
                    TextSpan(
                      text: "${splitTime[3]} ",
                    ),
                    TextSpan(
                      text: "${splitTime[4]} ",
                    ),
                    TextSpan(
                      text: "${splitTime[5]} ",
                      style: const TextStyle(color: AppColors.green),
                    ),
                    TextSpan(
                      text: splitTime[6],
                    ),
                    TextSpan(
                        text:
                            "\n${controller.superController.userModel.currentStreak} "),
                    TextSpan(text: LocaleKeys.daysInARow.tr),
                  ],
                  maxWidth: AppSizes.width(context),
                  maxLines: 3,
                  maxHeight: AppSizes.height(context) * 0.2,
                  textAlign: TextAlign.start,
                  fontFamily: Assets.fontFamilies.alegreyaSansF,
                  fontWeight: FontWeight.normal,
                  textColor: AppColors.subtitle1,
                ),
              ),
              GetBuilder<MeditateController>(
                id: GetxIds.selectedFeelingAfter,
                builder: (controller) => AnimatedSwitcher(
                  duration: Durations.animationDurationLong,
                  child: !controller.didSelectFeelingAfter
                      ? Container(
                          padding: Paddings.verticalPadding(context),
                          child: TextBuilder(
                            text: controller.didSelectFeelingAfter
                                ? ""
                                : LocaleKeys.howDoYouFeelAfter.tr,
                            maxWidth: AppSizes.width(context) * 0.02,
                            maxLines: 1,
                            maxHeight: AppSizes.height(context) * 0.15,
                            textAlign: TextAlign.start,
                            fontFamily: Assets.fontFamilies.alegreyaSansF,
                            fontWeight: FontWeight.normal,
                            textColor: AppColors.subtitle1,
                          ),
                        )
                      : const SizedBox(),
                ),
              ),
              Container(
                padding: Paddings.verticalPadding(context) * 5,
                child: GetBuilder<MeditateController>(
                  id: GetxIds.selectedFeelingAfter,
                  builder: (controller) => AnimatedSwitcher(
                    duration: Durations.animationDurationLong,
                    child: controller.didSelectFeelingAfter
                        ? RichTextBuilder(
                            // width: AppSizes.width(context),
                            maxWidth: AppSizes.width(context),
                            texts: [
                              TextSpan(text: LocaleKeys.youSaid.tr),
                              Get.locale?.languageCode != "ar"
                                  ? const TextSpan(text: " ")
                                  : const TextSpan(),
                              TextSpan(text: "${controller.feelingAfter}."),
                              TextSpan(text: controller.feelingsReponseAfter),
                            ],
                            fontFamily: Assets.fontFamilies.alegreyaSansF,
                            maxLines: 4,
                            fontWeight: FontWeight.normal,
                            textColor: AppColors.subtitle1,
                            // height: AppSizes.height(context) * 0.01,
                            maxHeight: AppSizes.height(context) * 0.1,
                          )
                        : const FeelingBoxes(beforeMeditation: false),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    ]);
  }
}
