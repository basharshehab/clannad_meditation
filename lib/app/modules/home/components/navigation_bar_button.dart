import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/constants/durations.dart';
import 'package:clannad/app/modules/home/controllers/home_controller.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/state_manager.dart';

class BottomNavigationBarButton extends GetView<HomeController> {
  const BottomNavigationBarButton({
    Key? key,
    required this.assetActive,
    required this.assetInActive,
    required this.iconHeight,
    required this.index,
  }) : super(key: key);

  final String assetActive;
  final String assetInActive;
  final double iconHeight;
  final int index;
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (controller) => GestureDetector(
        // highlightColor: AppColors.red,
        // focusColor: AppColors.red,
        // hoverColor: AppColors.red,
        // // color: AppColors.red,
        // splashColor: AppColors.red,
        behavior: HitTestBehavior.opaque,
        onTap: () {
          controller.updateIndex(index);
        },
        child: SizedBox(
          width: AppSizes.width(context) * 0.333,
          child: AnimatedSwitcher(
            duration: Durations.animationDurationLong,
            child: controller.currentIndex == index
                ? SvgPicture.asset(
                    assetActive,
                    // Assets.bottomBar.bottomBarHomeActive,
                    height: iconHeight,
                    // AppSizes.height(context) * 0.04,
                    color: AppColors.green,
                    key: const ValueKey("active"),
                  )
                : SvgPicture.asset(
                    assetInActive,
                    // Assets.bottomBar.bottomBarHomeInActive,
                    height: iconHeight,
                    color: AppColors.inActive,
                    key: const ValueKey("inActive"),
                  ),
          ),
        ),
      ),
    );
  }
}
