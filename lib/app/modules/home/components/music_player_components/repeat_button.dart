import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/constants/durations.dart';
import 'package:clannad/app/data/constants/getx_ids.dart';
import 'package:clannad/app/modules/home/controllers/music_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/state_manager.dart';

class RepeatButton extends StatelessWidget {
  const RepeatButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MusicController>(
      id: GetxIds.repeat,
      builder: (controller) => Expanded(
        child: GestureDetector(
          onTap: controller.toggleRepeat,
          child: Container(
            decoration: const BoxDecoration(shape: BoxShape.circle),
            alignment: Alignment.center,
            height: AppSizes.appBar(context),
            child: AnimatedSwitcher(
              duration: Durations.animationDurationLong,
              child: controller.repeatAll
                  ? SvgPicture.asset(
                      Assets.musicControls.repeat,
                      // package: 'audio_wave_progress',
                      color: AppColors.bottomIconActive,
                      key: const ValueKey("repeat_on"),
                    )
                  : controller.repeatOne
                      ? SvgPicture.asset(
                          Assets.musicControls.repeatOne,
                          // package: 'audio_wave_progress',
                          color: AppColors.bottomIconActive,
                          key: const ValueKey("repeat_one"),
                        )
                      : SvgPicture.asset(
                          Assets.musicControls.repeat,
                          // package: 'audio_wave_progress',
                          color: AppColors.audioPlayerInActive,
                          key: const ValueKey("repeat_off"),
                        ),
            ),
          ),
        ),
      ),
    );
  }
}
