import 'dart:math';

import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/constants/getx_ids.dart';
import 'package:clannad/app/data/constants/paddings.dart';
import 'package:clannad/app/data/general_components/text_builder.dart';
import 'package:clannad/app/modules/home/components/music_player_components/music_player_controls.dart';
import 'package:clannad/app/modules/home/controllers/music_controller.dart';
import 'package:clannad/app/services/layout_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AudioPlayerWaveProgress extends GetView<MusicController> {
  // final double progressBarHeight;
  final List<double> listOfHeights;
  final Color playedColor;
  final Color unplayedColor;
  final double playerWidth;

  final Color enabledButtonsColor;
  final Color grayedButtonsColor;
  final double? spaceBetweenProgressAndPlayer;
  final double? buttonMargin;

  final double? playButtonSize;
  const AudioPlayerWaveProgress({
    Key? key,
    required this.listOfHeights,
    required this.playedColor,
    required this.unplayedColor,
    required this.playerWidth,
    required this.enabledButtonsColor,
    required this.grayedButtonsColor,
    this.spaceBetweenProgressAndPlayer,
    this.buttonMargin,
    this.playButtonSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    controller.generateAudioBars(
        listOfHeights, playedColor, unplayedColor, playerWidth);
    return Directionality(
      textDirection: TextDirection.ltr,
      child: Align(
        alignment: Alignment.bottomCenter,
        // width: playerWidth,
        child: AnimatedBuilder(
          animation: controller,
          builder: (context, child) => Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(bottom: Paddings.horizontal(context)),
                child: Column(
                  children: [
                    Container(
                      padding:
                          EdgeInsets.only(bottom: Paddings.horizontal(context)),
                      child: Column(
                        children: [
                          TextBuilder(
                            maxWidth: AppSizes.width(context) * 0.6,
                            text: controller.currentTrack.value.title,
                            fontFamily: Assets.fontFamilies.alegreyaF,
                            maxLines: 1,
                            fontWeight: FontWeight.normal,
                            textColor: AppColors.black,
                            maxHeight: AppSizes.height(context) * 0.06,
                          ),
                          TextBuilder(
                            maxWidth: AppSizes.width(context) * 0.4,
                            text: controller.currentTrack.value.artist,
                            fontFamily: Assets.fontFamilies.alegreyaSansF,
                            maxLines: 1,
                            fontWeight: FontWeight.normal,
                            textColor: AppColors.subtitle3,
                            maxHeight: AppSizes.height(context) * 0.04,
                          ),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onHorizontalDragUpdate: controller.jumpToOnDrag,
                      // onHorizontalDragEnd: controller.skipOrNot,
                      child: SizedBox(
                        height: listOfHeights.reduce(max),
                        width: playerWidth,
                        child: Row(children: controller.bars),
                      ),
                    ),
                    Container(
                      padding: Paddings.allPadding(context),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GetBuilder<MusicController>(
                            id: GetxIds.currnetSongProgressDuration,
                            builder: (controller) => TextBuilder(
                              maxWidth: AppSizes.width(context) * 0.5,
                              text: LayoutHelper
                                  .durationToMinutesSecondsDoubleDigits(
                                      controller.audioPlayer.position),
                              fontFamily: Assets.fontFamilies.alegreyaSansF,
                              maxLines: 1,
                              fontWeight: FontWeight.bold,
                              textColor: AppColors.subtitle1_50,
                              maxHeight: AppSizes.height(context) * 0.02,
                            ),
                          ),
                          GetBuilder<MusicController>(
                            id: GetxIds.maxSongDuration,
                            builder: (controller) => TextBuilder(
                              maxWidth: AppSizes.width(context) * 0.1,
                              text: controller.audioPlayer.duration == null
                                  ? "00:00"
                                  : LayoutHelper
                                      .durationToMinutesSecondsDoubleDigits(
                                          controller.audioPlayer.duration!),
                              fontFamily: Assets.fontFamilies.alegreyaSansF,
                              maxLines: 1,
                              fontWeight: FontWeight.bold,
                              textColor: AppColors.subtitle1_50,
                              maxHeight: AppSizes.height(context) * 0.02,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.1,
                padding:
                    EdgeInsets.only(top: spaceBetweenProgressAndPlayer ?? 0),
                child: MusicPlayerControls(
                  // flipCardKey: controller.normalPlayerCardKey,
                  // cardController: controller.cardController,
                  playButtonSize: playButtonSize,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
