import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/constants/durations.dart';
import 'package:clannad/app/data/constants/getx_ids.dart';
import 'package:clannad/app/modules/home/controllers/music_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/state_manager.dart';

class MusicControlButton extends StatelessWidget {
  const MusicControlButton({
    Key? key,
    required this.builderId,
    this.onTap,
    required this.asset,
    // required this.buttonName;
  }) : super(key: key);
  final Object builderId;
  final Function()? onTap;
  final String asset;
  // final String buttonName;
  @override
  Widget build(BuildContext context) {
    return GetBuilder<MusicController>(
      id: builderId,
      builder: (controller) => Expanded(
        child: GestureDetector(
          onTap: onTap,
          child: Container(
            ////padding: const EdgeInsets.all(10),
            // margin: buttonMargin,
            height: AppSizes.appBar(context),
            decoration: const BoxDecoration(shape: BoxShape.circle),
            // margin: EdgeInsets.only(left: buttonMargin ?? 0),
            alignment: Alignment.center,
            child: AnimatedSwitcher(
              duration: Durations.animationDurationLong,
              child: builderId == GetxIds.shuffle
                  ? controller.shuffleOn
                      ? SvgPicture.asset(
                          asset,
                          color: AppColors.bottomIconActive,
                          key: const ValueKey("button_on"),
                        )
                      : SvgPicture.asset(
                          asset,
                          color: AppColors.audioPlayerInActive,
                          key: const ValueKey("button_off"),
                        )
                  : SvgPicture.asset(
                      asset,
                      color: AppColors.audioPlayerInActive,
                    ),
            ),
          ),
        ),
      ),
    );
  }
}
