import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/getx_ids.dart';
import 'package:clannad/app/modules/home/components/music_player_components/music_control_button.dart';
import 'package:clannad/app/modules/home/components/music_player_components/play_pause_button.dart';
import 'package:clannad/app/modules/home/components/music_player_components/repeat_button.dart';
import 'package:clannad/app/modules/home/controllers/music_controller.dart';

import 'package:flutter/widgets.dart';
import 'package:get/state_manager.dart';

class MusicPlayerControls extends GetView<MusicController> {
  const MusicPlayerControls({
    Key? key,
    required this.playButtonSize,
    // required this.cardController,
  }) : super(key: key);

  final double? playButtonSize;
  // final FlipCardController cardController;
  @override
  Widget build(BuildContext context) {
    return Align(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          MusicControlButton(
            asset: Assets.musicControls.shuffle,
            builderId: GetxIds.shuffle,
            onTap: controller.toggleShuffle,
          ),
          MusicControlButton(
            asset: Assets.musicControls.skipBack,
            builderId: GetxIds.skipBack,
            onTap: controller.skipBack,
          ),
          const PlayPauseButton(),
          MusicControlButton(
            asset: Assets.musicControls.skipAhead,
            builderId: GetxIds.skipAhead,
            onTap: controller.skipAhead,
          ),
          const RepeatButton(),
        ],
      ),
    );
  }
}
