import 'package:clannad/app/data/constants/getx_ids.dart';
import 'package:clannad/app/modules/home/controllers/music_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';

class PlayPauseButton extends GetView<MusicController> {
  const PlayPauseButton({
    Key? key,
    // required this.playButtonSize,
  }) : super(key: key);

  // final double? playButtonSize;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: controller.togglePlay,
      child: Container(
        alignment: Alignment.center,
        decoration: const BoxDecoration(
          color: Color.fromARGB(255, 97, 177, 90),
          shape: BoxShape.circle,
        ),
        height: /*playButtonSize ??**/ MediaQuery.of(context).size.height *
            0.092,
        width: /*playButtonSize ??**/ MediaQuery.of(context).size.height *
            0.092,
        child: GetBuilder<MusicController>(
          id: GetxIds.play,
          builder: (controller) => Stack(
            children: [
              AnimatedCard(
                animation: controller.frontRotation,
                child: Icon(
                  Icons.play_arrow_rounded,
                  size: MediaQuery.of(context).size.height * 0.046,
                  color: const Color.fromARGB(255, 254, 254, 254),
                  key: const ValueKey("paused"),
                ),
              ),
              AnimatedCard(
                animation: controller.backRotation,
                child: Icon(
                  Icons.pause_rounded,
                  size: MediaQuery.of(context).size.height * 0.046,
                  color: const Color.fromARGB(255, 254, 254, 254),
                  key: const ValueKey("playing"),
                ),
              ),
            ],
            // alignment: FractionalOffset.center,
            // child: controller.animationController.value <= 0.5
            // ?
          ),
        ),
      ),
    );
  }
}

class AnimatedCard extends StatelessWidget {
  const AnimatedCard({
    super.key,
    required this.child,
    required this.animation,
  });

  final Widget child;
  final Animation<double> animation;

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animation,
      builder: (BuildContext context, Widget? child) {
        var transform = Matrix4.identity();
        transform.setEntry(3, 2, 0.001);
        transform.rotateY(animation.value);
        return Transform(
          transform: transform,
          alignment: Alignment.center,
          child: child,
        );
      },
      child: child,
    );
  }
}
