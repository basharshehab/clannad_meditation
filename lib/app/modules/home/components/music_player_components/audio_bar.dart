import 'package:clannad/app/data/constants/durations.dart';
import 'package:clannad/app/data/constants/getx_ids.dart';
import 'package:clannad/app/modules/home/controllers/music_controller.dart';
// import 'package:clannad/app/modules/home/controllers/player_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AudioBar extends GetWidget<MusicController> {
  final double height;
  final double width;
  final Color playedColor;
  final Color unplayedColor;
  final int index;
  final double playerWidth;
  const AudioBar({
    Key? key,
    required this.height,
    required this.playedColor,
    required this.unplayedColor,
    required this.index,
    required this.width,
    required this.playerWidth,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => controller.jumpToOnClick(index),
      child: GetBuilder<MusicController>(
        id: GetxIds.audioBar,
        builder: (controller) => AnimatedSwitcher(
          duration: Durations.animationDurationLong,
          child: controller.currentlyAt >= index
              ? Container(
                  height: height,
                  width: width,
                  margin: EdgeInsets.only(right: playerWidth * 0.0075),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    color: playedColor,
                  ),
                  key: const ValueKey("played_bar"),
                )
              : Container(
                  height: height,
                  width: width,
                  margin: EdgeInsets.only(right: playerWidth * 0.0075),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    color: unplayedColor,
                  ),
                  key: const ValueKey("unplayed_bar"),
                ),
        ),
      ),
    );
  }
}
