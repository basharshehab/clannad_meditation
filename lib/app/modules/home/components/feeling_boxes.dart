import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/modules/home/components/feelings_box.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class FeelingBoxes extends StatelessWidget {
  final bool beforeMeditation;
  const FeelingBoxes({
    Key? key,
    required this.beforeMeditation,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        FeelingsBox(
          feeling: LocaleKeys.feelingOkay.tr,
          image: SvgPicture.asset(Assets.feelings.feelingCalmSvg),
          beforeMeditaion: beforeMeditation,
        ),
        FeelingsBox(
          beforeMeditaion: beforeMeditation,
          feeling: LocaleKeys.feelingRelaxed.tr,
          image: SvgPicture.asset(Assets.feelings.feelingRelax),
        ),
        FeelingsBox(
          feeling: LocaleKeys.feelingFocused.tr,
          beforeMeditaion: beforeMeditation,
          image: SvgPicture.asset(Assets.feelings.feelingFocus),
        ),
        FeelingsBox(
          feeling: LocaleKeys.feelingAnxious.tr,
          beforeMeditaion: beforeMeditation,
          image: SvgPicture.asset(Assets.feelings.feelingAnxiousSvg),
        ),
      ],
    );
  }
}
