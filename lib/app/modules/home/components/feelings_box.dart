import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/constants/paddings.dart';
import 'package:clannad/app/data/general_components/text_builder.dart';
import 'package:clannad/app/modules/home/controllers/welcome_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class FeelingsBox extends GetView<WelcomeController> {
  final SvgPicture image;
  final String feeling;
  final bool beforeMeditaion;
  const FeelingsBox({
    Key? key,
    required this.image,
    required this.feeling,
    required this.beforeMeditaion,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: AppSizes.width(context) / 6,
          width: AppSizes.width(context) / 6,
          alignment: Alignment.center,
          child: GestureDetector(
            onTap: () => beforeMeditaion
                ? controller.updateFeelingHistoryBeforeAndFeeling(feeling)
                : controller.meditateController
                    .updateFeelingHistoryAfterAndFeeling(feeling),
            child: Stack(
              children: [
                Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: AppColors.lightGreen,
                    borderRadius: BorderRadius.circular(20),
                    // image: DecorationImage(image: SvgPicture)
                  ),
                ),
                Container(alignment: Alignment.center, child: image),
              ],
            ),
          ),
        ),
        Container(
          padding: Paddings.verticalPadding(context),
          child: TextBuilder(
              maxWidth: AppSizes.width(context) / 2,
              text: feeling,
              fontFamily: Assets.fontFamilies.alegreyaSansF,
              maxLines: 1,
              fontWeight: FontWeight.normal,
              textColor: AppColors.description,
              maxHeight: AppSizes.width(context) / 24),
        )
      ],
    );
  }
}
