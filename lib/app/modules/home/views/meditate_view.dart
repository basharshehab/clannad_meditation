import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/constants/durations.dart';
import 'package:clannad/app/data/constants/getx_ids.dart';
import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/data/constants/paddings.dart';
import 'package:clannad/app/data/general_components/button_builder.dart';
import 'package:clannad/app/data/general_components/text_builder.dart';
import 'package:clannad/app/modules/home/controllers/meditate_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class MeditateView extends GetView<MeditateController> {
  const MeditateView({super.key});

  @override
  Widget build(BuildContext context) {

    return AnimatedBuilder(
      animation: controller,
      builder: (context, child) {
        return Container(
          padding: EdgeInsets.symmetric(
            horizontal: Paddings.horizontal(context),
            vertical: Paddings.vertical(context) * 8,
          ),
          decoration: const BoxDecoration(
            color: AppColors.white,
          ),
          child: Column(
            children: [
              TextBuilder(
                maxWidth: AppSizes.width(context),
                text: LocaleKeys.meditation.tr,
                fontFamily: Assets.fontFamilies.alegreyaF,
                maxLines: 1,
                fontWeight: FontWeight.normal,
                textColor: AppColors.subtitle1,
                maxHeight: AppSizes.height(context) * 0.065,
              ),
              TextBuilder(
                textAlign: TextAlign.center,
                maxWidth: AppSizes.width(context),
                text: LocaleKeys.meditationSubtitle.tr,
                fontFamily: Assets.fontFamilies.alegreyaSansF,
                maxLines: 2,
                fontWeight: FontWeight.normal,
                textColor: AppColors.subtitle1_50,
                maxHeight: AppSizes.height(context) * 0.06,
              ),
              Flexible(
                child: Container(
                  padding: EdgeInsets.only(
                    top: Paddings.vertical(context) * 6,
                    bottom: Paddings.vertical(context) * 6,
                  ),
                  child: SvgPicture.asset(
                    Assets.characters.characterSvg1,
                  ),
                ),
              ),
              GetBuilder<MeditateController>(
                builder: (controller) {
                  return Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                            bottom: Paddings.vertical(context) * 2),
                        child: TextBuilder(
                          maxWidth: AppSizes.width(context) * 0.5,
                          text: controller.getDurationMinutesSeconds(),
                          fontFamily: Assets.fontFamilies.alegreyaSansF,
                          maxLines: 1,
                          fontWeight: FontWeight.normal,
                          textColor: AppColors.subtitle2,
                          maxHeight: AppSizes.height(context) * 0.06,
                        ),
                      ),
                      AnimatedSwitcher(
                        switchInCurve: Curves.linear.flipped,
                        switchOutCurve: Curves.linear,
                        duration: Durations.animationDurationLong,
                        child: controller.getStopwatchIsRunning()
                            ? ButtonBuilder(
                                backgroundColor: AppColors.green,
                                padding: EdgeInsets.symmetric(
                                  vertical: Paddings.vertical(context) * 3.25,
                                  horizontal:
                                      Paddings.horizontal(context) * 1.8,
                                ),
                                borderRadius: 10,
                                width: AppSizes.width(context) * 0.3,
                                text: TextBuilder(
                                  textAlign: TextAlign.center,
                                  maxWidth: AppSizes.width(context) * 0.1,
                                  text: LocaleKeys.stop.tr,
                                  fontFamily: Assets.fontFamilies.alegreyaSansF,
                                  maxLines: 1,
                                  fontWeight: FontWeight.w500,
                                  textColor: AppColors.white,
                                  maxHeight: AppSizes.height(context) * 0.038,
                                ),
                                onPressed: () =>
                                    controller.stopStopwatch(context),
                                key: const ValueKey("started"),
                              )
                            : ButtonBuilder(
                                backgroundColor: AppColors.green,
                                padding: EdgeInsets.symmetric(
                                  vertical: Paddings.vertical(context) * 3.25,
                                  horizontal:
                                      Paddings.horizontal(context) * 1.8,
                                ),
                                borderRadius: 10,
                                width: AppSizes.width(context) * 0.3,
                                text: TextBuilder(
                                  textAlign: TextAlign.center,
                                  maxWidth: AppSizes.width(context) * 0.1,
                                  text: LocaleKeys.startNow.tr,
                                  fontFamily: Assets.fontFamilies.alegreyaSansF,
                                  maxLines: 1,
                                  fontWeight: FontWeight.w500,
                                  textColor: AppColors.white,
                                  maxHeight: AppSizes.height(context) * 0.038,
                                ),
                                onPressed: controller.startStopwatch,
                                key: const ValueKey("stopped"),
                              ),
                      ),
                    ],
                  );
                },
                id: GetxIds.stopwatch,
              ),
            ],
          ),
        );
      },
    );
  }
}
