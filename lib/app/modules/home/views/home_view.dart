import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/modules/drawer/views/clannad_drawer.dart';
import 'package:clannad/app/modules/home/components/clannad_navigation_bar.dart';
import 'package:clannad/app/data/general_components/clannad_app_bar.dart';
import 'package:clannad/app/modules/home/views/meditate_view.dart';
import 'package:clannad/app/modules/home/views/music_view.dart';
import 'package:clannad/app/modules/home/views/welcome_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
// import 'package:flutter_svg/svg.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    precacheImage(AssetImage(Assets.logoArt.fullArt), context);
    precachePicture(
        ExactAssetPicture(
          SvgPicture.svgStringDecoderBuilder,
          Assets.characters.characterSvg1,
        ),
        context);
    return Scaffold(
      drawerEnableOpenDragGesture: false,
      key: controller.scaffoldKey,
      appBar: ClannadAppBar(
        height: AppSizes.height(context) * 0.11,
        // bContext: context,
        scaffoldKey: controller.scaffoldKey,
      ),
      drawer: ClannadDrawer(
        scaffoldKey: controller.scaffoldKey,
      ),
      body: PageView(
        key: const ValueKey("pageview"),
        onPageChanged: controller.updateIndexNoMove,
        controller: controller.pageController,
        physics: const BouncingScrollPhysics(),
        children: const [
          WelcomeView(),
          MusicView(),
          MeditateView(),
        ],
      ),
      bottomNavigationBar: const ClannadNavigationBar(),
    );
  }
}
