import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/paddings.dart';
import 'package:clannad/app/modules/home/controllers/music_controller.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';
import 'package:flutter/material.dart';

class MusicView extends GetView<MusicController> {
  const MusicView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: Paddings.horizontalPadding(context),
      decoration: const BoxDecoration(
        color: AppColors.white,
      ),
      child: ShaderMask(
        shaderCallback: (Rect bounds) {
          return const LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              // Colors.purple,
              AppColors.transparent,
              AppColors.transparent,
              AppColors.description,
            ],
            stops: [
              // 0.0,
              0.1,
              0.95,
              1.0
            ], // 10% purple, 80% transparent, 10% purple
          ).createShader(bounds);
        },
        blendMode: BlendMode.dstOut,
        child: AnimatedList(
          key: controller.animatedPlaylistKey,
          initialItemCount: controller.musicTilesWidgets.length,
          physics: const BouncingScrollPhysics(),
          itemBuilder: (context, index, animation) {
            return FadeTransition(
              opacity: animation.drive(controller.fadeAnimationTween),
              child: controller.musicTilesWidgets[index],
            );
          },
        ),
      ),
    );
  }
}
