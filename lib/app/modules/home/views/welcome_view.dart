import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/constants/durations.dart';
import 'package:clannad/app/data/constants/getx_ids.dart';
import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/data/constants/paddings.dart';
import 'package:clannad/app/data/general_components/rich_text_builder.dart';
import 'package:clannad/app/modules/home/components/course_builder.dart';
import 'package:clannad/app/modules/home/components/feeling_boxes.dart';
import 'package:clannad/app/modules/home/controllers/welcome_controller.dart';
import 'package:clannad/app/modules/splash/controllers/super_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class WelcomeView extends GetView<WelcomeController> {
  const WelcomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: controller,
      builder: (context, child) => Container(
        padding: EdgeInsets.symmetric(
          horizontal: Paddings.horizontal(context),
          vertical: Paddings.vertical(context) * 5,
        ),
        color: AppColors.white,
        child: ShaderMask(
          shaderCallback: (Rect bounds) {
            return const LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                AppColors.transparent,
                AppColors.transparent,
                AppColors.white,
              ],
              stops: [0.0, 0.95, 1.0], // 95% transparent, 5% white
            ).createShader(bounds);
          },
          blendMode: BlendMode.dstOut,
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GetBuilder<Super>(
                  id: GetxIds.showsEditableUserData,
                  builder: (controller) => RichTextBuilder(
                    // width: AppSizes.width(context),
                    maxWidth: AppSizes.width(context),
                    texts: [
                      TextSpan(text: "${LocaleKeys.welcomeBack.tr}, "),
                      TextSpan(text: controller.userModel.name),
                    ],
                    fontFamily: Assets.fontFamilies.alegreyaF,
                    maxLines: 2,
                    fontWeight: FontWeight.normal,
                    textColor: AppColors.black,
                    // height: AppSizes.height(context) / 19,
                    maxHeight: AppSizes.height(context) * 0.07,
                  ),
                ),
                GetBuilder<WelcomeController>(
                  builder: (x) => AnimatedSwitcher(
                    duration: Durations.animationDurationLong,
                    child: controller.didSelectFeelingToday
                        ? const SizedBox()
                        : RichTextBuilder(
                            // width: AppSizes.width(context),
                            maxWidth: AppSizes.width(context),
                            texts: [
                              TextSpan(text: LocaleKeys.howDoYouFeel.tr),
                            ],
                            fontFamily: Assets.fontFamilies.alegreyaSansF,
                            maxLines: 1,
                            fontWeight: FontWeight.normal,
                            textColor: AppColors.subtitle1,
                            // height: AppSizes.height(context) / 27,
                            maxHeight: AppSizes.height(context) / 27,
                          ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: Paddings.vertical(context) * 5),
                  child: GetBuilder<WelcomeController>(
                    builder: (x) => AnimatedSwitcher(
                      duration: Durations.animationDurationLong,
                      child: controller.didSelectFeelingToday
                          ? RichTextBuilder(
                              // width: AppSizes.width(context),
                              maxWidth: AppSizes.width(context),
                              texts: [
                                TextSpan(text: LocaleKeys.youSaid.tr),
                                Get.locale?.languageCode != "ar"
                                    ? const TextSpan(text: " ")
                                    : const TextSpan(),
                                TextSpan(text: "${controller.feelingToday}."),
                                TextSpan(
                                    text: controller.feelingsResponseToday),
                              ],
                              fontFamily: Assets.fontFamilies.alegreyaSansF,
                              maxLines: 3,
                              fontWeight: FontWeight.normal,
                              textColor: AppColors.subtitle1,
                              // height: AppSizes.height(context) / 12,
                              maxHeight: AppSizes.height(context) / 12,
                            )
                          : const FeelingBoxes(beforeMeditation: true),
                    ),
                  ),
                ),
                GetBuilder<WelcomeController>(
                  id: GetxIds.courseAdsList,
                  builder: (_) => Container(
                    padding: Paddings.verticalPadding(context) * 5,
                    child: controller.courses.isNotEmpty
                        ? ListView.builder(
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: controller.courses.length,
                            itemBuilder: (context, index) => CourseWidget(
                              courseDescription:
                                  controller.courses[index].description,
                              coursePicture: controller.courses[index].art,
                              courseTitle: controller.courses[index].title,
                              url: controller.courses[index].url,
                            ),
                          )
                        : const SizedBox(),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
