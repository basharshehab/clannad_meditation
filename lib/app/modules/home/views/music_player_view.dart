import 'package:cached_network_image/cached_network_image.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/constants/durations.dart';
import 'package:clannad/app/data/constants/getx_ids.dart';
import 'package:clannad/app/data/constants/paddings.dart';
import 'package:clannad/app/modules/home/components/music_player_components/audio_player_wave_progress.dart';
import 'package:clannad/app/modules/home/controllers/music_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MusicPlayerView extends GetView<MusicController> {
  const MusicPlayerView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FractionallySizedBox(
      heightFactor: 1,
      child: Container(
        decoration: const BoxDecoration(color: AppColors.white),
        height: AppSizes.height(context),
        padding: EdgeInsets.only(
          top: Paddings.horizontal(context) * 2,
          bottom: AppSizes.bottomBar(context) * 0.5,
        ),
        child: GetBuilder<MusicController>(
          id: GetxIds.musicPlayer,
          builder: (controller) => AnimatedSwitcher(
            duration: Durations.transitionDuration,
            layoutBuilder: (currentChild, previousChildren) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    height: AppSizes.height(context) * 0.4,
                    width: AppSizes.height(context) * 0.4,
                    padding: EdgeInsets.symmetric(
                      vertical: Paddings.vertical(context) * 2,
                      // horizontal: Paddings.vertical(context),
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                    ),
                    child: CircleAvatar(
                      backgroundImage: CachedNetworkImageProvider(
                          controller.currentTrack.value.art
                          // Assets.backgrounds.courseBackgrounds.courseBackground2,
                          ),
                    ),
                  ),

                  // Container(
                  //   padding: EdgeInsets.only(
                  //     top: Paddings.vertical(context) * 5,
                  //   ),
                  // ),
                  AudioPlayerWaveProgress(
                    listOfHeights: [
                      AppSizes.height(context) * 0.015,
                      AppSizes.height(context) * 0.03,
                      AppSizes.height(context) * 0.05,
                      AppSizes.height(context) * 0.03,
                      AppSizes.height(context) * 0.015,
                      AppSizes.height(context) * 0.015,
                      AppSizes.height(context) * 0.03,
                      AppSizes.height(context) * 0.05,
                      AppSizes.height(context) * 0.03,
                      AppSizes.height(context) * 0.015,
                      AppSizes.height(context) * 0.015,
                      AppSizes.height(context) * 0.03,
                      AppSizes.height(context) * 0.05,
                      AppSizes.height(context) * 0.03,
                      AppSizes.height(context) * 0.015,
                      AppSizes.height(context) * 0.015,
                      AppSizes.height(context) * 0.03,
                      AppSizes.height(context) * 0.05,
                      AppSizes.height(context) * 0.03,
                      AppSizes.height(context) * 0.015,
                      AppSizes.height(context) * 0.015,
                      AppSizes.height(context) * 0.03,
                      AppSizes.height(context) * 0.05,
                      AppSizes.height(context) * 0.03,
                      AppSizes.height(context) * 0.015,
                      AppSizes.height(context) * 0.015,
                      AppSizes.height(context) * 0.03,
                      AppSizes.height(context) * 0.05,
                      AppSizes.height(context) * 0.03,
                      AppSizes.height(context) * 0.015,
                      AppSizes.height(context) * 0.015,
                      AppSizes.height(context) * 0.03,
                      AppSizes.height(context) * 0.05,
                      AppSizes.height(context) * 0.03,
                      AppSizes.height(context) * 0.015,
                      AppSizes.height(context) * 0.015,
                      AppSizes.height(context) * 0.03,
                      AppSizes.height(context) * 0.05,
                      AppSizes.height(context) * 0.03,
                      AppSizes.height(context) * 0.015,
                      AppSizes.height(context) * 0.015,
                      AppSizes.height(context) * 0.03,
                      AppSizes.height(context) * 0.05,
                      AppSizes.height(context) * 0.03,
                      AppSizes.height(context) * 0.015,
                      AppSizes.height(context) * 0.015,
                      AppSizes.height(context) * 0.03,
                      AppSizes.height(context) * 0.05,
                      AppSizes.height(context) * 0.03,
                      AppSizes.height(context) * 0.015,
                    ],
                    playedColor: AppColors.green,
                    unplayedColor: AppColors.unplayedBars,
                    playerWidth: AppSizes.width(context) * 0.8,
                    enabledButtonsColor: AppColors.bottomIconActive,
                    grayedButtonsColor: AppColors.audioPlayerInActive,
                    buttonMargin: AppSizes.width(context) * 0.1,
                    spaceBetweenProgressAndPlayer: 0,
                    // playButtonSize: AppSizes.width(context) * 0.1,
                  ),
                  const SizedBox()
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
