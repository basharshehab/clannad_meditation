import 'package:clannad/app/modules/profile/controllers/achievements_controller.dart';
import 'package:clannad/app/modules/profile/controllers/stats_controller.dart';
import 'package:get/get.dart';

import '../controllers/profile_controller.dart';

class ProfileBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ProfileController>(
      () => ProfileController(),
    );
    Get.lazyPut<AchievementsController>(
      () => AchievementsController(),
    );
    Get.lazyPut<StatsController>(
      () => StatsController(),
    );
  }
}
