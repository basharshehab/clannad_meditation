import 'package:cached_network_image/cached_network_image.dart';
import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/constants/durations.dart';
import 'package:clannad/app/data/constants/getx_ids.dart';
import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/data/constants/paddings.dart';
import 'package:clannad/app/data/general_components/clannad_app_bar.dart';
import 'package:clannad/app/modules/drawer/views/clannad_drawer.dart';
import 'package:clannad/app/data/general_components/text_builder.dart';
import 'package:clannad/app/modules/profile/components/achievements.dart';
import 'package:clannad/app/modules/profile/components/stats.dart';
import 'package:clannad/app/modules/splash/controllers/super_controller.dart';
import 'package:clannad/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/profile_controller.dart';

class ProfileView extends GetView<ProfileController> {
  const ProfileView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => controller.backButtonPressed(context),
      child: Scaffold(
        key: controller.scaffoldKey,
        drawerEnableOpenDragGesture: false,
        appBar: ClannadAppBar(
            // bContext: context,
            height: AppSizes.appBar(context),
            scaffoldKey: controller.scaffoldKey),
        backgroundColor: AppColors.white,
        drawerScrimColor: AppColors.white,
        drawer: ClannadDrawer(scaffoldKey: controller.scaffoldKey),
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            // mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Stack(
                children: [
                  Hero(
                    tag: Routes.PROFILE,
                    child: GetBuilder<Super>(
                      id: GetxIds.showsEditableUserData,
                      builder: (_) => Container(
                        height: AppSizes.width(context) * 0.4,
                        width: AppSizes.width(context) * 0.4,
                        margin: EdgeInsets.only(
                            top: Paddings.vertical(context) * 6),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            image:
                                controller.superController.userModel.imageUrl ==
                                            "" ||
                                        controller.superController.userModel
                                                .imageUrl ==
                                            ""
                                    ? AssetImage(
                                        Assets.logoArt.fullArt,
                                      )
                                    : CachedNetworkImageProvider(
                                        controller
                                            .superController.userModel.imageUrl,
                                      ) as ImageProvider,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                    ),
                  ),
                  GetBuilder<ProfileController>(
                    id: GetxIds.showsEditableUserData,
                    builder: (controller) => Positioned(
                      bottom: 0,
                      right: 0,
                      child: AnimatedSwitcher(
                        duration: Durations.animationDurationShort,
                        child: controller.editMode
                            ? EditModeEditPictureButton(
                                controller: controller,
                              )
                            : const SizedBox(),
                      ),
                    ),
                  ),
                ],
              ),
              GetBuilder<ProfileController>(
                id: GetxIds.showsEditableUserData,
                builder: (_) => AnimatedSwitcher(
                  duration: Durations.animationDurationShort,
                  child: controller.editMode
                      ? EditModeEditNameWidget(controller: controller)
                      : UserNameWidget(
                          controller: controller,
                        ),
                ),
              ),
              GetBuilder<ProfileController>(
                id: GetxIds.showsEditableUserData,
                builder: (_) => AnimatedSwitcher(
                  duration: Durations.animationDurationShort,
                  child: controller.editMode
                      ? EditModeEditProfileQuote(controller: controller)
                      : UserProfileQuoteWidget(controller: controller),
                ),
              ),
              GetBuilder<ProfileController>(
                id: GetxIds.showsEditableUserData,
                builder: (controller) => controller.editMode
                    ? const SizedBox()
                    : const StatsAchievementsTabBarWidget(),
              ),
              GetBuilder<ProfileController>(
                id: GetxIds.showsEditableUserData,
                builder: (controller) => controller.editMode
                    ? const SizedBox()
                    : const StatsAchievementsWidgets(),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class StatsAchievementsWidgets extends StatelessWidget {
  const StatsAchievementsWidgets({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: SizedBox(
        height: AppSizes.height(context),
        child: GetBuilder<ProfileController>(
          id: GetxIds.displayedTab,
          builder: (controller) {
            return controller.tabController.index == 0
                ? const Stats()
                : const Achievements();
          },
        ),
      ),
    );
  }
}

class StatsAchievementsTabBarWidget extends StatelessWidget {
  const StatsAchievementsTabBarWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProfileController>(
      id: GetxIds.profileTab,
      builder: (controller) => TabBar(
        controller: controller.tabController,
        indicatorColor: AppColors.green,
        padding: EdgeInsets.only(
          top: Paddings.vertical(context) * 2,
        ),
        onTap: controller.tabChanged,
        tabs: [
          Container(
            padding: Paddings.verticalPadding(context) * 2,
            child: TextBuilder(
              maxWidth: AppSizes.width(context),
              text: LocaleKeys.stats.tr.toUpperCase(),
              fontFamily: Assets.fontFamilies.alegreyaSansF,
              maxLines: 1,
              fontWeight: FontWeight.bold,
              textColor: controller.tabController.index == 0
                  ? AppColors.green
                  : AppColors.inActive,
              maxHeight: AppSizes.height(context) * 0.025,
            ),
          ),
          Container(
            padding: Paddings.verticalPadding(context) * 2,
            child: TextBuilder(
              maxWidth: AppSizes.width(context),
              text: LocaleKeys.achievements.tr.toUpperCase(),
              fontFamily: Assets.fontFamilies.alegreyaSansF,
              maxLines: 1,
              fontWeight: FontWeight.bold,
              textColor: controller.tabController.index == 1
                  ? AppColors.green
                  : AppColors.inActive,
              maxHeight: AppSizes.height(context) * 0.025,
            ),
          ),
        ],
      ),
    );
  }
}

class UserProfileQuoteWidget extends StatelessWidget {
  const UserProfileQuoteWidget({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final ProfileController controller;

  @override
  Widget build(BuildContext context) {
    return TextBuilder(
      maxWidth: AppSizes.width(context),
      text: controller.superController.userModel.profileQuote ??
          LocaleKeys.editProfileQuoteHint.tr,
      fontFamily: Assets.fontFamilies.alegreyaSansF,
      maxLines: 3,
      fontWeight: FontWeight.normal,
      textColor: AppColors.description,
      maxHeight: AppSizes.height(context) * 0.025,
      textAlign: TextAlign.center,
    );
  }
}

class EditModeEditProfileQuote extends StatelessWidget {
  const EditModeEditProfileQuote({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final ProfileController controller;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: Paddings.horizontalPadding(context),
      child: TextField(
        maxLines: 3,
        minLines: 1,
        textAlign: TextAlign.center,
        controller: controller.quoteController,
        style: TextStyle(
          fontSize: AppSizes.height(context) * 0.02,
        ),
        decoration: InputDecoration(
          border: const UnderlineInputBorder(),
          // hintText: "Edit your profile to add an inspiring quote.",
          hintText: LocaleKeys.editProfileQuoteHint.tr,
        ),
      ),
    );
  }
}

class UserNameWidget extends StatelessWidget {
  const UserNameWidget({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final ProfileController controller;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Icon(
                  Icons.local_fire_department_sharp,
                  color: AppColors.red,
                ),
                const SizedBox(
                  width: 5,
                ),
                Text(
                  controller.superController.userModel.maxStreak.toString(),
                  style: TextStyle(
                    fontSize: AppSizes.height(context) * 0.025,
                  ),
                ),
              ],
            ),
            TextBuilder(
              // text: "Best Streak",
              text: LocaleKeys.bestStreak.tr,

              textAlign: TextAlign.center,
              fontFamily: Assets.fontFamilies.alegreyaSansF,
              fontWeight: FontWeight.w500,
              textColor: AppColors.lightBlack,
              maxLines: 2,

              maxHeight: AppSizes.height(context) * 0.04,
              maxWidth: AppSizes.width(context) * 0.15,
            ),
          ],
        ),
        Container(
          padding: EdgeInsets.only(
            top: Paddings.vertical(context),
          ),
          child: TextBuilder(
            textAlign: TextAlign.center,
            maxWidth: AppSizes.width(context) * 0.5,
            text: controller.superController.userModel.name,
            fontFamily: Assets.fontFamilies.alegreyaF,
            maxLines: 1,
            fontWeight: FontWeight.normal,
            textColor: AppColors.black,
            maxHeight: AppSizes.height(context) * 0.065,
          ),
        ),
        Column(
          children: [
            Row(
              children: [
                const Icon(
                  Icons.stacked_line_chart_rounded,
                  color: AppColors.green,
                ),
                const SizedBox(
                  width: 5,
                ),
                Text(
                  controller.superController.userModel.currentStreak.toString(),
                  style: TextStyle(
                    fontSize: AppSizes.height(context) * 0.025,
                  ),
                ),
              ],
            ),
            TextBuilder(
              text: LocaleKeys.currentStreak.tr,
              textAlign: TextAlign.center,
              fontFamily: Assets.fontFamilies.alegreyaSansF,
              fontWeight: FontWeight.w500,
              textColor: AppColors.lightBlack,
              maxLines: 2,
              maxHeight: AppSizes.height(context) * 0.04,
              maxWidth: AppSizes.width(context) * 0.15,
            ),
          ],
        ),
      ],
    );
  }
}

class EditModeEditNameWidget extends StatelessWidget {
  const EditModeEditNameWidget({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final ProfileController controller;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: Paddings.horizontalPadding(context),
      child: TextField(
        textAlign: TextAlign.center,
        controller: controller.nameController,
        style: TextStyle(
          fontSize: AppSizes.height(context) * 0.055,
        ),
        decoration: InputDecoration(
          border: const UnderlineInputBorder(),
          hintText: LocaleKeys.enterName.tr,
        ),
      ),
    );
  }
}

class EditModeEditPictureButton extends StatelessWidget {
  final ProfileController controller;
  const EditModeEditPictureButton({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => controller.popSelectImageDialog(context),
      child: Container(
        height: AppSizes.width(context) * 0.1,
        width: AppSizes.width(context) * 0.1,
        decoration: const BoxDecoration(
          color: AppColors.lightBlack,
          shape: BoxShape.circle,
        ),
        child: const Icon(
          Icons.photo_size_select_actual_rounded,
          color: AppColors.white,
        ),
      ),
    );
  }
}
