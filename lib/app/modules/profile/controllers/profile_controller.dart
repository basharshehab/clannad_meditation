import 'dart:io';

import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/constants/collections.dart';
import 'package:clannad/app/data/constants/getx_ids.dart';
import 'package:clannad/app/data/general_components/text_builder.dart';
import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/data/models/user_model.dart';
import 'package:clannad/app/modules/splash/controllers/super_controller.dart';
import 'package:clannad/app/services/navigate.dart';
import 'package:clannad/app/services/storage_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

class ProfileController extends GetxController
    with GetSingleTickerProviderStateMixin {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  late final GlobalKey<ScaffoldState> scaffoldKey;
  late final TabController tabController;
  Super get superController => Get.find<Super>();
  bool editMode = false;

  String profileQuote = "Profile Quote";
  late ImagePicker _imagePicker;
  late ImageCropper _imageCropper;
  File? userImageToUpload;
  late TextEditingController nameController;
  late TextEditingController quoteController;
  @override
  void onInit() {
    super.onInit();
    scaffoldKey = GlobalKey<ScaffoldState>();
    tabController = TabController(length: 2, vsync: this);
    _imagePicker = ImagePicker();
    _imageCropper = ImageCropper();
  }

  @override
  void onReady() {
    super.onReady();
    nameController =
        TextEditingController(text: superController.userModel.name);
    quoteController =
        TextEditingController(text: superController.userModel.profileQuote);
  }

  @override
  void onClose() {
    super.onClose();
    tabController.dispose();
    nameController.dispose();
    quoteController.dispose();
  }

  void _pickImageFromCamera() async {
    Navigate.getBack();
    // userImageToUpload = null;
    XFile? file = await _imagePicker.pickImage(source: ImageSource.camera);

    if (file == null) {
      return;
    }
    CroppedFile? croppedFile = await _imageCropper.cropImage(
      sourcePath: file.path,
      aspectRatio: const CropAspectRatio(ratioX: 1, ratioY: 1),
      cropStyle: CropStyle.circle,
      compressFormat: ImageCompressFormat.jpg,
      compressQuality: 90,
    );
    if (croppedFile == null) {
      return;
    }

    await StorageService.uploadUserImage(
        userImage: File(croppedFile.path),
        userId: _firebaseAuth.currentUser!.uid);
  }

  void _pickImageFromGallery() async {
    Navigate.getBack();
    // userImageToUpload = null;
    XFile? file = await _imagePicker.pickImage(source: ImageSource.gallery);

    if (file == null) {
      return;
    }
    CroppedFile? croppedFile = await _imageCropper.cropImage(
      sourcePath: file.path,
      aspectRatio: const CropAspectRatio(ratioX: 1, ratioY: 1),
      cropStyle: CropStyle.circle,
      compressFormat: ImageCompressFormat.jpg,
      compressQuality: 90,
    );
    if (croppedFile == null) {
      return;
    }

    await StorageService.uploadUserImage(
        userImage: File(croppedFile.path),
        userId: _firebaseAuth.currentUser!.uid);
  }

  void popSelectImageDialog(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      builder: (context) => Container(
        // height: AppSizes.height(context) * 0.3,
        padding: const EdgeInsets.only(
            bottom: kBottomNavigationBarHeight,
            top: kBottomNavigationBarHeight),
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(16.0),
            topRight: Radius.circular(16.0),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            InkWell(
              onTap: _pickImageFromCamera,
              child: SizedBox(
                height: AppSizes.height(context) * 0.1,
                child: Column(
                  children: [
                    Flexible(
                      child: Icon(
                        Icons.camera_alt_outlined,
                        size: AppSizes.height(context) * 0.06,
                        color: AppColors.green,
                        // size: 30,
                      ),
                    ),
                    TextBuilder(
                      maxWidth: AppSizes.width(context) * 0.2,
                      // text: "Camera",
                      text: LocaleKeys.camera.tr,
                      fontFamily: Assets.fontFamilies.alegreyaSansF,
                      maxLines: 1,
                      fontWeight: FontWeight.normal,
                      textColor: AppColors.black,
                      maxHeight: AppSizes.height(context) * 0.025,
                    ),
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: _pickImageFromGallery,
              child: SizedBox(
                height: AppSizes.height(context) * 0.1,
                child: Column(
                  children: [
                    Flexible(
                      child: Icon(
                        Icons.photo_album_outlined,
                        size: AppSizes.height(context) * 0.06,
                        color: AppColors.green,
                        // size: 30,
                      ),
                    ),
                    TextBuilder(
                      maxWidth: AppSizes.width(context) * 0.2,
                      // text: "Gallery",
                      text: LocaleKeys.gallery.tr,

                      fontFamily: Assets.fontFamilies.alegreyaSansF,
                      maxLines: 1,
                      fontWeight: FontWeight.normal,
                      textColor: AppColors.black,
                      maxHeight: AppSizes.height(context) * 0.025,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void tabChanged(int i) {
    update([GetxIds.profileTab, GetxIds.displayedTab]);
  }

  void toggleEditMode(BuildContext context, bool shouldSave) async {
    editMode = !editMode;

    update([GetxIds.toggleEditModeButton, GetxIds.showsEditableUserData]);

    if (!shouldSave) {
      return;
    }
    if (!await InternetConnectionChecker().hasConnection) {
      Get.snackbar(
          LocaleKeys.noConnection.tr, LocaleKeys.checkInternetConnection.tr);
      return;
    }
    UserModel newUser = superController.userModel.copyWith(
        name: nameController.text, profileQuote: quoteController.text);

    try {
      await _firestore
          .collection(Collections.users)
          .doc(_firebaseAuth.currentUser!.uid)
          .update(newUser.toJson());
    } catch (_) {
      Get.snackbar(
          LocaleKeys.failedToSaveChanges.tr, LocaleKeys.tryAgainLater.tr);
    }
  }

  Future<bool> backButtonPressed(context) {
    return Future<bool>(
      () {
        if (editMode) {
          toggleEditMode(context, false);
          return false;
        }
        return true;
      },
    );
  }
}
