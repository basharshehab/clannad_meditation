import 'dart:math';

import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:get/get.dart';
import 'package:mrx_charts/mrx_charts.dart';

class StatsController extends GetxController {
  late final List<ChartBarDataItem> userChartBars;

  @override
  void onInit() {
    super.onInit();
    userChartBars = List.generate(
      7,
      (index) => ChartBarDataItem(
        color: index % 2 == 0 ? AppColors.stats1 : AppColors.stats2,
        value: Random().nextInt(250).toDouble(),
        x: index.toDouble() + 1,
      ),
    );
  }

  // @override
  // void onReady() {
  //   super.onReady();
  // }

  // @override
  // void onClose() {
  //   super.onClose();
  // }
}
