import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/durations.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AchievementsController extends GetxController {
  List<Widget> clannadAchievements = [];
  Tween<double> fadeAnimationTween = Tween(begin: 0.0, end: 1.0);
  final GlobalKey<AnimatedListState> listKey = GlobalKey<AnimatedListState>();

  @override
  void onInit() {
    super.onInit();
    addAchievementsTiles();
  }

  // @override
  // void onReady() {
  //   super.onReady();
  // }

  // @override
  // void onClose() {
  //   super.onClose();
  // }

  void addAchievementsTiles() {
    // List songsList = songsData.locale[neededLocale]!;
    Future ft = Future(() {});
    for (int i = 0; i < achievementsData.length; i++) {
      ft = ft.then((_) {
        return Future.delayed(Durations.animationDurationLong, () {
          clannadAchievements.add(
            ListTile(
              leading: const Icon(Icons.star),
              title: Text(
                achievementsData[i]["title"][Get.deviceLocale!.languageCode] ??
                    achievementsData[i]["title"]["en"],
                style: TextStyle(
                    fontFamily: Assets.fontFamilies.alegreyaF,
                    color: AppColors.black),
              ),
              subtitle: Text(
                achievementsData[i]["subtitle"]
                        [Get.deviceLocale!.languageCode] ??
                    achievementsData[i]["subtitle"]["en"],
                style: TextStyle(
                    fontFamily: Assets.fontFamilies.alegreyaSansF,
                    color: AppColors.subtitle1_50),
              ),
            ),
          );
          listKey.currentState?.insertItem(i);
        });
      });
    }
  }
}

List achievementsData = List.from([
  {
    "title": {
      "en": "Baby Steps",
      "de": "Baby Schritte",
      "ar": "الخطوات الأولى",
    },
    "subtitle": {
      "en": "Meditate once to unlock this achievement.",
      "de": "Einmal meditieren, um diese Leistun zu entsprren.",
      "ar": "تأمل لمرة لفتح هذا الإنجاز.",
    },
    "id": 29482124,
  },
  {
    "title": {
      "en": "Going Strong",
      "de": "Immer noch Stark",
      "ar": "مستمرٌ بقوة",
    },
    "subtitle": {
      "en": "Meditate 5 times to unlock this achievement",
      "de": "5 Male meditieren, um diese Leistun zu entsprren.",
      "ar": "تأمل لـ5 مرات لفتح هذا الإنجاز.",
    },
    "id": 29482123,
  },
  {
    "title": {
      "en": "Perseverance",
      "de": "Beharrlichkeit",
      "ar": "المثابرة",
    },
    "subtitle": {
      "en": "Meditate 20 times to unlock this achievement",
      "de": "20 Male meditieren, um diese Leistun zu entsprren.",
      "ar": "تأمل لـ20 مرة لفتح هذا الإنجاز.",
    },
    "id": 29482122,
  },
]);
