import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/paddings.dart';
import 'package:clannad/app/modules/profile/controllers/stats_controller.dart';
import 'package:clannad/app/services/int_to_date_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrx_charts/mrx_charts.dart';

class Stats extends GetView<StatsController> {
  const Stats({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        // color: Colors.blue,
        padding: EdgeInsets.symmetric(
          horizontal: Paddings.horizontal(context) * 2.5,
          vertical: Paddings.vertical(context) * 10,
        ),
        child: Chart(
          padding: EdgeInsets.symmetric(
              horizontal: Paddings.horizontal(context) * 0.5),
          layers: [
            ChartAxisLayer(
              settings: ChartAxisSettings(
                x: const ChartAxisSettingsAxis(
                  frequency: 1.0,
                  max: 7.0,
                  min: 1.0,
                  textStyle: TextStyle(
                    color: AppColors.black,
                    fontSize: 10.0,
                  ),
                ),
                y: ChartAxisSettingsAxis(
                  frequency: controller.userChartBars.reduce((a, b) {
                        return a.value > b.value ? a : b;
                      }).value /
                      5.0,
                  max: controller.userChartBars.reduce((a, b) {
                    return a.value > b.value ? a : b;
                  }).value,
                  min: 0.0,
                  textStyle: const TextStyle(
                    color: AppColors.black,
                    fontSize: 10.0,
                  ),
                ),
              ),
              labelX: (value) => Converter.intToDay(value.toInt()),
              labelY: (value) => value == 0 ? "Min" : value.toInt().toString(),
            ),
            ChartBarLayer(
              items: controller.userChartBars,
              settings: const ChartBarSettings(
                thickness: 12.0,
                radius: BorderRadius.all(Radius.circular(35.0)),
              ),
            ),
          ],
        ));
  }
}
