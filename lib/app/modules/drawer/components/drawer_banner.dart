import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:flutter/material.dart';

class DrawerBanner extends StatelessWidget {
  const DrawerBanner({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: ShaderMask(
        shaderCallback: (Rect bounds) {
          return const LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              AppColors.transparent,
              AppColors.transparent,
              AppColors.description,
            ],
            stops: [
              0.1,
              0.9,
              1.0
            ], // 10% purple, 80% transparent, 10% purple
          ).createShader(bounds);
        },
        blendMode: BlendMode.dst,
        child: Image.asset(
          Assets.logoArt.fullArt,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
