import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/general_components/button_builder.dart';
import 'package:clannad/app/data/general_components/text_builder.dart';
import 'package:flutter/material.dart';

class DrawerButton extends StatelessWidget {
  final Function()? onPressed;
  final String text;
  final Color? color;
  const DrawerButton(
      {Key? key,
      required this.scaffoldKey,
      this.onPressed,
      required this.text,
      this.color})
      : super(key: key);

  final GlobalKey<ScaffoldState> scaffoldKey;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: AppSizes.width(context),
      alignment: Alignment.center,
      child: ButtonBuilder(
        backgroundColor: AppColors.transparent,
        padding: const EdgeInsets.all(0),
        borderRadius: 0.0,
        text: TextBuilder(
          maxWidth: AppSizes.width(context),
          textAlign: TextAlign.center,
          text: text,
          fontFamily: Assets.fontFamilies.alegreyaSansF,
          maxLines: 1,
          fontWeight: FontWeight.bold,
          textColor: color ?? AppColors.black,
          maxHeight: AppSizes.height(context) * 0.035,
        ),
        onPressed: onPressed,
      ),
    );
  }
}
