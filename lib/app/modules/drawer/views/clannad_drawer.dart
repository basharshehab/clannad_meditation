import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/modules/drawer/components/drawer_banner.dart';
import 'package:clannad/app/modules/drawer/components/drawer_button.dart';
import 'package:clannad/app/modules/home/controllers/home_controller.dart';
import 'package:clannad/app/routes/app_pages.dart';
import 'package:clannad/app/services/navigate.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ClannadDrawer extends GetView<HomeController> {
  final GlobalKey<ScaffoldState> scaffoldKey;
  const ClannadDrawer({
    Key? key,
    required this.scaffoldKey,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      width: AppSizes.width(context) * 0.75,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              const DrawerBanner(),
              DrawerButton(
                scaffoldKey: scaffoldKey,
                text: LocaleKeys.settings.tr,
                onPressed: () {
                  Navigate.getTo(Routes.SETTINGS, null);
                  scaffoldKey.currentState?.closeDrawer();
                },
              ),
              DrawerButton(
                scaffoldKey: scaffoldKey,
                text: LocaleKeys.donate.tr,
                onPressed: () {
                  // Navigate.getTo(Routes.SETTINGS, null);
                  // scaffoldKey.currentState?.closeDrawer();
                },
              ),
            ],
          ),
          SafeArea(
            child: DrawerButton(
              scaffoldKey: scaffoldKey,
              text: LocaleKeys.logout.tr,
              color: AppColors.red,
              onPressed: controller.logout,
            ),
          ),
        ],
      ),
    );
  }
}
