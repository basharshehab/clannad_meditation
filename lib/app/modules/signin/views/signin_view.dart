import 'package:auto_size_text/auto_size_text.dart';
import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/data/constants/paddings.dart';
import 'package:clannad/app/data/general_components/button_builder.dart';
import 'package:clannad/app/data/general_components/clannad_logo.dart';
import 'package:clannad/app/data/general_components/text_builder.dart';
import 'package:clannad/app/services/on_tap.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:get/get.dart';

import '../controllers/signin_controller.dart';

class SigninView extends GetView<SigninController> {
  const SigninView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Tap.unFocus(context),
      child: Scaffold(
        // resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: SizedBox(
            height: AppSizes.height(context),
            child: Stack(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Align(
                  alignment: Alignment.bottomCenter,
                  child: SvgPicture.asset(
                    Assets
                        .backgrounds.widgetBackgrounds.backgroundLeafCroppedSvg,
                    fit: BoxFit.cover,
                    width: AppSizes.width(context),
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  padding: Paddings.horizontalPadding(context) * 1.75,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SafeArea(
                        top: true,
                        left: false,
                        bottom: false,
                        right: false,
                        child: SizedBox(),
                      ),
                      ClannadLogo(
                        padding: EdgeInsets.only(
                            top: AppSizes.height(context) * 0.1),
                      ),
                      Container(
                        padding:
                            EdgeInsets.only(top: AppSizes.height(context) / 25),
                        child: TextBuilder(
                          // width: AppSizes.width(context) / 2,
                          // height: AppSizes.height(context) / 17,
                          maxWidth: AppSizes.width(context),
                          maxHeight: AppSizes.height(context) * 0.055,
                          text: LocaleKeys.signin.tr,
                          fontFamily: Assets.fontFamilies.alegreyaF,
                          maxLines: 1,
                          fontWeight: FontWeight.normal,
                          textColor: AppColors.black,
                        ),
                      ),
                      TextBuilder(
                        // width: AppSizes.width(context) / 1,
                        // height: AppSizes.height(context) / 14.35,
                        maxWidth: AppSizes.width(context) / 1,
                        maxHeight: AppSizes.height(context) * 0.07,
                        text: LocaleKeys.signinSubtitle.tr,
                        fontFamily: Assets.fontFamilies.alegreyaSansF,
                        maxLines: 2,
                        fontWeight: FontWeight.normal,
                        textColor: AppColors.subtitle1,
                      ),
                      Form(
                        key: controller.formKey,
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(
                                  top: AppSizes.height(context) / 40),
                              child: TextFormField(
                                textInputAction: TextInputAction.next,
                                decoration: InputDecoration(
                                  focusColor: AppColors.green,
                                  alignLabelWithHint: true,
                                  label: Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: Text(LocaleKeys.emailAddress.tr),
                                  ),
                                  labelStyle: TextStyle(
                                    fontSize: 18,
                                    fontFamily:
                                        Assets.fontFamilies.alegreyaSansF,
                                    fontWeight: FontWeight.w400,
                                    color: AppColors.hintText,
                                  ),
                                ),
                                style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: Assets.fontFamilies.alegreyaSansF,
                                  fontWeight: FontWeight.w400,
                                  color: AppColors.black,
                                ),
                                keyboardType: TextInputType.emailAddress,
                                onSaved: controller.saveEmailAddress,
                                validator: controller.validateEmailAddress,
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top: AppSizes.height(context) / 70),
                              child: TextFormField(
                                textInputAction: TextInputAction.done,
                                decoration: InputDecoration(
                                  label: Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: Text(LocaleKeys.password.tr),
                                  ),
                                  alignLabelWithHint: true,
                                  labelStyle: TextStyle(
                                    fontSize: 18,
                                    fontFamily:
                                        Assets.fontFamilies.alegreyaSansF,
                                    fontWeight: FontWeight.w400,
                                    color: AppColors.hintText,
                                  ),
                                ),
                                style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: Assets.fontFamilies.alegreyaSansF,
                                  fontWeight: FontWeight.w400,
                                  color: AppColors.black,
                                ),
                                obscureText: true,
                                keyboardType: TextInputType.visiblePassword,
                                onSaved: controller.savePassword,
                                validator: controller.validatePassword,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: TextButton(
                          onPressed: () => controller.goToRecovery(context),
                          child: TextBuilder(
                            text: LocaleKeys.forgotPassword.tr,
                            fontFamily: Assets.fontFamilies.alegreyaSansF,
                            fontWeight: FontWeight.normal,
                            textColor: AppColors.subtitle3,
                            // height: AppSizes.height(context) / 45,
                            maxHeight: AppSizes.height(context) / 45,
                            maxLines: 1,
                            maxWidth: AppSizes.width(context) / 3,

                            textAlign: TextAlign.right,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: AppSizes.height(context) / 150),
                        child: ButtonBuilder(
                          backgroundColor: AppColors.green,
                          padding: EdgeInsets.symmetric(
                              vertical: AppSizes.height(context) / 50),
                          borderRadius: 10.0,
                          text: TextBuilder(
                            // width: AppSizes.width(context),
                            // height: AppSizes.width(context) / 13,
                            maxWidth: AppSizes.width(context),
                            maxHeight: AppSizes.width(context) * 0.08,
                            text: LocaleKeys.signin.tr,
                            fontFamily: Assets.fontFamilies.latoF,
                            maxLines: 1,
                            fontWeight: FontWeight.w500,
                            textAlign: TextAlign.center,
                            textColor: AppColors.white,
                          ),
                          onPressed: () => controller.signIn(context),
                        ),
                      ),
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.center,
                      //   crossAxisAlignment: CrossAxisAlignment.center,
                      //   children: [
                      //     TextBuilder(
                      //       // width: AppSizes.width(context),
                      //       // height: AppSizes.width(context) / 13,
                      //       maxWidth: AppSizes.width(context),
                      //       maxHeight: AppSizes.width(context) * 0.06,
                      //       text: LocaleKeys.orSignInWith.tr,
                      //       fontFamily: Assets.fontFamilies.alegreyaSansF,
                      //       maxLines: 1,
                      //       fontWeight: FontWeight.bold,
                      //       textAlign: TextAlign.center,
                      //       textColor: AppColors.lightBlack,
                      //     ),
                      //     GestureDetector(
                      //       onTap: () {},
                      //       child: Container(
                      //         alignment: Alignment.center,
                      //         margin: EdgeInsets.only(
                      //           top: AppSizes.height(context) * 0.05,
                      //           bottom: AppSizes.height(context) * 0.05,
                      //           left: AppSizes.height(context) * 0.03,
                      //           right: AppSizes.height(context) * 0.03,
                      //         ),
                      //         height: AppSizes.width(context) * 0.125,
                      //         width: AppSizes.width(context) * 0.125,
                      //         child: SizedBox.expand(
                      //           child: SvgPicture.asset(
                      //             Assets.socialMedia.googleLogo,
                      //             fit: BoxFit.contain,
                      //           ),
                      //         ),
                      //       ),
                      //     ),
                      //   ],
                      // ),
                      SizedBox(
                        height: AppSizes.height(context) * 0.029,
                      ),
                      SizedBox(
                        width: AppSizes.width(context),
                        child: LimitedBox(
                          maxWidth: AppSizes.width(context),
                          child: GestureDetector(
                            onTap: () => controller.goToSignUp(context),
                            child: AutoSizeText.rich(
                              TextSpan(
                                children: [
                                  TextSpan(
                                    text: "${LocaleKeys.noAccount.tr}  ",
                                    style: TextStyle(
                                      fontFamily:
                                          Assets.fontFamilies.alegreyaSansF,
                                      fontWeight: FontWeight.w500,
                                      color: AppColors.subtitle2,
                                    ),
                                  ),
                                  TextSpan(
                                    text: LocaleKeys.signup.tr,
                                    style: TextStyle(
                                      fontFamily:
                                          Assets.fontFamilies.alegreyaSansF,
                                      fontWeight: FontWeight.w700,
                                      color: AppColors.subtitle2,
                                    ),
                                  ),
                                ],
                              ),
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                fontSize: 20,
                              ),
                              minFontSize: 10,
                              maxFontSize: 30,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
