import 'package:clannad/app/data/constants/collections.dart';
import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/data/models/user_model.dart';
import 'package:clannad/app/modules/splash/controllers/super_controller.dart';
import 'package:clannad/app/routes/app_pages.dart';
import 'package:clannad/app/services/navigate.dart';
import 'package:clannad/app/services/on_tap.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

class SigninController extends GetxController {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  late String? _emailAddress;
  late String? _password;

  Super get superController => Get.find<Super>();
  // @override
  // void onInit() {
  //   super.onInit();
  // }

  // @override
  // void onReady() {
  //   super.onReady();
  // }

  // @override
  // void onClose() {
  //   super.onClose();
  // }

  void _handleFirebaseAuthExceptionSignin(FirebaseAuthException e) {
    switch (e.code) {
      case "invalid-email":
        Get.snackbar(LocaleKeys.couldntSignIn.tr, LocaleKeys.invalidEmail.tr);
        break;
      case "user-disabled":
        Get.snackbar(LocaleKeys.couldntSignIn.tr, LocaleKeys.userDisabled.tr);
        break;
      case "user-not-found":
        Get.snackbar(LocaleKeys.couldntSignIn.tr, LocaleKeys.userNotFound.tr);
        break;
      case "wrong-password":
        Get.snackbar(LocaleKeys.couldntSignIn.tr, LocaleKeys.wrongPassword.tr);
        break;
      default:
        Get.snackbar(LocaleKeys.couldntSignIn.tr, LocaleKeys.unknownError.tr);
      // break;
    }
  }

  void saveEmailAddress(String? value) {
    _emailAddress = value?.trim();
  }

  String? validateEmailAddress(String? value) {
    value = value?.trim();

    if (value == null || value.isEmpty) {
      return LocaleKeys.pleaseEnterEmailAddress.tr;
    }
    return null;
  }

  void savePassword(String? value) {
    _password = value?.trim();
  }

  String? validatePassword(String? value) {
    value = value?.trim();

    if (value == null || value.isEmpty) {
      return LocaleKeys.pleaseEnterPassword.tr;
    }
    return null;
  }

  void goToSignUp(BuildContext context) {
    Tap.unFocus(context);
    Navigate.getTo(Routes.SIGNUP, null);
  }

  void goToRecovery(BuildContext context) {
    Tap.unFocus(context);
    Navigate.getTo(Routes.RECOVERY, null);
  }

  void goToHome(BuildContext context) {
    Tap.unFocus(context);
    Navigate.getTo(Routes.HOME, null);
  }

  void signIn(BuildContext context) async {
    Tap.unFocus(context);
    if (!await InternetConnectionChecker().hasConnection) {
      Get.snackbar(
          LocaleKeys.noConnection.tr, LocaleKeys.checkInternetConnection.tr);
      return;
    }
    if (formKey.currentState == null || !formKey.currentState!.validate()) {
      return;
    }

    formKey.currentState!.save();
    late UserCredential userCred;
    try {
      userCred = await _firebaseAuth.signInWithEmailAndPassword(
          email: _emailAddress!, password: _password!);
    } on FirebaseAuthException catch (e) {
      _handleFirebaseAuthExceptionSignin(e);
      return;
    }

    // UserModel userModel;
    try {
      superController.userModel = UserModel.fromJson((await _firestore
              .collection(Collections.users)
              .doc(userCred.user?.uid)
              .get())
          .data()!);
      Navigate.getOffAll(Routes.HOME, null);
    } on Exception catch (e) {
      _handleUnknownError(e);
    }
  }

  void _handleUnknownError(Exception e) {
    Get.snackbar(LocaleKeys.unknownError.tr, e.toString());
  }
}
