// import 'package:clannad/app/data/constants/app_colors.dart';
// import 'package:clannad/app/data/constants/app_sizes.dart';
// import 'package:clannad/app/data/constants/assets.dart';
// import 'package:clannad/app/data/constants/paddings.dart';
// import 'package:clannad/app/data/general_components/button_builder.dart';
// import 'package:clannad/app/data/general_components/clannad_logo.dart';
// import 'package:clannad/app/data/general_components/text_builder.dart';
// import 'package:clannad/app/data/locale/locale_keys.dart';
// import 'package:clannad/app/modules/recovery/controllers/recovery_controller.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';

// class ConfirmPasswordResetDialog extends GetView<RecoveryController> {
//   const ConfirmPasswordResetDialog({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: Paddings.horizontalPadding(context) * 1.75,
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           const SafeArea(
//             top: true,
//             left: false,
//             bottom: false,
//             right: false,
//             child: SizedBox(),
//           ),
//           ClannadLogo(
//             padding: EdgeInsets.only(top: AppSizes.height(context) * 0.1),
//           ),
//           Container(
//             padding: EdgeInsets.only(top: AppSizes.height(context) / 25),
//             child: TextBuilder(
//               // width: AppSizes.width(context) / 2,
//               // height: AppSizes.height(context) / 17,
//               maxWidth: AppSizes.width(context) / 2,
//               maxHeight: AppSizes.height(context) * 0.055,
//               text: LocaleKeys.recoverAccount.tr,
//               fontFamily: Assets.fontFamilies.alegreyaF,
//               maxLines: 1,
//               fontWeight: FontWeight.normal,
//               textColor: AppColors.black,
//             ),
//           ),
//           TextBuilder(
//             // width: AppSizes.width(context) / 1,
//             // height: AppSizes.height(context) / 14.35,
//             maxWidth: AppSizes.width(context) / 1,
//             maxHeight: AppSizes.height(context) * 0.07,
//             text: LocaleKeys.enterNewPassword.tr,
//             fontFamily: Assets.fontFamilies.alegreyaSansF,
//             maxLines: 3,
//             fontWeight: FontWeight.normal,
//             textColor: AppColors.subtitle1,
//           ),
//           Form(
//             key: controller.newPasswordFormKey,
//             child: Column(
//               children: [
//                 Container(
//                   padding: EdgeInsets.only(top: AppSizes.height(context) / 40),
//                   child: TextFormField(
//                     controller: controller.newPassword,
//                     // validator: controller.validateEmail,
//                     decoration: InputDecoration(
//                       focusColor: AppColors.green,
//                       alignLabelWithHint: true,
//                       label: Container(
//                         // ignore: prefer_const_constructors
//                         padding: EdgeInsets.symmetric(horizontal: 10),
//                         child: Text(LocaleKeys.password.tr),
//                       ),
//                       labelStyle: TextStyle(
//                         fontSize: 18,
//                         fontFamily: Assets.fontFamilies.alegreyaSansF,
//                         fontWeight: FontWeight.w400,
//                         color: AppColors.hintText,
//                       ),
//                     ),
//                     style: TextStyle(
//                       fontSize: 18,
//                       fontFamily: Assets.fontFamilies.alegreyaSansF,
//                       fontWeight: FontWeight.w400,
//                       color: AppColors.black,
//                     ),
//                     keyboardType: TextInputType.visiblePassword,
//                   ),
//                 ),
//               ],
//             ),
//           ),
//           Container(
//             padding: EdgeInsets.only(top: AppSizes.height(context) / 20),
//             child: ButtonBuilder(
//               onPressed: controller.confirmPasswordReset,
//               backgroundColor: AppColors.green,
//               padding:
//                   EdgeInsets.symmetric(vertical: AppSizes.height(context) / 50),
//               borderRadius: 10.0,
//               text: TextBuilder(
//                 maxWidth: AppSizes.width(context),
//                 maxHeight: AppSizes.width(context) * 0.08,
//                 text: LocaleKeys.confirmReset.tr,
//                 fontFamily: Assets.fontFamilies.latoF,
//                 maxLines: 1,
//                 fontWeight: FontWeight.w500,
//                 textAlign: TextAlign.center,
//                 textColor: AppColors.white,
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
