import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/constants/durations.dart';
import 'package:clannad/app/data/constants/getx_ids.dart';
import 'package:clannad/app/modules/recovery/components/send_email_dialog.dart';
import 'package:clannad/app/services/on_tap.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:get/get.dart';

import '../controllers/recovery_controller.dart';

class RecoveryView extends GetView<RecoveryController> {
  const RecoveryView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Tap.unFocus(context),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GetBuilder<RecoveryController>(
              id: GetxIds.resetCode,
              builder: (_) => AnimatedSwitcher(
                  duration: Durations.animationDurationLong,
                  switchInCurve: Curves.easeIn,
                  switchOutCurve: Curves.easeIn.flipped,
                  child:
                      //  !controller.emailSent
                      //     ?
                      const SendEmailDialog()
                  // : !controller.codeVerified
                  //     ? const VerifyCodeDialog()
                  //     : const ConfirmPasswordResetDialog(),
                  ),
            ),
            Flexible(
              child: SvgPicture.asset(
                Assets.backgrounds.widgetBackgrounds.backgroundLeafSvg,
                fit: BoxFit.cover,
                width: AppSizes.width(context),
              ),
            )
          ],
        ),
      ),
    );
  }
}
