import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/services/navigate.dart';
import 'package:clannad/app/services/validator_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class RecoveryController extends GetxController {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  // bool emailSent = false;
  // bool codeVerified = false;

  late TextEditingController email;
  // late TextEditingController code;
  // late TextEditingController newPassword;

  late GlobalKey<FormState> emailFormKey;
  // late GlobalKey<FormState> codeFormKey;
  // late GlobalKey<FormState> newPasswordFormKey;
  @override
  void onInit() {
    super.onInit();

    email = TextEditingController();
    // code = TextEditingController();
    // newPassword = TextEditingController();
    // newPasswordFormKey = GlobalKey<FormState>();
    // codeFormKey = GlobalKey<FormState>();
    emailFormKey = GlobalKey<FormState>();
  }

  // @override
  // void onReady() {
  //   super.onReady();
  // }

  // @override
  // void onClose() {
  //   super.onClose();
  // }

  void sendPasswordResetEmail() async {
    if (!emailFormKey.currentState!.validate()) {
      return;
    }
    try {
      await _auth.sendPasswordResetEmail(email: email.text);
    } on FirebaseAuthException catch (e) {
      switch (e.code) {
        case "invalid-email":
          Get.snackbar(LocaleKeys.invalidEmail.tr, LocaleKeys.tryAgainLater.tr);
          return;
        case "missing-android-pkg-name":
          Get.snackbar(LocaleKeys.unknownError.tr, LocaleKeys.tryAgainLater.tr);
          return;

        case "missing-continue-uri":
          Get.snackbar(LocaleKeys.unknownError.tr, LocaleKeys.tryAgainLater.tr);
          return;

        case "missing-ios-bundle-id":
          Get.snackbar(LocaleKeys.unknownError.tr, LocaleKeys.tryAgainLater.tr);
          return;

        case "invalid-continue-uri":
          Get.snackbar(LocaleKeys.unknownError.tr, LocaleKeys.tryAgainLater.tr);
          return;

        case "unauthorized-continue-uri":
          Get.snackbar(LocaleKeys.unknownError.tr, LocaleKeys.tryAgainLater.tr);
          return;

        case "user-not-found":
          Get.snackbar(LocaleKeys.userNotFound.tr, LocaleKeys.tryAgainLater.tr);
          return;
      }
    }
    // emailSent = true;
    // update([GetxIds.resetCode]);
    Navigate.getBack();

    Get.snackbar(
        LocaleKeys.weSentResetEmail.tr, LocaleKeys.followInstructionsEmail.tr);
  }

  // void verifyPasswordResetCode() async {
  //   try {
  //     await _auth.verifyPasswordResetCode(code.text);
  //   } on FirebaseAuthException catch (e) {
  //     switch (e.code) {
  //       case "expired-action-code":
  //         Get.snackbar(
  //             "LocaleKeys.expiredActionCode.tr", LocaleKeys.tryAgainLater.tr);
  //         return;
  //       case "invalid-action-code":
  //         Get.snackbar(
  //             "LocaleKeys.invalidActionCode.tr", LocaleKeys.tryAgainLater.tr);
  //         return;
  //       case "user-disabled":
  //         Get.snackbar(LocaleKeys.userDisabled.tr, LocaleKeys.tryAgainLater.tr);
  //         return;
  //       case "user-not-found":
  //         Get.snackbar(LocaleKeys.userNotFound.tr, LocaleKeys.tryAgainLater.tr);
  //         return;
  //     }
  //   }
  //   // codeVerified = true;
  //   update([GetxIds.resetCode]);
  // }

  // void confirmPasswordReset() async {
  //   try {
  //     await _auth.confirmPasswordReset(
  //         code: code.text, newPassword: newPassword.text);
  //   } on FirebaseException catch (e) {
  //     switch (e.code) {
  //       case "expired-action-code":
  //         Get.snackbar(
  //             "LocaleKeys.expiredActionCode.tr", LocaleKeys.tryAgainLater.tr);
  //         return;
  //       case "invalid-action-code":
  //         Get.snackbar(
  //             "LocaleKeys.invalidActionCode.tr", LocaleKeys.tryAgainLater.tr);
  //         return;
  //       case "user-disabled":
  //         Get.snackbar(LocaleKeys.userDisabled.tr, LocaleKeys.tryAgainLater.tr);
  //         return;
  //       case "user-not-found":
  //         Get.snackbar(LocaleKeys.userNotFound.tr, LocaleKeys.tryAgainLater.tr);
  //         return;
  //       case "weak-password":
  //         Get.snackbar(LocaleKeys.weakPassword.tr, LocaleKeys.tryAgainLater.tr);
  //         return;
  //     }
  //   }
  //   Get.snackbar(LocaleKeys.passwordResetDone.tr, LocaleKeys.youCanSignIn.tr);

  //   Navigate.getOffAll(Routes.SIGNIN, null);
  //   // emailSent = true;
  //   // update([GetxIds.resetCode]);
  // }

  String? validateEmail(String? string) {
    if (string == null || !Validator.isEmail(string)) {
      return LocaleKeys.invalidEmail.tr;
    }
    return null;
  }
}
