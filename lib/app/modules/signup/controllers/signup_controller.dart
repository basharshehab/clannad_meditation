import 'package:clannad/app/data/constants/collections.dart';
import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/data/models/user_model.dart';
import 'package:clannad/app/routes/app_pages.dart';
import 'package:clannad/app/services/navigate.dart';
import 'package:clannad/app/services/on_tap.dart';
import 'package:clannad/app/services/validator_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

class SignupController extends GetxController {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  String _username = "";
  String _emailAddress = "";
  String _password = "";
  String _profileQuote = "";

  // @override
  // void onInit() {
  //   super.onInit();
  // }

  // @override
  // void onReady() {
  //   super.onReady();
  // }

  // @override
  // void onClose() {
  //   super.onClose();
  // }

  void signUpEmailPassword() async {
    if (!await InternetConnectionChecker().hasConnection) {
      Get.snackbar(
          LocaleKeys.noConnection.tr, LocaleKeys.checkInternetConnection.tr);
      return;
    }
    if (formKey.currentState == null || !formKey.currentState!.validate()) {
      return;
    }

    UserModel userModel = UserModel(
      name: _username,
      emailAddress: _emailAddress,
      profileQuote: _profileQuote,
      feelingsHistoryAfter: {},
      feelingsHistoryBefore: {},
      meditationHistory: {},
      currentStreak: 0,
      maxStreak: 0,
      createdAt:
          Timestamp.fromDate(DateTime.now()).millisecondsSinceEpoch.toString(),
      imageUrl: "",
      lastStreakUpdateTimestamp:
          Timestamp.fromDate(DateTime.now()).millisecondsSinceEpoch.toString(),
    );
    late UserCredential userCred;
    try {
      userCred = await _firebaseAuth.createUserWithEmailAndPassword(
          email: _emailAddress, password: _password);
      await _firebaseAuth.signOut();
    } on FirebaseAuthException catch (e) {
      _handleFirebaseAuthExceptionSignUp(e);
      return;
    }

    try {
      await _firestore
          .collection(Collections.users)
          .doc(userCred.user?.uid)
          .set(userModel.toJson());
    } on Exception catch (e) {
      _handleUnknownError(e);
      _deleteAccountOnSignupFailure(_emailAddress, _password);
    }

    Get.snackbar(LocaleKeys.signUpSuccessful.tr, LocaleKeys.youCanSignIn.tr);
    Navigate.getOffAll(Routes.SIGNIN, null);
  }

  void _handleFirebaseAuthExceptionSignUp(FirebaseAuthException e) {
    switch (e.code) {
      case "email-already-in-use":
        Get.snackbar(
            LocaleKeys.couldntSignUp.tr, LocaleKeys.emailAlreadyInUse.tr);
        break;
      case "invalid-email":
        Get.snackbar(LocaleKeys.couldntSignUp.tr, LocaleKeys.invalidEmail.tr);
        break;
      case "operation-not-allowed":
        Get.snackbar(
            LocaleKeys.couldntSignUp.tr, LocaleKeys.operationNotAllowed.tr);
        break;
      case "weak-password":
        Get.snackbar(LocaleKeys.couldntSignUp.tr, LocaleKeys.weakPassword.tr);
        break;
      default:
        Get.snackbar(LocaleKeys.couldntSignUp.tr, LocaleKeys.unknownError.tr);
      // break;
    }
  }

  void saveName(String value) {
    _username = value.trim();
  }

  void saveEmailAddress(String value) {
    _emailAddress = value.trim();
  }

  void savePassword(String value) {
    _password = value.trim();
  }

  void saveProfileQuote(String value) {
    _profileQuote = value.trim();
  }

  String? validateName(String? value) {
    // _username = value.trim();
    value = value?.trim();

    if (value == null || value.isEmpty) {
      return LocaleKeys.invalidUsername.tr;
    }
    return null;
  }

  String? validateEmailAddress(String? value) {
    // _username = value.trim();
    value = value?.trim();

    if (value == null || !Validator.isEmail(value)) {
      return LocaleKeys.invalidEmail.tr;
    }
    return null;
  }

  String? validatePassword(String? value) {
    // _username = value.trim();
    value = value?.trim();

    if (value == null || value.isEmpty || !Validator.isPassword(value)) {
      return LocaleKeys.weakPassword.tr;
    }
    return null;
  }

  String? validateProfileQuote(String? value) {
    // _username = value.trim();
    value = value?.trim();

    if (value == null || value.isEmpty) {
      return LocaleKeys.invalidProfileQuote.tr;
    }
    return null;
  }

  void goBackToLogin(BuildContext context) {
    Tap.unFocus(context);
    Navigate.getBack();
  }

  void _deleteAccountOnSignupFailure(String email, String password) async {
    try {
      User? user = _firebaseAuth.currentUser;
      AuthCredential credentials =
          EmailAuthProvider.credential(email: email, password: password);
      UserCredential? result =
          await user?.reauthenticateWithCredential(credentials);
      await result?.user?.delete();
      return;
    } on Exception catch (e) {
      // print(e.toString());
      _handleUnknownError(e);
    }
  }

  void _handleUnknownError(Exception e) {
    Get.snackbar(LocaleKeys.unknownError.tr, e.toString());
  }
}
