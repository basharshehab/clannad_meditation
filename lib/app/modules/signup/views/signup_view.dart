import 'package:auto_size_text/auto_size_text.dart';
import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/data/constants/paddings.dart';
import 'package:clannad/app/data/general_components/button_builder.dart';
import 'package:clannad/app/data/general_components/clannad_logo.dart';
import 'package:clannad/app/data/general_components/text_builder.dart';
import 'package:clannad/app/services/on_tap.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:get/get.dart';

import '../controllers/signup_controller.dart';

class SignupView extends GetView<SignupController> {
  const SignupView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Tap.unFocus(context),
      child: Scaffold(
        body: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: SizedBox(
            height: AppSizes.height(context),
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.bottomCenter,
                  child: SvgPicture.asset(
                    Assets
                        .backgrounds.widgetBackgrounds.backgroundLeafCroppedSvg,
                    fit: BoxFit.cover,
                    width: AppSizes.width(context),
                  ),
                ),
                Container(
                  padding: Paddings.horizontalPadding(context) * 1.75,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // shrinkWrap: true,
                    // physics: const NeverScrollableScrollPhysics(),
                    children: [
                      const SafeArea(
                        top: true,
                        left: false,
                        bottom: false,
                        right: false,
                        child: SizedBox(),
                      ),
                      ClannadLogo(
                        padding: EdgeInsets.only(
                            top: AppSizes.height(context) * 0.08),
                      ),
                      Container(
                        padding:
                            EdgeInsets.only(top: AppSizes.height(context) / 25),
                        child: TextBuilder(
                          // width: AppSizes.width(context) / 2,
                          // height: AppSizes.height(context) / 17,
                          maxWidth: AppSizes.width(context) / 2,
                          maxHeight: AppSizes.height(context) * 0.055,
                          text: LocaleKeys.signup.tr,
                          fontFamily: Assets.fontFamilies.alegreyaF,
                          maxLines: 1,
                          fontWeight: FontWeight.normal,
                          textColor: AppColors.black,
                        ),
                      ),
                      TextBuilder(
                        // width: AppSizes.width(context) / 1,
                        // height: AppSizes.height(context) / 14.35,
                        maxWidth: AppSizes.width(context) / 2,
                        maxHeight: AppSizes.height(context) * 0.07,
                        text: LocaleKeys.signupSubtitle.tr,
                        fontFamily: Assets.fontFamilies.alegreyaSansF,
                        maxLines: 2,
                        fontWeight: FontWeight.normal,
                        textColor: AppColors.description,
                      ),
                      Form(
                        key: controller.formKey,
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(
                                  top: AppSizes.height(context) / 40),
                              child: TextFormField(
                                textInputAction: TextInputAction
                                    .next, // Moves focus to next.

                                decoration: InputDecoration(
                                  alignLabelWithHint: true,
                                  label: Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: Text(LocaleKeys.name.tr),
                                  ),
                                  labelStyle: TextStyle(
                                    fontSize: 18,
                                    fontFamily:
                                        Assets.fontFamilies.alegreyaSansF,
                                    fontWeight: FontWeight.w400,
                                    color: AppColors.hintText,
                                  ),
                                ),
                                style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: Assets.fontFamilies.alegreyaSansF,
                                  fontWeight: FontWeight.w400,
                                  color: AppColors.black,
                                ),
                                keyboardType: TextInputType.text,
                                validator: controller.validateName,
                                onChanged: controller.saveName,
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top: AppSizes.height(context) / 70),
                              child: TextFormField(
                                textInputAction: TextInputAction
                                    .next, // Moves focus to next.

                                decoration: InputDecoration(
                                  alignLabelWithHint: true,
                                  label: Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: Text(LocaleKeys.emailAddress.tr),
                                  ),
                                  labelStyle: TextStyle(
                                    fontSize: 18,
                                    fontFamily:
                                        Assets.fontFamilies.alegreyaSansF,
                                    fontWeight: FontWeight.w400,
                                    color: AppColors.hintText,
                                  ),
                                ),
                                style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: Assets.fontFamilies.alegreyaSansF,
                                  fontWeight: FontWeight.w400,
                                  color: AppColors.black,
                                ),
                                keyboardType: TextInputType.emailAddress,
                                validator: controller.validateEmailAddress,
                                onChanged: controller.saveEmailAddress,
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top: AppSizes.height(context) / 70),
                              child: TextFormField(
                                textInputAction: TextInputAction
                                    .next, // Moves focus to next.

                                decoration: InputDecoration(
                                  label: Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: Text(LocaleKeys.password.tr),
                                  ),
                                  alignLabelWithHint: true,
                                  labelStyle: TextStyle(
                                    fontSize: 18,
                                    fontFamily:
                                        Assets.fontFamilies.alegreyaSansF,
                                    fontWeight: FontWeight.w400,
                                    color: AppColors.hintText,
                                  ),
                                ),
                                style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: Assets.fontFamilies.alegreyaSansF,
                                  fontWeight: FontWeight.w400,
                                  color: AppColors.black,
                                ),
                                obscureText: true,
                                keyboardType: TextInputType.visiblePassword,
                                validator: controller.validatePassword,
                                onChanged: controller.savePassword,
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top: AppSizes.height(context) / 70),
                              child: TextFormField(
                                textInputAction: TextInputAction
                                    .done, // Moves focus to next.

                                decoration: InputDecoration(
                                  alignLabelWithHint: true,
                                  label: Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: Text(LocaleKeys.profileQuote.tr),
                                  ),
                                  labelStyle: TextStyle(
                                    fontSize: 18,
                                    fontFamily:
                                        Assets.fontFamilies.alegreyaSansF,
                                    fontWeight: FontWeight.w400,
                                    color: AppColors.hintText,
                                  ),
                                ),
                                style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: Assets.fontFamilies.alegreyaSansF,
                                  fontWeight: FontWeight.w400,
                                  color: AppColors.black,
                                ),
                                keyboardType: TextInputType.text,
                                validator: controller.validateProfileQuote,
                                onChanged: controller.saveProfileQuote,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding:
                            EdgeInsets.only(top: AppSizes.height(context) / 30),
                        child: ButtonBuilder(
                          onPressed: controller.signUpEmailPassword,
                          backgroundColor: AppColors.green,
                          padding: EdgeInsets.symmetric(
                              vertical: AppSizes.height(context) / 50),
                          borderRadius: 10.0,
                          text: TextBuilder(
                            // width: AppSizes.width(context),
                            // height: AppSizes.width(context) / 13,
                            maxWidth: AppSizes.width(context),
                            maxHeight: AppSizes.width(context) * 0.08,
                            text: LocaleKeys.signup.tr,
                            fontFamily: Assets.fontFamilies.latoF,
                            maxLines: 1,
                            fontWeight: FontWeight.w500,
                            textAlign: TextAlign.center,
                            textColor: AppColors.white,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                          top: AppSizes.height(context) * 0.029,
                          bottom: AppSizes.height(context) * 0.029,
                        ),
                        child: SizedBox(
                          width: AppSizes.width(context),
                          child: LimitedBox(
                            maxWidth: AppSizes.width(context),
                            child: GestureDetector(
                              onTap: () => controller.goBackToLogin(context),
                              child: AutoSizeText.rich(
                                TextSpan(
                                  children: [
                                    TextSpan(
                                      text: "${LocaleKeys.alreadyAccount.tr}  ",
                                      style: const TextStyle(
                                        fontWeight: FontWeight.w500,
                                        color: AppColors.subtitle2,
                                      ),
                                    ),
                                    TextSpan(
                                      text: LocaleKeys.signin.tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.w700,
                                        color: AppColors.subtitle2,
                                      ),
                                    ),
                                  ],
                                ),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontFamily: Assets.fontFamilies.alegreyaSansF,
                                  fontSize: 20,
                                ),
                                minFontSize: 10,
                                maxFontSize: 30,
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
