import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/constants/durations.dart';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../controllers/splash_controller.dart';

class SplashView extends GetView<SplashController> {
  const SplashView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // Future.delayed(const Duration(seconds: 3), () async {
    //   controller.choosePath();
    // // });
    return Scaffold(
      body: TweenAnimationBuilder(
        tween: Tween<double>(begin: 0, end: AppSizes.width(context) * 0.3),
        duration: Durations.animationDurationLong * 2.5,
        builder: (BuildContext context, double value, Widget? child) {
          return Center(
            child: SizedBox(
              width: AppSizes.width(context) * 0.2,
              height: value,
              child: Column(
                children: [
                  Flexible(
                    child: SvgPicture.asset(
                      Assets.logoArt.logoSvg,
                      // width: AppSizes.width(context),
                    ),
                  ),
                  Text(
                    controller.appName,
                    // "Clannad",
                    style: TextStyle(
                      fontFamily: Assets.fontFamilies.alegreyaSansF,
                      color: AppColors.green,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
          );
        },
        // child: Icon(Icons.perm_identity_outlined),
      ),
    );
  }
}

// class SplashView extends GetView<SplashController> {
//   const SplashView({Key? key}) : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     // Future.delayed(const Duration(seconds: 3), () async {
//     //   controller.choosePath();
//     // // });
//     return AnimatedSplashScreen(
//       nextScreen: const HomeView(),
//       splash: SizedBox(
//         width: AppSizes.width(context) * 0.5,
        // child: Column(
        //   children: [
        //     Flexible(
        //       child: SvgPicture.asset(
        //         Assets.logoArt.logoSvg,
        //         // width: AppSizes.width(context),
        //       ),
        //     ),
        //     Text(
        //       controller.appName,
        //       style: TextStyle(
        //         fontFamily: Assets.fontFamilies.alegreyaSansF,
        //         color: AppColors.green,
        //         fontWeight: FontWeight.bold,
        //       ),
        //     ),
        //   ],
        // ),
//       ),
//       duration: 30000,
//       animationDuration: Durations.animationDuration,
//       nextRoute: Routes.SIGNIN,
//       splashTransition: SplashTransition.scaleTransition,
//       pageTransitionType: PageTransitionType.leftToRightWithFade,
//     );
//   }
// }
