import 'dart:async';

import 'package:clannad/app/data/constants/collections.dart';
import 'package:clannad/app/data/constants/getx_ids.dart';
import 'package:clannad/app/data/models/user_model.dart';
import 'package:clannad/app/services/caching_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';

/// The GetX library lacks documentation for SuperController.
/// The following is what I managed to understand from testing.
/// I don't quite understand the difference between [onInactive] and [onPaused],
/// but it is sufficient for our needs for now.
///
class Super extends SuperController {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  StreamSubscription<DocumentSnapshot<Map<String, dynamic>>>?
      _firestoreUpdatesStream;
  late UserModel userModel;

  @override
  void onInit() async {
    super.onInit();

    UserModel? um = await CachingService.getUserModel();
    if (um != null) {
      userModel = um;
    }
    _firebaseAuth.authStateChanges().listen((user) {
      if (user == null) {
        _firestoreUpdatesStream = null;
        return;
      }

      if (_firestoreUpdatesStream != null) {
        return;
      }

      _firestoreUpdatesStream = _firestore
          .collection(Collections.users)
          .doc(user.uid)
          .snapshots()
          .listen((event) async {
        if (event.data() == null) {
          _firestoreUpdatesStream?.cancel();
          return;
        }
        userModel = UserModel.fromJson(event.data()!);
        update([GetxIds.showsEditableUserData]);

        await CachingService.saveUserModel(userModel);
      });
    });
  }

  /// This method is called when app is opened for some reason
  /// seems to be Flutter platform limitation.
  @override
  void onDetached() {
    // debugPrint("I'm onDetached");
    // super.onDetached();
  }

  /// This method is called when app goes to the background.
  @override
  void onInactive() {
    // debugPrint("I'm onInactive");
  }

  /// This method is called when app goes to the background.
  @override
  void onPaused() async {
    // debugPrint("I'm onPaused");
  }

  /// This method is called when app returns from background.
  @override
  void onResumed() async {}
}
