import 'package:clannad/app/data/constants/collections.dart';
import 'package:clannad/app/data/models/user_model.dart';
import 'package:clannad/app/modules/splash/controllers/super_controller.dart';
import 'package:clannad/app/routes/app_pages.dart';
import 'package:clannad/app/services/navigate.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';

class SplashController extends GetxController {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  final String appName = "Clannad";

  String thisYear = DateTime.now().year.toString();
  String thisMonth = DateTime.now().month.toString();
  String thisDay = DateTime.now().day.toString();

  Super get superController => Get.find<Super>();
  @override
  void onInit() {
    super.onInit();
    _choosePath();

    // debugPrint("et");
  }

  // @override
  // void onReady() {
  //   super.onReady();
  // }

  // @override
  // void onClose() {
  //   super.onClose();
  // }

  void _choosePath() async {
    if (_firebaseAuth.currentUser == null) {
      Future.delayed(
        const Duration(seconds: 3),
        () {
          Navigate.getOffAll(Routes.SIGNIN, null);
        },
      );
      return;
    }

    Map<String, dynamic>? data = (await _firestore
            .collection(Collections.users)
            .doc(_firebaseAuth.currentUser!.uid)
            .get())
        .data();
    if (data == null) {
      Future.delayed(
        const Duration(seconds: 3),
        () {
          Navigate.getOffAll(Routes.SIGNIN, null);
        },
      );
      return;
    }

    // UserModel usermodel = UserModel.fromJson(data);

    superController.userModel = UserModel.fromJson(data);
    // UserModel? userWithResetStreak;

    if (superController.userModel.profileQuote == null) {
      Future.delayed(
        const Duration(seconds: 3),
        () {
          // Navigate.getOffAll(Routes.HOME, null);
        },
      );
      return;
    }

    DateTime now = DateTime.now();
    DateTime lastMeditation = DateTime.fromMillisecondsSinceEpoch(
        int.parse(superController.userModel.lastStreakUpdateTimestamp));

    if (now.difference(lastMeditation) > const Duration(days: 1)) {
      await _firestore
          .collection(Collections.users)
          .doc(_firebaseAuth.currentUser!.uid)
          .update({
        "current_streak": 0,
        "last_streak_update": Timestamp.now().millisecondsSinceEpoch.toString(),
      });
    }
    // if (usermodel.meditationHistoryDetails[thisYear]?[thisMonth]?[thisDay] == )
    Future.delayed(
      const Duration(seconds: 3),
      () {
        // userWithResetStreak != null
        //     ? superController.userModel = userWithResetStreak
        //     : null;
        Navigate.getOffAll(Routes.HOME, null);
      },
    );
    return;
  }
}
