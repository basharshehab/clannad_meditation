import 'package:clannad/app/modules/splash/controllers/super_controller.dart';
import 'package:get/get.dart';

import '../controllers/splash_controller.dart';

class SplashBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SplashController>(
      () => SplashController(),
    );
    Get.put<Super>(
      Super(),
      permanent: true,
    );
  }
}
