import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/constants/paddings.dart';
import 'package:clannad/app/modules/settings/components/langauge_tile.dart';
import 'package:clannad/app/modules/settings/controllers/settings_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ClannadBottomSheet {
  static languageBottomSheet(
      SettingsController controller, BuildContext context) {
    return Get.bottomSheet(
      Container(
        height: AppSizes.height(context) * 0.25,
        decoration: const BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(16),
            topRight: Radius.circular(16),
          ),
        ),
        padding: Paddings.allPadding(context),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            LanguageTile(
              onTap: () {
                controller.changeAppLocale(const Locale("de"), context);
              },
              flagAssetPath: Assets.countryFlags.german,
              titleText: "Detusch",
              context: context,
            ),
            LanguageTile(
              onTap: () {
                controller.changeAppLocale(const Locale("en"), context);
              },
              flagAssetPath: Assets.countryFlags.english,
              context: context,
              titleText: "English",
            ),
            LanguageTile(
              onTap: () {
                controller.changeAppLocale(const Locale("ar"), context);
              },
              flagAssetPath: Assets.countryFlags.arabic,
              context: context,
              titleText: "العربية",
            ),
          ],
        ),
      ),
    );
  }
}
