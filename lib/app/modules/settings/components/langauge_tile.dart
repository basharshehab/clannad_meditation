import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/general_components/text_builder.dart';
import 'package:clannad/app/services/layout_helper.dart';
import 'package:flutter/material.dart';

class LanguageTile extends StatelessWidget {
  final String titleText;
  final Function()? onTap;
  final String flagAssetPath;
  final BuildContext context;
  const LanguageTile({
    Key? key,
    required this.titleText,
    this.onTap,
    required this.flagAssetPath,
    required this.context,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: TextBuilder(
        textAlign:
            LayoutHelper.isRTL(context) ? TextAlign.right : TextAlign.left,
        maxWidth: AppSizes.width(context),
        text: titleText,
        fontFamily: Assets.fontFamilies.alegreyaSansF,
        maxLines: 1,
        fontWeight: FontWeight.bold,
        textColor: AppColors.black,
        maxHeight: AppSizes.height(context) * 0.03,
      ),
      leading: Container(
        width: 25,
        height: 25,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(500),
          color: AppColors.white,
          image: DecorationImage(
            image: AssetImage(
                // Assets.countryFlags.german,
                flagAssetPath
                // height: 20,
                ),
          ),
        ),
      ),
      contentPadding: const EdgeInsets.all(0),
      onTap: onTap,
    );
  }
}
