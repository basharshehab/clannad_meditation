import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/services/navigate.dart';
import 'package:flutter/material.dart';

class SettingsBanner extends StatelessWidget {
  const SettingsBanner({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ShaderMask(
          shaderCallback: (Rect bounds) {
            return const LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                // Colors.purple,
                AppColors.transparent,
                AppColors.transparent,
                AppColors.white,
              ],
              stops: [
                // 0.0,
                0.1,
                0.8,
                1.0
              ], // 10% purple, 80% transparent, 10% purple
            ).createShader(bounds);
          },
          blendMode: BlendMode.dst,
          child: Image.asset(
            Assets.logoArt.fullArt,
            fit: BoxFit.cover,
          ),
        ),
        SafeArea(
          child: BackButton(
            color: AppColors.green,
            onPressed: () => Navigate.getBack(),
          ),
        ),
      ],
    );
  }
}
