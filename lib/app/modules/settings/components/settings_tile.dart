import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/general_components/text_builder.dart';
import 'package:clannad/app/modules/settings/controllers/settings_controller.dart';
import 'package:clannad/app/services/layout_helper.dart';
import 'package:flutter/material.dart';

class SettingsTile extends StatelessWidget {
  final Function()? onPressed;
  final String label;
  final Widget icon;
  final Color? textColor;
  const SettingsTile(
      {Key? key,
      required this.controller,
      this.onPressed,
      required this.label,
      required this.icon,
      this.textColor})
      : super(key: key);

  final SettingsController controller;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: icon,
      title: TextBuilder(
        textAlign:
            LayoutHelper.isRTL(context) ? TextAlign.right : TextAlign.left,
        maxWidth: AppSizes.width(context),
        text: label,
        fontFamily: Assets.fontFamilies.alegreyaSansF,
        maxLines: 1,
        fontWeight: FontWeight.bold,
        textColor: textColor ?? AppColors.black,
        maxHeight: AppSizes.height(context) * 0.03,
      ),
      onTap: onPressed,
    );
  }
}
