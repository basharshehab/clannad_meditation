import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/modules/settings/components/clannad_language_bottom_sheet.dart';
import 'package:clannad/app/modules/settings/components/settings_banner.dart';
import 'package:clannad/app/modules/settings/components/settings_tile.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/settings_controller.dart';

class SettingsView extends GetView<SettingsController> {
  const SettingsView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const SettingsBanner(),
          SettingsTile(
            controller: controller,
            icon: const Icon(
              Icons.language,
              color: AppColors.green,
            ),
            label: LocaleKeys.appLanguage.tr,
            onPressed: () =>
                ClannadBottomSheet.languageBottomSheet(controller, context),
          ),
          SettingsTile(
            controller: controller,
            icon: const Icon(
              Icons.delete,
              color: AppColors.red,
            ),
            label: LocaleKeys.deleteAccount.tr,
            textColor: AppColors.red,
            onPressed: () => controller.popDeleteAccountDialog(context),
          ),
        ],
      ),
    );
  }
}
