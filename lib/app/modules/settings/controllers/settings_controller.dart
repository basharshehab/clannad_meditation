import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/collections.dart';
import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/modules/splash/controllers/super_controller.dart';
import 'package:clannad/app/routes/app_pages.dart';
import 'package:clannad/app/services/caching_service.dart';
import 'package:clannad/app/services/navigate.dart';
import 'package:clannad/app/services/storage_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SettingsController extends GetxController {
  Locale? currentLocal;

  late Super superController;
  @override
  void onInit() {
    super.onInit();
    currentLocal = Get.locale;
  }

  @override
  void onReady() {
    super.onReady();
    superController = Get.find<Super>();
  }

  // @override
  // void onClose() {
  //   super.onClose();
  // }

  void changeAppLocale(Locale l, BuildContext context) async {
    if (currentLocal == l) {
      Navigate.getBack();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(LocaleKeys.languageAlreadySet.tr),
        ),
      );
      return;
    }
    Get.updateLocale(l);
    currentLocal = l;
    Navigate.getBack();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(LocaleKeys.restartAppFullChanges.tr),
      ),
    );
    await CachingService.saveAppLanguage(l.languageCode);
  }

  void popDeleteAccountDialog(BuildContext context) async {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        TextEditingController tc = TextEditingController();
        return AlertDialog(
          title: Text(LocaleKeys.pleaseEnterPassword.tr),
          content: Material(
            child: TextField(
              cursorColor: AppColors.green,
              controller: tc,
              obscureText: true,
              keyboardType: TextInputType.visiblePassword,
              // onChanged: (x) => _tc.text = x,
              decoration: InputDecoration(
                hintText: LocaleKeys.password.tr,
                filled: true,
                border: InputBorder.none,
                fillColor: AppColors.white,
                focusColor: AppColors.white,
                hintStyle: TextStyle(
                  fontFamily: Assets.fontFamilies.alegreyaSansF,
                ),
                hoverColor: AppColors.white,
              ),
            ),
          ),
          actionsAlignment: MainAxisAlignment.spaceEvenly,
          actions: [
            TextButton(
              onPressed: Navigate.getBack,
              child: Text(
                LocaleKeys.goBack.tr,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: Assets.fontFamilies.alegreyaSansF,
                    color: AppColors.green),
              ),
            ),
            TextButton(
              onPressed: () => _deleteAccount(
                tc.text,
              ),
              child: Text(
                textAlign: TextAlign.center,
                LocaleKeys.deleteAccount.tr,
                style: TextStyle(
                    fontFamily: Assets.fontFamilies.alegreyaSansF,
                    color: AppColors.red),
              ),
            ),
          ],
        );
      },
    );
  }

  Future<void> _deleteAccount(String password) async {
    Navigate.getBack();
    final FirebaseAuth auth = FirebaseAuth.instance;
    final FirebaseFirestore firestore = FirebaseFirestore.instance;
    AuthCredential credentials = EmailAuthProvider.credential(
        email: superController.userModel.emailAddress, password: password);

    try {
      await auth.currentUser!.reauthenticateWithCredential(credentials);
    } on FirebaseAuthException catch (e) {
      switch (e.code) {
        case "user-mismatch":
          Get.snackbar(
            // "Can't delete the account",
            LocaleKeys.cantDeleteAccount.tr,
            LocaleKeys.wrongPassword.tr,
          );
          return;
        case "user-not-found":
          Get.snackbar(
            LocaleKeys.cantDeleteAccount.tr,
            LocaleKeys.userNotFound.tr,
          );
          return;
        case "invalid-credential":
          Get.snackbar(
            LocaleKeys.cantDeleteAccount.tr,
            LocaleKeys.wrongPassword.tr,
          );
          return;
        case "invalid-email":
          Get.snackbar(
            LocaleKeys.cantDeleteAccount.tr,
            LocaleKeys.invalidEmail.tr,
          );
          return;
        case "wrong-password:":
          Get.snackbar(
            LocaleKeys.cantDeleteAccount.tr,
            LocaleKeys.wrongPassword.tr,
          );
          return;
        case "invalid-verification-code":
          Get.snackbar(
            LocaleKeys.cantDeleteAccount.tr,
            LocaleKeys.wrongPassword.tr,
          );
          return;
        case "invalid-verification-id":
          Get.snackbar(
            LocaleKeys.cantDeleteAccount.tr,
            LocaleKeys.wrongPassword.tr,
          );
          return;
        default:
          Get.snackbar(
            LocaleKeys.cantDeleteAccount.tr,
            LocaleKeys.wrongPassword.tr,
          );
          return;
      }
    } catch (_) {
      Get.snackbar(
        LocaleKeys.cantDeleteAccount.tr,
        LocaleKeys.unknownError.tr,
      );
      return;
    }

    bool? imageDeleted;

    try {
      imageDeleted =
          await StorageService.deleteUserImage(userId: auth.currentUser!.uid);
    } catch (e) {
      debugPrint(e.toString());
    }

    if (imageDeleted == null) {
      Get.snackbar(
        LocaleKeys.cantDeleteAccount.tr,
        LocaleKeys.tryAgainLater.tr,
      );
      return;
    }

    try {
      await firestore
          .collection(Collections.users)
          .doc(auth.currentUser!.uid)
          .delete();
    } catch (_) {
      Get.snackbar(
        LocaleKeys.cantDeleteAccount.tr,
        // "Can't delete your account at the moment",
        LocaleKeys.tryAgainLater.tr,
      );
      return;
    }

    try {
      await auth.currentUser!.delete();
    } catch (_) {
      Get.snackbar(
        LocaleKeys.cantDeleteAccount.tr,
        // "Can't delete your account at the moment",
        LocaleKeys.tryAgainLater.tr,
      );
      return;
    }

    try {
      await auth.signOut();
      Navigate.getOffAll(Routes.SIGNIN, null);
    } catch (_) {
      Navigate.getOffAll(Routes.SIGNIN, null);
      return;
    }
  }
}
