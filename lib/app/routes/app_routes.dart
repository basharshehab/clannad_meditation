part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart
// ignore_for_file: constant_identifier_names

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const SPLASH = _Paths.SPLASH;
  static const TEST = _Paths.TEST;
  static const SIGNIN = _Paths.SIGNIN;
  static const SIGNUP = _Paths.SIGNUP;
  static const RECOVERY = _Paths.RECOVERY;
  static const PROFILE = _Paths.PROFILE;
  static const SETTINGS = _Paths.SETTINGS;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const SPLASH = '/splash';
  static const TEST = '/test';
  static const SIGNIN = '/signin';
  static const SIGNUP = '/signup';
  static const RECOVERY = '/recovery';
  static const PROFILE = '/profile';
  static const SETTINGS = '/settings';
}
