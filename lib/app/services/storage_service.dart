import 'dart:io';

import 'package:clannad/app/data/constants/cloud_storage_const.dart';
import 'package:clannad/app/data/constants/collections.dart';
import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:clannad/app/data/models/user_model.dart';
import 'package:clannad/app/modules/splash/controllers/super_controller.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

class StorageService {
  static final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  static final FirebaseFirestore _firebaseFirestore =
      FirebaseFirestore.instance;

  static final FirebaseStorage _storage = FirebaseStorage.instance;

  static Future<void> uploadUserImage({
    required File userImage,
    required String userId,
  }) async {
    if (!await InternetConnectionChecker().hasConnection) {
      Get.snackbar(
          LocaleKeys.noConnection.tr, LocaleKeys.checkInternetConnection.tr);
      return;
    }
    String fileName = userId;
    late String imageUrl;
    try {
      imageUrl = await (await _storage
              .ref()
              .child("${CloudStorageConst.profileImages}$fileName")
              .putFile(userImage))
          .ref
          .getDownloadURL();
    } catch (_) {
      Get.snackbar(
          LocaleKeys.couldntUploadImage.tr, LocaleKeys.tryAgainLater.tr);
      return;
    }
    try {
      Super superController = Get.find<Super>();

      UserModel userModel =
          superController.userModel.copyWith(imageUrl: imageUrl);

      await _firebaseFirestore
          .collection(Collections.users)
          .doc(_firebaseAuth.currentUser!.uid)
          .update(userModel.toJson());
    } catch (_) {
      Get.snackbar(
          LocaleKeys.couldntUploadImage.tr, LocaleKeys.tryAgainLater.tr);
      return;
    }
  }

  static Future<bool> deleteUserImage({
    required String userId,
  }) async {
    try {
      await _storage
          .ref()
          .child("${CloudStorageConst.profileImages}$userId")
          .delete();
      return true;
    } catch (_) {
      return false;
    }
  }
}

class CloudStorageResult {
  final String imageUrl;
  final String imageFileName;
  CloudStorageResult({required this.imageUrl, required this.imageFileName});
}
