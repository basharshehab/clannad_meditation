// ignore_for_file: library_private_types_in_public_api
import 'package:clannad/app/data/models/user_model.dart';
import 'package:get_storage/get_storage.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
// import 'package:json_cache/json_cache.dart';

class CachingService {
  static GetStorage storage = GetStorage();

  static Future<void> saveAppLanguage(String lang) async {
    // final prefs = await SharedPreferences.getInstance();
    // final JsonCache jsonCache = JsonCacheMem(JsonCachePrefs(prefs));

    // await jsonCache.refresh(Caching.keys.userPreferences, {
    //   Caching.valueKeys.language: lang,
    // });

    await storage.write(Caching.keys.language, lang);
  }

  static String? getAppLanguage() {
    // final prefs = await SharedPreferences.getInstance();
    // final JsonCache jsonCache = JsonCacheMem(JsonCachePrefs(prefs));

    // Map<String, dynamic>? userPreferences = await jsonCache.value(
    //   Caching.keys.userPreferences,
    // );

    // return userPreferences?[Caching.valueKeys.language];
    return storage.read(Caching.keys.language);
  }

  static Future<void> saveUserModel(UserModel userModel) async {
    // final prefs = await SharedPreferences.getInstance();
    // final JsonCache jsonCache = JsonCacheMem(JsonCachePrefs(prefs));

    // await jsonCache.refresh(Caching.keys.userPreferences,
    //     {Caching.valueKeys.userModel: userModel.toJson()});

    await storage.write(Caching.keys.userModel, userModel.toJson());
    await storage.write(
        Caching.keys.userModelTimestamp, DateTime.now().toIso8601String());
  }

  static Future<UserModel?> getUserModel() async {
    // final prefs = await SharedPreferences.getInstance();
    // final JsonCache jsonCache = JsonCacheMem(JsonCachePrefs(prefs));

    // Map<String, dynamic>? userPreferences =
    //     await jsonCache.value(Caching.keys.userPreferences);
    Map<String, dynamic>? userModelMap = storage.read(Caching.keys.userModel);
    String? timestampStr = storage.read(Caching.keys.userModelTimestamp);

    if (timestampStr == null) {
      return null;
    }
    DateTime? timestamp = DateTime.tryParse(timestampStr);

    if (timestamp == null) {
      return null;
    }
    if (timestamp.isBefore(DateTime.now().subtract(const Duration(days: 1)))) {
      if ((await InternetConnectionChecker().hasConnection)) {
        return null;
      }
    }

    UserModel userModel = UserModel.fromJson(userModelMap!);

    return userModel;
  }

  static Future<void> saveMusic(List<Map<String, dynamic>> music) async {
    await storage.write(Caching.keys.music, music);
    await storage.write(
        Caching.keys.musicTimestamp, DateTime.now().toIso8601String());
  }

  static Future<List<Map<String, dynamic>>?> getCachedMusic() async {
    List<dynamic>? s = storage.read(Caching.keys.music);
    String? timestampStr = storage.read(Caching.keys.musicTimestamp);

    if (timestampStr == null) {
      return null;
    }
    DateTime? timestamp = DateTime.tryParse(timestampStr);

    if (timestamp == null) {
      return null;
    }
    if (timestamp.isBefore(DateTime.now().subtract(const Duration(days: 1)))) {
      if ((await InternetConnectionChecker().hasConnection)) {
        return null;
      }
    }
    // List<dynamic> data = s;
    List<Map<String, dynamic>> data =
        List<Map<String, dynamic>>.from(s!.map((i) => i));

    return data;
  }

  static Future<void> saveCourses(List<Map<String, dynamic>> courses) async {
    await storage.write(Caching.keys.coursesList, courses);
    await storage.write(
        Caching.keys.coursesListTimestamp, DateTime.now().toIso8601String());
  }

  static Future<List<Map<String, dynamic>>?> getCachedCourses() async {
    List<dynamic>? s = storage.read(Caching.keys.coursesList);
    String? timestampStr = storage.read(Caching.keys.coursesListTimestamp);

    if (timestampStr == null) {
      return null;
    }
    DateTime? timestamp = DateTime.tryParse(timestampStr);

    if (timestamp == null) {
      return null;
    }
    if (timestamp.isBefore(DateTime.now().subtract(const Duration(days: 1)))) {
      if ((await InternetConnectionChecker().hasConnection)) {
        return null;
      }
    }
    // List<dynamic> data = s;
    List<Map<String, dynamic>> data =
        List<Map<String, dynamic>>.from(s!.map((i) => i));

    return data;
  }
}

class Caching {
  // static final _Keys keys = _Keys();
  static final _Keys keys = _Keys();
}

class _Keys {
  static final _Keys _singleton = _Keys._internal();

  factory _Keys() {
    return _singleton;
  }

  _Keys._internal();

  final String language = "language";
  final String music = "music";
  final String musicTimestamp = "music_timestamp";
  final String userModel = "user_model";
  final String userModelTimestamp = "user_model_timestamp";
  final String coursesListTimestamp = "courses_list_timestamp";
  final String coursesList = "courses_list";
}
