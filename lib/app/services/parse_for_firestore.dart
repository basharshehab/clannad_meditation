import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:get/get.dart';

class ParseForFirestore {
  static String feelingsFromLocale(String feeling) {
    feeling = feeling.trim();
    feeling = feeling.toLowerCase();
    switch (feeling) {
      case "anxious":
        return "anxious";
      case "okay":
        return "okay";
      case "focused":
        return "focused";
      case "relaxed":
        return "relaxed";
      case "unruhig":
        return "anxious";
      case "fokussiert":
        return "focused";
      case "entpannt":
        return "relaxed";
      case "القلق":
        return "anxious";
      case "حالة جيدة":
        return "okay";
      case "التركيز":
        return "focused";
      case "الراحة":
        return "relaxed";
      default:
        return "unknown";
      // case "anxious":
    }
  }

  static String feelingsToLocale(String feeling) {
    feeling = feeling.trim();
    feeling = feeling.toLowerCase();

    // String lang = Get.locale!.languageCode;

    switch (feeling) {
      case "anxious":
        return LocaleKeys.feelingAnxious.tr;
      case "okay":
        return LocaleKeys.feelingOkay.tr;

      case "focused":
        return LocaleKeys.feelingFocused.tr;

      case "relaxed":
        return LocaleKeys.feelingRelaxed.tr;

      default:
        return LocaleKeys.unknown.tr;
    }
  }
}
