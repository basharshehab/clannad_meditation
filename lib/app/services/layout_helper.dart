import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class LayoutHelper {
  static bool isRTL(BuildContext context) => Directionality.of(context)
      .toString()
      .contains(TextDirection.rtl.name.toLowerCase());

  static String durationToMinutesSecondsDoubleDigits(Duration duration) {
    // String _printDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "$twoDigitMinutes:$twoDigitSeconds";
    // }
  }

  /// Returns the duration in a human readable text form
  static String durationToHumanReadableDoubleDigits(Duration duration) {
    String hours =
        duration.inHours > 1 ? LocaleKeys.hours.tr : LocaleKeys.hours.tr;
    String minutes = duration.inMinutes.remainder(60) > 1
        ? LocaleKeys.minutes.tr
        : LocaleKeys.minute.tr;
    String seconds = duration.inSeconds.remainder(60) > 1
        ? LocaleKeys.seconds.tr
        : LocaleKeys.second.tr;
    return "${duration.inHours} $hours, ${duration.inMinutes.remainder(60)} $minutes, ${LocaleKeys.and.tr} ${(duration.inSeconds.remainder(60))} $seconds.";
  }

  static String generateFeelingsResponse(String todayFeeling) {
    String response;

    if (LocaleKeys.feelingOkay.tr.toLowerCase() == todayFeeling.toLowerCase()) {
      response = "\n${LocaleKeys.feelingResponseOkay.tr}";
    } else if (LocaleKeys.feelingAnxious.tr.toLowerCase() ==
        todayFeeling.toLowerCase()) {
      response = "\n${LocaleKeys.feelingResponseAnxious.tr}";
    } else if (LocaleKeys.feelingRelaxed.tr.toLowerCase() ==
        todayFeeling.toLowerCase()) {
      response = "\n${LocaleKeys.feelingResponseRelaxed.tr}";
    } else if (LocaleKeys.feelingFocused.tr.toLowerCase() ==
        todayFeeling.toLowerCase()) {
      response = "\n${LocaleKeys.feelingResponseFocused.tr}";
    } else {
      response = "\n${LocaleKeys.iWonderWhy.tr}";
    }
    return response;
  }

  static int daysOfMonth(int month) {
    switch (month) {
      case 1:
        return 31;
      case 2:
        return _isLeapYear(DateTime.now().year) ? 29 : 28;
      case 3:
        return 31;
      case 4:
        return 30;
      case 5:
        return 31;
      case 6:
        return 30;
      case 7:
        return 31;
      case 8:
        return 31;
      case 9:
        return 30;
      case 10:
        return 31;
      case 11:
        return 30;
      case 12:
        return 31;
      default:
        return 0;
    }
  }

  static bool _isLeapYear(int year) {
    if (year % 4 == 0) {
      if (year % 100 == 0) {
        if (year % 400 == 0) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}
