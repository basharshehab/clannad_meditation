import 'package:clannad/app/data/locale/locale_keys.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';

class Converter {
  static intToDay(int i) {
    assert(i <= 7 && i >= 1);
    switch (i) {
      case 1:
        return LocaleKeys.monday.tr;
      case 2:
        return LocaleKeys.tuesday.tr;
      case 3:
        return LocaleKeys.wednesday.tr;
      case 4:
        return LocaleKeys.thursday.tr;
      case 5:
        return LocaleKeys.friday.tr;
      case 6:
        return LocaleKeys.saturday.tr;
      case 7:
        return LocaleKeys.sunday.tr;
    }
  }

  static List<Map<String, dynamic>> querySnapshotToMap(
      List<QueryDocumentSnapshot<Map<String, dynamic>>> query) {
    List<Map<String, dynamic>> data = [];
    for (int i = 0; i < query.length; i++) {
      data.add(query[i].data());
    }
    return data;
  }

  // static intToMonth(int i) {
  //   assert(i <= 12 && i >= 1);
  //   switch (i) {
  //     case 1:
  //       return LocaleKeys.january.tr;
  //     case 2:
  //       return LocaleKeys.february.tr;
  //     case 3:
  //       return LocaleKeys.march.tr;
  //     case 4:
  //       return LocaleKeys.april.tr;
  //     case 5:
  //       return LocaleKeys.may.tr;
  //     case 6:
  //       return LocaleKeys.june.tr;
  //     case 7:
  //       return LocaleKeys.july.tr;
  //     case 8:
  //       return LocaleKeys.august.tr;
  //     case 9:
  //       return LocaleKeys.september.tr;
  //     case 10:
  //       return LocaleKeys.october.tr;
  //     case 11:
  //       return LocaleKeys.november.tr;
  //     case 12:
  //       return LocaleKeys.december.tr;
  //   }
  // }
}
