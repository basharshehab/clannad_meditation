import 'package:get/utils.dart';

class Validator {
  static bool isEmail(String email) {
    return GetUtils.isEmail(email.toLowerCase().trim());
  }

  static bool isPassword(String pass) {
    return pass.trim().isEmpty ||
        RegExp(r'^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})')
            .hasMatch(pass.trim());
  }

  static bool isStringOnly(String string) {
    return RegExp(r"^[a-zA-Z]*$").hasMatch(string.trim()) &&
        string.trim().length > 1;
  }

  static bool isNum(String string) {
    return GetUtils.isNum(string.trim());
  }

  static bool isPhone(String string) {
    return GetUtils.isPhoneNumber(string.trim());
  }
}
