import 'package:get/get.dart';

class Navigate {
  static getTo(String route, arguments) {
    Get.toNamed(route, arguments: arguments);
  }

  static getOff(String route, arguments) {
    Get.offNamed(route, arguments: arguments);
  }

  static getBack() {
    Get.back();
  }

  static getOffAll(String route, arguments) {
    Get.offAllNamed(route, arguments: arguments);
  }
}
