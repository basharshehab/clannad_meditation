import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:flutter/widgets.dart';

class Paddings {
  static double horizontal(context) {
    return AppSizes.width(context) / 20;
  }

  static double vertical(context) {
    return AppSizes.height(context) / 175;
  }

  static EdgeInsets horizontalPadding(BuildContext context) {
    return EdgeInsets.symmetric(horizontal: horizontal(context));
  }

  static EdgeInsets verticalPadding(BuildContext context) {
    return EdgeInsets.symmetric(vertical: vertical(context));
  }

  static EdgeInsets allPadding(BuildContext context) {
    return EdgeInsets.symmetric(
        horizontal: horizontal(context), vertical: vertical(context));
  }
}
