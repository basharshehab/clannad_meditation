import 'package:flutter/material.dart';

class AppColors {
  // General
  static const Color green = Color.fromARGB(255, 97, 177, 90);
  static const Color red = Color.fromARGB(255, 170, 49, 49);
  static const Color darkGreen = Color.fromARGB(255, 75, 145, 69);
  static const Color lightGreen = Color.fromARGB(255, 173, 206, 116);
  static const Color white = Color.fromARGB(255, 254, 254, 254);
  static const Color black = Color.fromARGB(255, 51, 51, 51);
  static const Color lightBlack = Color.fromARGB(178, 51, 51, 51);

  static const Color subtitle1 = Color.fromARGB(255, 66, 72, 74);
  static const Color subtitle1_50 = Color.fromARGB(128, 66, 72, 74);
  static const Color subtitle2 = Color.fromARGB(255, 108, 113, 114);
  static const Color subtitle3 = Color.fromARGB(255, 190, 194, 194);
  static const Color inActive = Color.fromARGB(255, 171, 174, 175);
  static const Color description = Color.fromARGB(255, 129, 133, 134);
  static const Color hintText = Color.fromARGB(255, 134, 141, 141);
  static const Color stats1 = lightGreen;
  static const Color stats2 = Color.fromARGB(255, 255, 247, 106);
  static const Color courseBackground = Color.fromARGB(255, 244, 248, 236);
  static const Color bottomIconInactive = Color.fromARGB(255, 160, 163, 164);
  static const Color bottomIconActive = green;
  static const Color transparent = Color.fromARGB(0, 0, 0, 0);
  static const Color unplayedBars = Color.fromARGB(255, 205, 230, 203);
  static const Color appBarButton = Color.fromARGB(255, 181, 184, 184);
  static const Color audioPlayerInActive = Color(0xFF818586);
}
