import 'package:flutter/material.dart';

class AppSizes {
  static double width(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  static double height(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }

  static double titleTxt(BuildContext context) {
    return MediaQuery.of(context).size.width / 2;
  }

  static double subtitleTxt(BuildContext context) {
    return MediaQuery.of(context).size.width / 4;
  }

  static double bottomBar(BuildContext context) {
    return AppSizes.height(context) * 0.084;
  }

  static double appBar(BuildContext context) {
    return AppSizes.height(context) * 0.11;
  }
}
