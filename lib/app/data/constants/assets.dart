// ignore_for_file: constant_identifier_names, library_private_types_in_public_api

class Assets {
  static final _AppBar appBar = _AppBar();
  static final _Audio audio = _Audio();
  static final _MusicControls musicControls = _MusicControls();
  static final _Backgrounds backgrounds = _Backgrounds();
  static final _BottomBar bottomBar = _BottomBar();
  static final _Characters characters = _Characters();
  static final _Feelings feelings = _Feelings();
  static final _Meditation meditation = _Meditation();
  static final _LogoArt logoArt = _LogoArt();
  static final _FontFamilies fontFamilies = _FontFamilies();
  static final _CountryFlags countryFlags = _CountryFlags();
  static final _SocialMedia socialMedia = _SocialMedia();
}

class _Base {
  static final _Base _singleton = _Base._internal();

  factory _Base() {
    return _singleton;
  }

  _Base._internal();

  final String _base = 'assets';
  final String _baseAppbar = 'assets/app_bar_icons';
  final String _baseSocialMedia = 'assets/social_media';
  final String _baseMusicControls = 'assets/music_controls';
  final String _baseAudio = 'assets/audio_files';
  final String _baseBackgrounds = 'assets/backgrounds';
  final String _baseBottomBar = 'assets/bottom_bar_icons';
  final String _baseCharacters = 'assets/characters';
  final String _baseCourseBackgrounds = 'assets/course_backgrounds';
  final String _baseFeelings = 'assets/feelings';
  final String _baseMeditationSessions = 'assets/meditation_sessions';
  final String _countryFlags = 'assets/country_flags';
}

class _AppBar {
  static final _AppBar _singleton = _AppBar._internal();

  factory _AppBar() {
    return _singleton;
  }

  _AppBar._internal();
  static final _Base base = _Base();
  // AppBar Icons
  final String appBarHamburgerSvg = "${base._baseAppbar}/hamburger.svg";
  final String appBarLogoSvg = "${base._baseAppbar}/logo.svg";
}

class _SocialMedia {
  static final _SocialMedia _singleton = _SocialMedia._internal();

  factory _SocialMedia() {
    return _singleton;
  }

  _SocialMedia._internal();
  static final _Base base = _Base();
  // AppBar Icons
  final String googleLogo = "${base._baseSocialMedia}/google.svg";
  // final String appBarLogoSvg = "${base._baseAppbar}/logo.svg";
}

class _MusicControls {
  static final _MusicControls _singleton = _MusicControls._internal();

  factory _MusicControls() {
    return _singleton;
  }

  _MusicControls._internal();
  static final _Base base = _Base();
  // AppBar Icons
  final String repeat = "${base._baseMusicControls}/repeat.svg";
  final String repeatOne = "${base._baseMusicControls}/repeat_one.svg";
  final String shuffle = "${base._baseMusicControls}/shuffle.svg";
  final String skipAhead = "${base._baseMusicControls}/skip_ahead.svg";
  final String skipBack = "${base._baseMusicControls}/skip_back.svg";
}

class _Audio {
  static final _Audio _singleton = _Audio._internal();

  factory _Audio() {
    return _singleton;
  }

  _Audio._internal();

  static final _Base base = _Base();

  // Audio types
  final String audio1Png = "${base._baseAudio}/audio_1.png";
  final String audio2Png = "${base._baseAudio}/audio_2.png";
  final String audio3Png = "${base._baseAudio}/audio_3.png";
  final String audio4Png = "${base._baseAudio}/audio_4.png";
}

class _Backgrounds {
  static final _Backgrounds _singleton = _Backgrounds._internal();

  factory _Backgrounds() {
    return _singleton;
  }

  _Backgrounds._internal();
  final _CourseBackgrounds courseBackgrounds = _CourseBackgrounds();
  final _WidgetBackgrounds widgetBackgrounds = _WidgetBackgrounds();
}

class _CourseBackgrounds {
  static final _CourseBackgrounds _singleton = _CourseBackgrounds._internal();

  factory _CourseBackgrounds() {
    return _singleton;
  }

  _CourseBackgrounds._internal();
  static final _Base base = _Base();
  // Courses backgrounds
  final String courseBackground1 =
      "${base._baseCourseBackgrounds}/course_background_1.png";
  final String courseBackground2 =
      "${base._baseCourseBackgrounds}/course_background_2.png";
}

class _WidgetBackgrounds {
  static final _WidgetBackgrounds _singleton = _WidgetBackgrounds._internal();

  factory _WidgetBackgrounds() {
    return _singleton;
  }

  _WidgetBackgrounds._internal();
  static final _Base base = _Base();
  // Widget Backgrounds
  final String backgroundLeafCroppedSvg =
      "${base._baseBackgrounds}/background_leaf_cropped.svg";
  final String backgroundLeafSvg =
      "${base._baseBackgrounds}/background_leaf.svg";
  final String backgroundCloud = "${base._baseBackgrounds}/cloud.svg";
  final String backgroundPlayWidget =
      "${base._baseBackgrounds}/play_widget_background.png";
}

class _BottomBar {
  static final _BottomBar _singleton = _BottomBar._internal();

  factory _BottomBar() {
    return _singleton;
  }

  _BottomBar._internal();
  static final _Base base = _Base();

  // BottomBar icons
  final String bottomBarHomeActive =
      "${base._baseBottomBar}/bottom_bar_home_active.svg";
  final String bottomBarHomeInActive =
      "${base._baseBottomBar}/bottom_bar_home_inactive.svg";
  final String bottomBarMusicInActive =
      "${base._baseBottomBar}/bottom_bar_music_inactive.svg";
  final String bottomBarMusicActive =
      "${base._baseBottomBar}/bottom_bar_music_active.svg";
  final String bottomBarMeditateActive =
      "${base._baseBottomBar}/bottom_bar_meditate_active.svg";
  final String bottomBarMeditateInActive =
      "${base._baseBottomBar}/bottom_bar_meditate_inactive.svg";
}

class _Characters {
  static final _Characters _singleton = _Characters._internal();

  factory _Characters() {
    return _singleton;
  }

  _Characters._internal();
  static final _Base base = _Base();
  // Characters

  final String characterSvg1 =
      "${base._baseCharacters}/meditating_character.svg";
}

class _Feelings {
  static final _Feelings _singleton = _Feelings._internal();

  factory _Feelings() {
    return _singleton;
  }

  _Feelings._internal();
  static final _Base base = _Base();
  // Feelings
  final String feelingAnxiousSvg = "${base._baseFeelings}/anxious.svg";
  final String feelingCalmSvg = "${base._baseFeelings}/calm.svg";
  final String feelingFocus = "${base._baseFeelings}/focus.svg";
  final String feelingRelax = "${base._baseFeelings}/relax.svg";
}

class _Meditation {
  static final _Meditation _singleton = _Meditation._internal();

  factory _Meditation() {
    return _singleton;
  }

  _Meditation._internal();
  static final _Base base = _Base();

  // Meditation/Sleep Sessions
  final String shortSessionHalfmoon =
      "${base._baseMeditationSessions}/half_moon.svg";
  final String mediumSessionStar = "${base._baseMeditationSessions}/star.svg";
  final String longSessionZ = "${base._baseMeditationSessions}/z.svg";
}

class _LogoArt {
  static final _LogoArt _singleton = _LogoArt._internal();

  factory _LogoArt() {
    return _singleton;
  }

  _LogoArt._internal();
  static final _Base base = _Base();
  // Logo and Art
  final String logoSvg = "${base._baseAppbar}/logo.svg";
  final String fullArt = "${base._base}/fullart.png";
}

class _FontFamilies {
  static final _FontFamilies _singleton = _FontFamilies._internal();

  factory _FontFamilies() {
    return _singleton;
  }

  _FontFamilies._internal();
  // Font Families
  final String alegreyaF = "Alegreya";
  final String alegreyaSansF = "AlegreyaSans";
  final String texF = "TeX";
  final String karlaF = "Karla";
  final String latoF = "Lato";
}

class _CountryFlags {
  static final _CountryFlags _singleton = _CountryFlags._internal();

  factory _CountryFlags() {
    return _singleton;
  }
  static final _Base base = _Base();

  _CountryFlags._internal();
  // Font Families
  // as SVG
  final String german = "${base._countryFlags}/german.png";
  final String english = "${base._countryFlags}/english.png";
  final String arabic = "${base._countryFlags}/arabic.png";
}
