class Durations {
  static const Duration animationDurationLong = Duration(milliseconds: 400);
  static const Duration animationDurationShort = Duration(milliseconds: 200);
  static const Duration transitionDuration = Duration(milliseconds: 400);
}
