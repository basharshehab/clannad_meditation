class GetxIds {
  static const String stopwatch = "stopwatch";
  static const String musicPlayer = "music_player";
  static const String maxSongDuration = "max_song_duration";
  static const String shuffle = "shuffle";
  static const String repeat = "repeat";
  static const String play = "play";
  static const String skipBack = "skip_back";
  static const String skipAhead = "skip_ahead";
  static const String audioBar = "audio_bar";
  static const String currnetSongProgressDuration =
      "current_song_progress_duration";
  static const String profileTab = "profile_tab";
  static const String displayedTab = "displayed_tab";
  static const String floatingPlayer = "floating_player";
  static const String selectedFeelingAfter = "selected_feeling_after";
  static const String toggleEditModeButton = "toggle_edit_mode_button";
  // static const String editModeWidgets = "edit_mode_widgets";
  static const String showsEditableUserData = "shows_editable_user_data";
  static const String currentlyListening = "currently_listening";
  static const String courseAdsList = "course_ads_list";
  static const String resetCode = "reset_code";
}
