class Fonts {
  static const String algsansReg =
      "assets/fonts/alegreya_sans/AlegreyaSans-Regular.ttf";
  static const String algsansMed =
      "assets/fonts/alegreya_sans/AlegreyaSans-Medium.ttf";
  static const String algsansLight =
      "assets/fonts/alegreya_sans/AlegreyaSans-Light.ttf";
  static const String algsansBold =
      "assets/fonts/alegreya_sans/AlegreyaSans-Bold.ttf";
  static const String texReg = "assets/fonts/tex/tex-regular.otf";
  static const String algMed = "assets/fonts/alegreya/Alegreya-Medium.ttf";
  static const String latoReg = "assets/fonts/lato/Lato-Regular.ttf";
  static const String karlaVar = "assets/fonts/karla/Karla-Variable.ttf";
}
