import 'package:auto_size_text/auto_size_text.dart';
import 'package:clannad/app/services/layout_helper.dart';
import 'package:flutter/material.dart';

class TextBuilder extends StatelessWidget {
  final double maxWidth;
  final double maxHeight;
  final String text;
  final String fontFamily;
  final int maxLines;
  final FontWeight fontWeight;

  final TextAlign? textAlign;
  final Color textColor;
  const TextBuilder(
      {Key? key,
      required this.maxWidth,
      required this.text,
      required this.fontFamily,
      required this.maxLines,
      required this.fontWeight,
      this.textAlign,
      required this.textColor,
      required this.maxHeight})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LimitedBox(
      maxWidth: maxWidth,
      maxHeight: maxHeight,
      child: AutoSizeText(
        text,
        maxLines: maxLines,
        maxFontSize: 1000,
        textAlign: textAlign ??
            (LayoutHelper.isRTL(context) ? TextAlign.right : TextAlign.left),
        style: TextStyle(
          fontSize: 1000,
          fontFamily: fontFamily,
          fontWeight: fontWeight,
          color: textColor,
        ),
        overflow: TextOverflow.ellipsis,
      ),
    );
  }
}
