import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ClannadLogo extends StatelessWidget {
  final EdgeInsets padding;
  const ClannadLogo({
    Key? key,
    required this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: padding,
      child: SvgPicture.asset(
        Assets.logoArt.logoSvg,
        height: AppSizes.height(context) / 15,
      ),
    );
  }
}
