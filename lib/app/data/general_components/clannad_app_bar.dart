import 'package:cached_network_image/cached_network_image.dart';
import 'package:clannad/app/data/constants/assets.dart';
import 'package:clannad/app/data/constants/app_colors.dart';
import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/constants/durations.dart';
import 'package:clannad/app/data/constants/paddings.dart';
import 'package:clannad/app/modules/home/controllers/home_controller.dart';
import 'package:clannad/app/modules/profile/controllers/profile_controller.dart';
import 'package:clannad/app/modules/splash/controllers/super_controller.dart';
import 'package:clannad/app/routes/app_pages.dart';
import 'package:clannad/app/services/navigate.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../constants/getx_ids.dart';

class ClannadAppBar extends GetView<HomeController> with PreferredSizeWidget {
  final double height;
  // final BuildContext bContext;
  final GlobalKey<ScaffoldState> scaffoldKey;
  const ClannadAppBar(
      {Key? key,
      required this.height,
      // required this.bContext,
      required this.scaffoldKey})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: Paddings.horizontalPadding(context),
      // width: AppSizes.width(context),
      alignment: Alignment.bottomCenter,
      color: AppColors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          IconButton(
            icon: SvgPicture.asset(Assets.appBar.appBarHamburgerSvg,
                color: AppColors.appBarButton),
            onPressed: () => scaffoldKey.currentState!.openDrawer(),
          ),
          SvgPicture.asset(
            Assets.logoArt.logoSvg,
            height: AppSizes.height(context) / 15,
          ),
          GestureDetector(
            onTap: () => Navigate.getTo(Routes.PROFILE, null),
            child: scaffoldKey.currentWidget ==
                    controller.scaffoldKey.currentWidget
                ? Hero(
                    tag: Routes.PROFILE,
                    child: GetBuilder<Super>(
                      id: GetxIds.showsEditableUserData,
                      builder: (_) => Container(
                        height: 35,
                        width: 35,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.contain,
                            image: controller.superController.userModel.imageUrl
                                        .isEmpty ||
                                    controller.superController.userModel
                                        .imageUrl.isEmpty
                                ? AssetImage(
                                    Assets.logoArt.fullArt,
                                  )
                                : CachedNetworkImageProvider(controller
                                    .superController
                                    .userModel
                                    .imageUrl) as ImageProvider,
                          ),
                        ),
                      ),
                    ),
                  )
                : GetBuilder<ProfileController>(
                    id: GetxIds.toggleEditModeButton,
                    builder: (controller) => GestureDetector(
                      onTap: () => controller.toggleEditMode(context, true),
                      child: Container(
                        height: 35,
                        width: 35,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                        ),
                        child: AnimatedSwitcher(
                          duration: Durations.animationDurationShort,
                          child: controller.editMode
                              ? const Icon(
                                  Icons.done,
                                  color: AppColors.appBarButton,
                                  key: ValueKey("edit_mode_on"),
                                )
                              : const Icon(
                                  Icons.edit,
                                  color: AppColors.appBarButton,
                                  key: ValueKey("edit_mode_off"),
                                ),
                        ),
                      ),
                    ),
                  ),
          ),
        ],
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(
        height,
      );
}
