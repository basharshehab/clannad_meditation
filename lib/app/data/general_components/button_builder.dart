import 'package:clannad/app/data/constants/app_sizes.dart';
import 'package:clannad/app/data/general_components/text_builder.dart';
import 'package:flutter/material.dart';

class ButtonBuilder extends StatelessWidget {
  final Color backgroundColor;
  final EdgeInsets padding;
  final double borderRadius;
  final TextBuilder text;
  final Function()? onPressed;
  final Widget? icon;
  final double? width;
  final bool rightToLeft;
  final EdgeInsetsGeometry? iconPadding;
  const ButtonBuilder({
    Key? key,
    required this.backgroundColor,
    required this.padding,
    required this.borderRadius,
    required this.text,
    this.onPressed,
    this.icon,
    this.width,
    this.rightToLeft = false,
    this.iconPadding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ButtonStyle(
        elevation: MaterialStateProperty.all<double>(0.0),
        backgroundColor: MaterialStateProperty.all<Color>(backgroundColor),
        padding: MaterialStateProperty.all(padding),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadius),
          ),
        ),
      ),
      child: icon != null
          ? Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: rightToLeft
                  ? [
                      text,
                      Flexible(
                        child: Container(
                          padding:
                              iconPadding ?? const EdgeInsets.only(left: 10),
                          child: icon!,
                        ),
                      ),
                    ]
                  : [
                      Flexible(
                        child: Container(
                          padding:
                              iconPadding ?? const EdgeInsets.only(left: 10),
                          child: icon!,
                        ),
                      ),
                      text,
                    ],
            )
          : SizedBox(
              width: width ?? AppSizes.width(context),
              child: text,
            ),
    );
  }
}
