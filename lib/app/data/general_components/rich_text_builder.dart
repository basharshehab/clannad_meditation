import 'package:auto_size_text/auto_size_text.dart';
import 'package:clannad/app/services/layout_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RichTextBuilder extends StatelessWidget {
  // final double? width;
  // final double? height;
  final double maxWidth;
  final double maxHeight;
  final List<InlineSpan> texts;
  final String fontFamily;
  final int maxLines;
  final FontWeight fontWeight;

  final TextAlign? textAlign;
  final Color textColor;
  const RichTextBuilder(
      {Key? key,
      // this.width,
      // this.height,
      required this.maxWidth,
      required this.maxHeight,
      required this.texts,
      required this.fontFamily,
      required this.maxLines,
      required this.fontWeight,
      this.textAlign,
      required this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LimitedBox(
      maxWidth: maxWidth,
      maxHeight: maxHeight,
      child: AutoSizeText.rich(
        TextSpan(children: texts),
        maxLines: maxLines,
        maxFontSize: 1000,
        textAlign: textAlign ??
            (LayoutHelper.isRTL(context) ? TextAlign.right : TextAlign.left),
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          fontSize: 1000,
          fontFamily: fontFamily,
          fontWeight: fontWeight,
          color: textColor,
          locale: Get.deviceLocale,
        ),
      ),
    );
  }
}
