// To parse this JSON data, do
//
//     final courseModel = courseModelFromJson(jsonString);

import 'dart:convert';

class CourseModel {
  CourseModel({
    required this.title,
    required this.art,
    required this.url,
    required this.description,
  });

  final String title;
  final String art;
  final String url;
  final String description;

  CourseModel copyWith({
    String? title,
    String? art,
    String? url,
    String? description,
  }) =>
      CourseModel(
        title: title ?? this.title,
        art: art ?? this.art,
        url: url ?? this.url,
        description: description ?? this.description,
      );

  factory CourseModel.fromRawJson(String str) =>
      CourseModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CourseModel.fromJson(Map<String, dynamic> json) => CourseModel(
        title: json["title"],
        art: json["art"],
        url: json["url"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "title": title,
        "art": art,
        "url": url,
        "description": description,
      };
}
