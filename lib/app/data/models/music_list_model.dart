// To parse this JSON data, do
//
//     final musicListModel = musicListModelFromJson(jsonString);

import 'dart:convert';

//   {
//     "title": "Painting Forest",
//     "artist": "Painting with Passion",
//     "duration": "42",
//     "time_unit": "minute",
//     "current_listeners": 59899,
//     "art": "https://via.placeholder.com/100",
//     "id": 29482123,
//     "url": "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3"
//   },

class MusicModel {
  MusicModel({
    required this.title,
    required this.artist,
    required this.duration,
    required this.timeUnit,
    required this.currentListeners,
    required this.art,
    required this.id,
    required this.url,
  });

  final String title;
  final String artist;
  final String duration;
  final String timeUnit;
  final int currentListeners;
  final String art;
  final String id;
  final String url;

  MusicModel copyWith({
    String? title,
    String? artist,
    String? duration,
    String? timeUnit,
    int? currentListeners,
    String? art,
    String? id,
    String? url,
  }) =>
      MusicModel(
        title: title ?? this.title,
        artist: artist ?? this.artist,
        duration: duration ?? this.duration,
        timeUnit: timeUnit ?? this.timeUnit,
        currentListeners: currentListeners ?? this.currentListeners,
        art: art ?? this.art,
        id: id ?? this.id,
        url: url ?? this.url,
      );

  factory MusicModel.fromRawJson(String str) =>
      MusicModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory MusicModel.fromJson(Map<String, dynamic> json) => MusicModel(
        title: json["title"],
        artist: json["artist"],
        duration: json["duration"],
        timeUnit: json["time_unit"],
        currentListeners: json["current_listeners"],
        art: json["art"],
        id: json["id"],
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "title": title,
        "artist": artist,
        "duration": duration,
        "time_unit": timeUnit,
        "current_listeners": currentListeners,
        "art": art,
        "id": id,
        "url": url,
      };
}
