// To parse this JSON data, do
//
//     final userModel = userModelFromJson(jsonString);

import 'dart:convert';

class UserModel {
  UserModel({
    required this.name,
    required this.emailAddress,
    required this.profileQuote,
    required this.imageUrl,
    required this.feelingsHistoryBefore,
    required this.feelingsHistoryAfter,
    required this.meditationHistory,
    required this.currentStreak,
    required this.maxStreak,
    required this.createdAt,
    required this.lastStreakUpdateTimestamp,
  });

  final String name;
  final String emailAddress;
  final String imageUrl;
  final String? profileQuote;
  final Map<String, Map<String, Map<String, FeelingsHistory>>>
      feelingsHistoryBefore;
  final Map<String, Map<String, Map<String, FeelingsHistory>>>
      feelingsHistoryAfter;
  final Map<String,
          Map<String, Map<String, Map<String, MeditationHistoryDetails>>>>
      meditationHistory;
  // meditationHistory;
  final int currentStreak;
  final int maxStreak;

  final String createdAt;
  final String lastStreakUpdateTimestamp;
  factory UserModel.fromRawJson(String str) =>
      UserModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        name: json["name"],
        emailAddress: json["email_address"],
        profileQuote: json["profile_quote"],
        imageUrl: json["image_url"],
        feelingsHistoryBefore: Map.from(json["feelings_history_before"]).map(
            (k, v) =>
                MapEntry<String, Map<String, Map<String, FeelingsHistory>>>(
                    k,
                    Map.from(v).map((k, v) =>
                        MapEntry<String, Map<String, FeelingsHistory>>(
                            k,
                            Map.from(v).map(
                                (k, v) => MapEntry<String, FeelingsHistory>(
                                    k, FeelingsHistory.fromJson(v))))))),
        feelingsHistoryAfter: Map.from(json["feelings_history_after"]).map((k,
                v) =>
            MapEntry<String, Map<String, Map<String, FeelingsHistory>>>(
                k,
                Map.from(v).map((k, v) =>
                    MapEntry<String, Map<String, FeelingsHistory>>(
                        k,
                        Map.from(v).map((k, v) =>
                            MapEntry<String, FeelingsHistory>(
                                k, FeelingsHistory.fromJson(v))))))),
        meditationHistory: Map.from(json["meditation_history"]).map((k, v) =>
            MapEntry<String, Map<String, Map<String, Map<String, MeditationHistoryDetails>>>>(
                k,
                Map.from(v).map((k, v) => MapEntry<String, Map<String, Map<String, MeditationHistoryDetails>>>(
                    k,
                    Map.from(v).map((k, v) =>
                        MapEntry<String, Map<String, MeditationHistoryDetails>>(
                            k, Map.from(v).map((k, v) => MapEntry<String, MeditationHistoryDetails>(k, MeditationHistoryDetails.fromJson(v))))))))),
        currentStreak: json["current_streak"] ?? 0,
        maxStreak: json["max_streak"] ?? 0,
        createdAt: json["created_at"],
        lastStreakUpdateTimestamp: json["last_streak_update"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "email_address": emailAddress,
        "profile_quote": profileQuote,
        "image_url": imageUrl,
        "feelings_history_before": Map.from(feelingsHistoryBefore).map((k, v) =>
            MapEntry<String, dynamic>(
                k,
                Map.from(v).map((k, v) => MapEntry<String, dynamic>(
                    k,
                    Map.from(v).map(
                        (k, v) => MapEntry<String, dynamic>(k, v.toJson())))))),
        "feelings_history_after": Map.from(feelingsHistoryAfter).map((k, v) =>
            MapEntry<String, dynamic>(
                k,
                Map.from(v).map((k, v) => MapEntry<String, dynamic>(
                    k,
                    Map.from(v).map(
                        (k, v) => MapEntry<String, dynamic>(k, v.toJson())))))),
        "meditation_history": Map.from(meditationHistory).map((k, v) =>
            MapEntry<String, dynamic>(
                k,
                Map.from(v).map((k, v) => MapEntry<String, dynamic>(
                    k,
                    Map.from(v).map((k, v) => MapEntry<String, dynamic>(
                        k,
                        Map.from(v).map((k, v) =>
                            MapEntry<String, dynamic>(k, v.toJson())))))))),
        "current_streak": currentStreak,
        "max_streak": maxStreak,
        "created_at": createdAt,
        "last_streak_update": lastStreakUpdateTimestamp,
      };

  UserModel copyWith({
    String? name,
    String? emailAddress,
    String? profileQuote,
    String? imageUrl,
    Map<String, Map<String, Map<String, FeelingsHistory>>>?
        feelingsHistoryBefore,
    Map<String, Map<String, Map<String, FeelingsHistory>>>?
        feelingsHistoryAfter,
    Map<String,
            Map<String, Map<String, Map<String, MeditationHistoryDetails>>>>?
        meditationHistory,
    int? currentStreak,
    int? maxStreak,
    String? createdAt,
    String? lastStreakUpdateTimestamp,
  }) {
    return UserModel(
      name: name ?? this.name,
      emailAddress: emailAddress ?? this.emailAddress,
      profileQuote: profileQuote ?? this.profileQuote,
      imageUrl: imageUrl ?? this.imageUrl,
      feelingsHistoryBefore:
          feelingsHistoryBefore ?? this.feelingsHistoryBefore,
      feelingsHistoryAfter: feelingsHistoryAfter ?? this.feelingsHistoryAfter,
      meditationHistory: meditationHistory ?? this.meditationHistory,
      currentStreak: currentStreak ?? this.currentStreak,
      maxStreak: maxStreak ?? this.maxStreak,
      createdAt: createdAt ?? this.createdAt,
      lastStreakUpdateTimestamp:
          lastStreakUpdateTimestamp ?? this.lastStreakUpdateTimestamp,
    );
  }
}

class FeelingsHistory {
  FeelingsHistory({
    required this.feeling,
  });

  final String? feeling;

  factory FeelingsHistory.fromRawJson(String str) =>
      FeelingsHistory.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory FeelingsHistory.fromJson(Map<String, dynamic> json) =>
      FeelingsHistory(
        feeling: json["feeling"],
      );

  Map<String, dynamic> toJson() => {
        "feeling": feeling,
      };
}

class MeditationHistoryDetails {
  MeditationHistoryDetails({
    required this.fromTimestamp,
    required this.toTimestamp,
    // required this.beforeFeeling,
    required this.feeling,
  });

  final String fromTimestamp;
  final String toTimestamp;
  // final String beforeFeeling;
  final String? feeling;

  factory MeditationHistoryDetails.fromRawJson(String str) =>
      MeditationHistoryDetails.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory MeditationHistoryDetails.fromJson(Map<String, dynamic> json) =>
      MeditationHistoryDetails(
        fromTimestamp: json["from_timestamp"],
        toTimestamp: json["to_timestamp"],
        // beforeFeeling: json["before_feeling"],
        feeling: json["feeling"],
        // afterFeeling: json["after_feeling"],
      );

  Map<String, dynamic> toJson() => {
        "from_timestamp": fromTimestamp,
        "to_timestamp": toTimestamp,
        "feeling": feeling,
        // "before_feeling": beforeFeeling,
        // "after_feeling": afterFeeling,
      };
}
