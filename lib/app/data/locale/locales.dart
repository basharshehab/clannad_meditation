import 'package:clannad/app/data/locale/locale_keys.dart';

class Locales {
  static const Map<String, String> en = {
    // Signin, Signup, and Splash screens
    LocaleKeys.signin: 'Sign In',
    LocaleKeys.name: 'Name',
    LocaleKeys.signinSubtitle:
        'Sign in now to access your excercises and saved music.',
    LocaleKeys.noAccount: 'Don\'t have an account?',
    LocaleKeys.signup: 'Sign Up',
    LocaleKeys.signupSubtitle: 'Sign up now for free and start meditating.',
    LocaleKeys.byUsingApp: 'By using this app you agree to our',
    LocaleKeys.privacyPolicy: 'Privacy Policy',
    LocaleKeys.forgotPassword: 'Forgot Password?',
    LocaleKeys.emailAddress: 'Email Address',
    LocaleKeys.password: 'Password',
    LocaleKeys.profileQuote: 'Profile Quote',
    LocaleKeys.alreadyAccount: 'Already have an account?',
    LocaleKeys.orSignInWith: 'Or sign in with',

    // Recovery
    LocaleKeys.recoverAccount: 'Account Recovery',
    LocaleKeys.recoverAccountSubtitle:
        'Enter your email address and we will send you an email to reset your password.',
    LocaleKeys.sendRecoveryEmail: 'Send me an Email',
    LocaleKeys.weSentResetEmail: 'We have sent you a recovery email',
    LocaleKeys.followInstructionsEmail:
        "Follow the instructions in the email to reset your password",

    // LocaleKeys.verificationCode: 'Verification code',
    // LocaleKeys.verifyCode: 'Verify code',
    // LocaleKeys.enterNewPassword:
    //     'The verification code has been successfully verified! Enter your new Password here',
    // LocaleKeys.confirmReset: 'Reset password',
    // LocaleKeys.passwordResetDone: 'Your password has been reset',

    // Welcome screen
    LocaleKeys.welcomeBack: "Welcome back",
    LocaleKeys.howDoYouFeel: "How are you feeling today?",
    LocaleKeys.youSaid: "You said you're feeling",
    LocaleKeys.feelingAnxious: "Anxious",
    LocaleKeys.feelingResponseAnxious:
        "That't okay, let's meditate and you will feel better.",
    LocaleKeys.feelingOkay: "Okay",
    LocaleKeys.feelingResponseOkay: "That makes me feel glad.",
    LocaleKeys.feelingFocused: "Focused",
    LocaleKeys.feelingResponseFocused:
        "A focused mind is what your spirit needs.",
    LocaleKeys.feelingRelaxed: "Relaxed",
    LocaleKeys.feelingResponseRelaxed: "A cup of Jasmine tea would be perfect.",
    LocaleKeys.iWonderWhy: "I wonder why..",

    LocaleKeys.watchNow: "Watch now",
    LocaleKeys.couldntOpenCourse: "Can't open this course at the moment",

    // Music screen
    LocaleKeys.relaxMeditate: "Relax & Meditate",
    LocaleKeys.relaxMeditateSubtitle:
        "Sometimes the most productive thing you can do is relax.",
    LocaleKeys.startListening: "Start listening",
    LocaleKeys.listening: "Listening",
    LocaleKeys.seconds: "Seconds",
    LocaleKeys.minutes: "Minutes",
    LocaleKeys.hours: "Hours",
    LocaleKeys.second: "Second",
    LocaleKeys.minute: "Minute",
    LocaleKeys.hour: "Hour",
    // Meditate Screen
    LocaleKeys.meditation: "Meditation",
    LocaleKeys.meditationSubtitle:
        "Start the music playlist, and begin your meditation here.",
    LocaleKeys.startNow: "Start Now",
    LocaleKeys.stop: "Stop",

    // Profile Screen
    LocaleKeys.stats: "Stats",
    LocaleKeys.achievements: "Achievements",
    LocaleKeys.camera: "Camera",
    LocaleKeys.gallery: "Gallery",
    LocaleKeys.editProfileQuoteHint:
        "Edit your profile and add an inspiring quote!",
    LocaleKeys.bestStreak: "Best Streak",
    LocaleKeys.currentStreak: "Current Streak",

    // Stats
    LocaleKeys.monday: "Mon",
    LocaleKeys.tuesday: "Tue",
    LocaleKeys.wednesday: "Wed",
    LocaleKeys.thursday: "Thu",
    LocaleKeys.friday: "Fri",
    LocaleKeys.saturday: "Sat",
    LocaleKeys.sunday: "Sun",

    // Drawer
    LocaleKeys.settings: "Settings",
    LocaleKeys.donate: "Donate",
    LocaleKeys.logout: "Logout",

    // Settings
    LocaleKeys.deleteAccount: "Delete Account",
    LocaleKeys.goBack: "Go Back",
    LocaleKeys.appLanguage: "App Language",

    // Snackbars
    LocaleKeys.restartAppFullChanges:
        "You may need to restart Clannad for the changes to take full effect.",
    LocaleKeys.languageAlreadySet: "Clannad is already using this language",

    // Unknown
    LocaleKeys.unknown: "Unknown",
    LocaleKeys.unknownError: "Unknown error. Maybe try again later?",

    // After Meditation Screen
    LocaleKeys.howDoYouFeelAfter: "How are you feeling now?",
    LocaleKeys.youMeditatedForWithInARow: "You meditated for",
    LocaleKeys.youMeditatedFor: "You meditated for",
    LocaleKeys.daysInARow: "days in a row!",

    // Other
    LocaleKeys.and: "and",

    /// Validator messages
    /// // Sign in
    LocaleKeys.couldntSignIn: "Couldn't sign in",
    LocaleKeys.invalidEmail: "The email address you provided is invalid.",
    LocaleKeys.userDisabled: "This user has been disabled.",
    LocaleKeys.userNotFound: "This user does not exist.",
    LocaleKeys.wrongPassword: "Wrong password.",

    LocaleKeys.pleaseEnterEmailAddress: "Please enter your email address.",
    LocaleKeys.enterName: "Enter your name",
    LocaleKeys.pleaseEnterPassword: "Please enter your password.",

    /// // Sign up
    LocaleKeys.couldntSignUp: "Couldn't sign up",
    LocaleKeys.emailAlreadyInUse:
        "The provided email address is already in use.",
    LocaleKeys.operationNotAllowed:
        "The developer has made an oopsie, operation not allowed!",
    LocaleKeys.weakPassword: "Your password is too weak!",
    LocaleKeys.invalidUsername: "This username is invalid.",
    // LocaleKeys.invalidEmailAddress: "Invalid email address",
    LocaleKeys.invalidProfileQuote: "Try typing something inspirational.",
    LocaleKeys.signUpSuccessful: "Signed up and ready!",
    LocaleKeys.youCanSignIn: "You can sign in now.",

    /// // WelcomeView
    LocaleKeys.couldntSyncFeelings:
        "Couldn't sync your feelings with the cloud",
    LocaleKeys.tryAgainLater: "Try again later.",
    LocaleKeys.couldntLogout: "Couldn't log out",

    /// // Profile View
    LocaleKeys.couldntUploadImage: "Couldn't upload your image",
    LocaleKeys.failedToSaveChanges: "Changes could not be saved",

    /// // Internet Connection Check
    LocaleKeys.noConnection: "No Connection",
    LocaleKeys.checkInternetConnection: "Check your internet connection.",

    /// // Settings
    LocaleKeys.cantDeleteAccount: "Can't delete the account at the moment",
  };

  static const de = {
    // Signin, Signup, and Splash screens

    LocaleKeys.signin: 'Einloggen',
    LocaleKeys.name: 'Name',
    LocaleKeys.signinSubtitle:
        'Logge dich jetzt ein, um auf deine Übungen und gespeicherte Musik zuzugreifen.',
    LocaleKeys.signup: 'Anmelden',
    LocaleKeys.signupSubtitle:
        'Melde dich jetzt kostenlos an und beginn zu meditieren.',
    LocaleKeys.noAccount: 'Hast du noch kein Konto?',
    LocaleKeys.byUsingApp:
        'Durch die Nutzung dieser App erklären Sie sich mit unserer',
    LocaleKeys.privacyPolicy: 'Datenschutzbestimmungen',
    LocaleKeys.forgotPassword: 'Passwort vergessen?',
    LocaleKeys.emailAddress: 'Email Adresse',
    LocaleKeys.password: 'Passwort',
    LocaleKeys.profileQuote: 'Aufhänger',
    LocaleKeys.alreadyAccount: 'Hast du schon ein Konto?',
    LocaleKeys.orSignInWith: 'Oder mit',

    // Recovery
    LocaleKeys.recoverAccount: 'Konto Wiederherstellung',
    LocaleKeys.recoverAccountSubtitle:
        'Gib deine Email Adresse ein und wir schicken dir eine Email, um deine Passwort zurückzusetzen.',
    LocaleKeys.sendRecoveryEmail: 'Schick mir eine Email',

    LocaleKeys.weSentResetEmail:
        'Wir haben dir eine Wiederherstellungs-E-Mail geschickt.',
    LocaleKeys.followInstructionsEmail:
        "Folge die Hinweise in der E-Mail, um dein Passwort zurückzusetzen",
    // LocaleKeys.verificationCode: 'Verifizierungscode',
    // LocaleKeys.verifyCode: 'Code bestätigen',
    // LocaleKeys.enterNewPassword:
    //     'Code erfolgreich bestätigt! Gib dein neues Passwort hier ein',
    // LocaleKeys.confirmReset: 'Passwort bestätigen',
    // LocaleKeys.passwordResetDone: 'Dein Passwort wurde zurückgesetzt',

    // Welcome screen
    LocaleKeys.welcomeBack: "Willkommen zurück",
    LocaleKeys.howDoYouFeel: "Wie fühlst du dich heute?",
    LocaleKeys.youSaid: "Du sagtest, du fühlst dich",
    LocaleKeys.feelingAnxious: "Unruhig",
    LocaleKeys.feelingResponseAnxious:
        "Das ist okay. Lass uns meditieren und du wirst dich besser fühlen.",
    LocaleKeys.feelingOkay: "Okay",
    LocaleKeys.feelingResponseOkay: "Das freut mich total.",
    LocaleKeys.feelingFocused: "Fokussiert",
    LocaleKeys.feelingResponseFocused:
        "Ein fokussierter Geist ist das, was deine Seele braucht.",
    LocaleKeys.feelingRelaxed: "Entpannt",
    LocaleKeys.feelingResponseRelaxed: "Eine Tasse Jasmintee wäre perfekt.",
    LocaleKeys.iWonderWhy: "Ich wundere mich warum..",
    LocaleKeys.watchNow: "Schau jetzt an",
    LocaleKeys.couldntOpenCourse:
        "Dieser Kurs kann derzeit nicht geöffnet werden",

    // Music screen
    LocaleKeys.relaxMeditate: "Relaxieren & Meditatieren",
    LocaleKeys.relaxMeditateSubtitle:
        "Manchmal ist das Produktivste, was man tun kann, um sich zu entspannen.",
    LocaleKeys.startListening: "Jetzt zuhören",
    LocaleKeys.listening: "hören zu",
    LocaleKeys.seconds: "Sekunden",
    LocaleKeys.minutes: "Minuten",
    LocaleKeys.hours: "Stunden",
    LocaleKeys.second: "Sekunde",
    LocaleKeys.minute: "Minute",
    LocaleKeys.hour: "Stunde",

    // Meditate Screen
    LocaleKeys.meditation: "Meditation",
    LocaleKeys.meditationSubtitle:
        "Schalt die Musik an, und beginn mit deiner Meditation.",
    LocaleKeys.startNow: "Jetzt anfangen",
    LocaleKeys.stop: "Stop",

    // Profile Screen
    LocaleKeys.stats: "Stats",
    LocaleKeys.achievements: "Leistungen",
    LocaleKeys.camera: "Kamera",
    LocaleKeys.gallery: "Galerie",
    LocaleKeys.editProfileQuoteHint:
        "Bearbeite dein Profil und füge etwas Inspirierendes hinzu!",
    LocaleKeys.bestStreak: "Beste Serie",
    LocaleKeys.currentStreak: "Aktuelle Serie",

    // Stats
    LocaleKeys.monday: "Mon",
    LocaleKeys.tuesday: "Die",
    LocaleKeys.wednesday: "Mit",
    LocaleKeys.thursday: "Don",
    LocaleKeys.friday: "Fri",
    LocaleKeys.saturday: "Sam",
    LocaleKeys.sunday: "Son",

    // Drawer
    LocaleKeys.settings: "Einstellungen",
    LocaleKeys.donate: "Spenden",
    LocaleKeys.logout: "Ausloggen",

    // Settings
    LocaleKeys.deleteAccount: "Konto löschen",
    LocaleKeys.goBack: "Zurück",

    LocaleKeys.appLanguage: "App-Sprache",

    // Snackbars
    LocaleKeys.restartAppFullChanges:
        "Möglicherweise müsst du Clannad neu starten, damit die Änderungen vollständig wirksam werden.",
    LocaleKeys.languageAlreadySet: "Clannad benutzt schon diese Sprache",

    // Unknown
    LocaleKeys.unknown: "Unbekannt",

    // After Meditation Screen
    LocaleKeys.howDoYouFeelAfter: "Wie fühlst du dich jetzt?",
    LocaleKeys.youMeditatedForWithInARow: "Du hast",
    LocaleKeys.youMeditatedFor: "Du hast so lange meditiert",
    LocaleKeys.daysInARow: "Tage hintereinander meditiert!",

    // Other
    LocaleKeys.and: "und",

    /// Validator messages
    /// // Sign in
    LocaleKeys.couldntSignIn: "Konnte nicht einloggen",
    LocaleKeys.invalidEmail: "Die angegebene E-Mail-Adresse ist ungültig.",
    LocaleKeys.userDisabled: "Dieser Benutzer wurde deaktiviert.",
    LocaleKeys.userNotFound: "Dieser Benutzer konnte nicht gefunden werden.",
    LocaleKeys.wrongPassword: "Falshes Passwort",
    LocaleKeys.unknownError:
        "Unbekannter Fehler. Vielleicht später noch einmal versuchen?",

    LocaleKeys.pleaseEnterEmailAddress: "Bitte gib deine Email-Addresse ein",
    LocaleKeys.enterName: "Bitte gib deinen Namen ein",
    LocaleKeys.pleaseEnterPassword: "Bitte gib dein Passwort ein.",

    /// // Sign up
    LocaleKeys.couldntSignUp: "Konnte nicht anmelden",
    LocaleKeys.emailAlreadyInUse:
        "Die angegebene E-Mail-Addresse ist bereits in Gebrauch.",
    LocaleKeys.operationNotAllowed:
        "Dem Entwickler ist ein Fehler unterlaufen, Operation nicht erlaubt!",
    LocaleKeys.weakPassword: "Dein Passwort ist zu schwach!",
    LocaleKeys.invalidUsername: "Dieser Benutzername ist ungültig.",
    // LocaleKeys.invalidEmailAddress: "Invalid email address",
    LocaleKeys.invalidProfileQuote:
        "Versuche, etwas Inspirierendes zu schreiben.",
    LocaleKeys.signUpSuccessful: "Angemeldet und bereit!",
    LocaleKeys.youCanSignIn: "Du kannst nun einloggen.",

    /// // WelcomeView
    LocaleKeys.couldntSyncFeelings:
        "Konnte deine Gefühle nicht mit der Wolke synchronisieren",
    LocaleKeys.tryAgainLater: "Versuche es später noch einmal.",
    LocaleKeys.couldntLogout: "Konnte nicht ausloggen",

    /// // Profile View
    LocaleKeys.couldntUploadImage: "Konnte den Bild nicht hochladen",
    LocaleKeys.failedToSaveChanges:
        "Die Änderungen konnten nicht gespeichert werden",

    /// // Internet Connection Check
    LocaleKeys.noConnection: "Keine Verbindung",
    LocaleKeys.checkInternetConnection: "Überprüfe deine Internetverbindung.",

    /// // Settings
    LocaleKeys.cantDeleteAccount: "Konto kann derzeit nicht gelöscht werden",
  };

  static const ar = {
    // Signin, Signup, and Splash screens

    LocaleKeys.forgotPassword: 'نسيت كلمة المرور؟',
    LocaleKeys.howDoYouFeel: "كيف تشعر اليوم؟",
    LocaleKeys.signin: 'سجل الدخول',
    LocaleKeys.name: 'الأسم',
    LocaleKeys.signinSubtitle:
        'سجل الدخول الآن لترى التمارين و الموسيقى الخاصة بك.',
    LocaleKeys.noAccount: 'ليس لديك حساب؟',
    LocaleKeys.signup: 'انضم لنا',
    LocaleKeys.signupSubtitle: 'انشئ حساباً الآن مجاناً و ابدأ بالتأمل!',
    LocaleKeys.byUsingApp: 'من خلال استخدامك للتطبيق فإنك توافق على',
    LocaleKeys.privacyPolicy: 'سياسة الخصوصية',
    LocaleKeys.emailAddress: 'البريد الإلكتروني',
    LocaleKeys.password: 'كلمة المرور',
    LocaleKeys.profileQuote: 'شارتك الشخصية',
    LocaleKeys.alreadyAccount: 'لديك حساب؟',

    // Recovery
    LocaleKeys.sendRecoveryEmail: 'أرسل لي بريد الاسترجاع',
    LocaleKeys.recoverAccount: 'استرجاع الحساب',
    LocaleKeys.recoverAccountSubtitle:
        'أعطنا بريدك الإلكتروني و سوف نرسل لك بريداً لإعادة إنشاء كلمة مرورك',
    LocaleKeys.weSentResetEmail: 'لقد أرسلنا اليك بريداً الكترونياً',
    LocaleKeys.followInstructionsEmail:
        "اتبع التعليمات المُرسلة لإعادة تعيين كلمة المرور",
    // LocaleKeys.verificationCode: 'رمز الإسترجاع',
    // LocaleKeys.verifyCode: 'تحقق من الرمز',
    // LocaleKeys.enterNewPassword:
    //     'تم التحقق من الرمز بنجاح! أدخل كلمة مرورك الجديدة',
    // LocaleKeys.confirmReset: 'استرجع الحساب',
    // LocaleKeys.passwordResetDone: 'تم إعادة تعيين كلمة المرور',

    // Welcome screen
    LocaleKeys.welcomeBack: "مرحباً بعودتك",
    LocaleKeys.youSaid: "لقد قلت أنك تشعر ب",
    LocaleKeys.feelingAnxious: "القلق",
    LocaleKeys.feelingResponseAnxious:
        "لا بأس. هيا نتأمل و سوف تشعر بحالة أفضل.",
    LocaleKeys.feelingOkay: "حالة جيدة",
    LocaleKeys.feelingResponseOkay: "هذا يشعرني بالسرور.",
    LocaleKeys.feelingFocused: "التركيز",
    LocaleKeys.feelingResponseFocused: "عقل مركّز هو ما تحتاجه الروح.",
    LocaleKeys.feelingRelaxed: "الراحة",
    LocaleKeys.feelingResponseRelaxed: "كأس من شاي الياسمين سيكون رائعاً الآن.",
    LocaleKeys.iWonderWhy: "أتسائل لماذا..",
    LocaleKeys.watchNow: "شاهد الآن",
    LocaleKeys.couldntOpenCourse: "لا يمكن مشاهدة هذه الدورة حالياً",

    // Music screen
    LocaleKeys.relaxMeditate: "استرخي و تأمل",
    LocaleKeys.relaxMeditateSubtitle: "احيانا تكون الراحة هي أكثر الأفعال جدوة",
    LocaleKeys.startListening: "استمع الآن",
    LocaleKeys.listening: "مستمع",
    LocaleKeys.seconds: "ثواني",
    LocaleKeys.minutes: "دقائق",
    LocaleKeys.hours: "ساعات",
    LocaleKeys.second: "ثانية",
    LocaleKeys.minute: "دقيقة",
    LocaleKeys.hour: "ساعة",

    // Meditate Screen
    LocaleKeys.meditation: "التأمل",
    LocaleKeys.meditationSubtitle: "ابدأ الموسيقى و باشر بالتأمل",
    LocaleKeys.startNow: "أبدأ الآن",
    LocaleKeys.stop: "توقف",

    // Profile Screen
    LocaleKeys.stats: "الإحصائيات",
    LocaleKeys.achievements: "الإنجازات",
    LocaleKeys.camera: "الكاميرا",
    LocaleKeys.gallery: "الاستديو",
    LocaleKeys.bestStreak: "أطول حماسة",
    LocaleKeys.currentStreak: "حماستك الحالية",

    // Stats
    LocaleKeys.monday: "Mon",
    LocaleKeys.tuesday: "Tue",
    LocaleKeys.wednesday: "Wed",
    LocaleKeys.thursday: "Thu",
    LocaleKeys.friday: "Fri",
    LocaleKeys.saturday: "Sat",
    LocaleKeys.sunday: "Sun",

    // Drawer
    LocaleKeys.settings: "الإعدادات",
    LocaleKeys.donate: "تبرع",
    LocaleKeys.logout: "تسجيل الخروج",

    // Settings
    LocaleKeys.deleteAccount: "امسح الحساب",
    LocaleKeys.goBack: "العودة",
    LocaleKeys.appLanguage: "لغة التطبيق",

    // Snackbars
    LocaleKeys.restartAppFullChanges:
        "بعض التغييرات قد تحتاج لإعادة تشغيل التطبيق.",
    LocaleKeys.languageAlreadySet: "هذه اللغة مطبقة بالفعل.",
    LocaleKeys.orSignInWith: 'أو عن طريق',

    // Unknown
    LocaleKeys.unknown: "لا أعلم",
    LocaleKeys.unknownError: "خطأ غير معروف. حاول لاحقاً مرة اخرى.",

    // After Meditation Screen
    LocaleKeys.howDoYouFeelAfter: "كيف تشعر الآن؟",
    LocaleKeys.youMeditatedForWithInARow: "لقد تأملت لمدة",
    LocaleKeys.youMeditatedFor: "لقد تأملت لمدة",

    LocaleKeys.daysInARow: "أيام على التوالي!",

    // Other
    LocaleKeys.and: "و",

    /// Validator messages
    /// // Sign in
    LocaleKeys.couldntSignIn: "لا يمكن تسجيل الدخول",
    LocaleKeys.invalidEmail: "هذا البريد الإلكتروني غير صالح.",
    LocaleKeys.userDisabled: "لقد تم تعطيل هذا المستخدم.",
    LocaleKeys.userNotFound: "هذا المسنخدم غير موجود.",
    LocaleKeys.wrongPassword: "كلمة المرور غلط.",

    LocaleKeys.pleaseEnterEmailAddress: "رجاء أدخل بريدك الإلكتروني.",
    LocaleKeys.pleaseEnterPassword: "رجاء أدخل كلمة المرور.",
    LocaleKeys.enterName: "أدخل اسمك",

    /// // Sign up
    LocaleKeys.couldntSignUp: "لا يمكن الإنضمام",
    LocaleKeys.emailAlreadyInUse: "هذا البريد الإلكتروني يتم استخدامه بالفعل.",
    LocaleKeys.operationNotAllowed: "خطأ من المطور. عملية غير مسموحة!",
    LocaleKeys.weakPassword: "كلمة مرورك ضعيفة!",
    LocaleKeys.invalidUsername: "هذا الإسم غير صالح.",
    // LocaleKeys.invalidEmailAddress: "Invalid email address",
    LocaleKeys.invalidProfileQuote: "حاول كتابة شيء مشجع.",
    LocaleKeys.signUpSuccessful: "تم الإنضمام بنجاح.",
    LocaleKeys.youCanSignIn: "يمكنك تسجيل الدخول الآن.",

    /// // WelcomeView
    LocaleKeys.couldntSyncFeelings: "لم نتمكن من مزامنة مشاعرك مع السحابة",
    LocaleKeys.tryAgainLater: "عاود المحاولة لاحقاً.",
    LocaleKeys.couldntLogout: "لم نتمكن من تسجيل الخروج",

    /// // Profile View
    LocaleKeys.couldntUploadImage: "لم نتمكن من رفع الصورة",
    LocaleKeys.failedToSaveChanges: "لم نتمكن من حفظ التغييرات",

    /// // Internet Connection Check
    LocaleKeys.noConnection: "لا إتصال",
    LocaleKeys.checkInternetConnection: "تحقق من اتصالك بالإنترنت.",

    /// // Settings
    LocaleKeys.cantDeleteAccount: "لا يمكن حذف الحساب حالياً",
  };
}
