import 'package:clannad/app/data/locale/locales.dart';
import 'package:get/get_navigation/get_navigation.dart';

class AppLocale extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en_US': Locales.en,
        'de_DE': Locales.de,
        "ar_SA": Locales.ar,
      };
}
