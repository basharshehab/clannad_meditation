class LocaleKeys {
  // Sign in, Signup, and Splash screens
  static const String splashTitle = 'splash_title';
  static const String splashSubtitle = 'splash_subtitle';
  static const String signin = 'signin';
  static const String signinSubtitle = 'signin_subtitle';
  static const String signup = 'signup';
  static const String signupSubtitle = 'signup_subtitle';
  static const String noAccount = 'no_account';
  static const String alreadyAccount = 'already_account';
  static const String byUsingApp = 'by_using_app';
  static const String privacyPolicy = 'privacy_policy';
  static const String forgotPassword = 'forgot_password';
  static const String emailAddress = 'email_address';
  static const String password = 'password';
  static const String name = 'name';
  static const String profileQuote = 'profile_quote';
  static const String orSignInWith = 'or_sign_in_with';

  // Recovery
  static const String recoverAccount = 'recover_account';
  static const String recoverAccountSubtitle = 'recover_account_subtitle';
  static const String sendRecoveryEmail = 'send_recovery_email';
  static const String weSentResetEmail = 'we_sent_reset_email';
  static const String followInstructionsEmail =
      'follow_instructions_to_reset_password';

  // static const String verificationCode = 'verification_code';
  // static const String verifyCode = 'verify_code';
  // static const String enterNewPassword = 'enter_new_password';
  // static const String confirmReset = 'confirm_reset';
  // static const String passwordResetDone = 'password_reset_done';

  // Welcome screen
  static const String welcomeBack = "welcome_back";
  static const String howDoYouFeel = "how_do_you_feel";
  static const String youSaid = "you_said";
  static const String feelingOkay = "feeling_okay";
  static const String feelingFocused = "feeling_focused";
  static const String feelingAnxious = "feeling_anxious";
  static const String feelingRelaxed = "feeling_relaxed";
  static const String feelingResponseOkay = "feeling_response_okay";
  static const String feelingResponseFocused = "feeling_response_foused";
  static const String feelingResponseAnxious = "feeling_response_anxious";
  static const String feelingResponseRelaxed = "feeling_response_relaxed";
  static const String iWonderWhy = "i_wonder_why";
  static const String watchNow = "watch_now";
  static const String couldntOpenCourse = "couldnt_launch_url";

  // Music Screen
  static const String relaxMeditate = "relax_meditate";
  static const String relaxMeditateSubtitle = "relax_meditate_subtitle";
  static const String startListening = "start_listening";
  static const String listening = "listening";
  static const String minutes = "minutes";
  static const String hours = "hours";
  static const String seconds = "seconds";
  static const String minute = "minute";
  static const String hour = "hour";
  static const String second = "second";

  // Meditation Screen
  static const String meditation = "meditation";
  static const String meditationSubtitle = "meditation_subtitle";
  static const String startNow = "start_now";
  static const String stop = "stop";

  // Profile Screen
  static const String stats = "stats";
  static const String achievements = "achievements";
  static const String gallery = "gallery";
  static const String camera = "camera";
  static const String editProfileQuoteHint = "edit_profile_quote_hint";
  static const String bestStreak = "best_streak";
  static const String currentStreak = "current_streak";
  static const String failedToSaveChanges = "failed_to_save_changes";

  // Stats // Days and Months
  static const String monday = "monday";
  static const String tuesday = "tuesday";
  static const String wednesday = "wednesday";
  static const String thursday = "thursday";
  static const String friday = "friday";
  static const String saturday = "saturday";
  static const String sunday = "sunday";

  static const String january = "january";
  static const String february = "february";
  static const String march = "march";
  static const String april = "april";
  static const String may = "may";
  static const String june = "june";
  static const String july = "july";
  static const String august = "august";
  static const String september = "september";
  static const String october = "october";
  static const String november = "november";
  static const String december = "december";
  // Drawer
  static const String donate = "donate";
  static const String settings = "settings";
  static const String logout = "logout";

  // Settings
  static const String appLanguage = "app_language";
  static const String deleteAccount = "delete_account";
  static const String goBack = "go_back";

  // Snackbars
  static const String languageAlreadySet = "language_already_set";
  static const String restartAppFullChanges = "restart_app_full_changes";

  // Unknown
  static const String unknown = "unknown";
  static const String unknownError = "unknown_error";

  // After Meditation Screen
  static const String howDoYouFeelAfter = "how_do_you_feel_after";
  static const String youMeditatedForWithInARow = "you_meditated_for_in_a_row";
  static const String youMeditatedFor = "you_meditated_for";

  static const String daysInARow = "days_in_a_row";

  // Other
  static const String and = "and";

  /// Validator messages
  /// // Sign in
  static const String couldntSignIn = "couldnt_sign_in";
  static const String invalidEmail = "invalid_email";
  static const String userDisabled = "user_disabled";
  static const String userNotFound = "user_not_found";
  static const String wrongPassword = "wrong_password";

  static const String pleaseEnterEmailAddress = "please_enter_email_address";
  static const String enterName = "please_enter_name";
  static const String pleaseEnterPassword = "please_enter_password";

  /// // Sign up
  static const String couldntSignUp = "couldnt_sign_ip";
  static const String emailAlreadyInUse = "email_already_in_use";
  static const String operationNotAllowed = "operation_not_allowed";
  static const String weakPassword = "weak_password";
  static const String invalidUsername = "invalid_username";
  static const String invalidProfileQuote = "invalid_profile_quote";
  static const String signUpSuccessful = "sign_up_successful";
  static const String youCanSignIn = "you_can_sign_in";

  /// // WelcomeView
  static const String couldntSyncFeelings = "couldnt_sync_feelings";
  static const String tryAgainLater = "try_again_later";
  static const String couldntLogout = "couldnt_log_out";

  /// // Profile Edit
  static const String couldntUploadImage = "couldnt_upload_image";

  /// // Settings
  static const String cantDeleteAccount = "couldnt_delete_account";

  /// // Internet Connection Check
  static const String checkInternetConnection = "check_intenret_connection";
  static const String noConnection = "no_connection";
}
