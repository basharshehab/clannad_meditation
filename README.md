# Clannad: Meditate and keep at it.

This small Flutter project is the first of many more to come. Clannad is a Meditaion app, built to be simple and helps to encourage users to Meditate daily using features like streak count, achievements, pattern-tracking, and more.


![Logo](https://drive.google.com/uc?export=download&id=1j40KX83c07Nw1IL5LItj3irlm8qwjSNM)



## Authors

- [@Bashar (That's me!)](https://gitlab.com/basharshehab)


## How to install

To build and install this app on your phone manually, run

```bash
  flutter pub get
```
### - Android - Install from Releases
Simply download the latest release and install it. 
### - Android - Build it yourself

```bash
  flutter build apk
```
An APK file will be generated (path will be displayed in the console). Simply move the APK file to your phone and install it.

### - IOS
Apple makes this way too complicated, too much for me to bother.


## Features

- Arabic, English, and German languages are fully supported!
- Curated meditation playlists selected by hand from YouTube.
- Curated meditation music.
- Timer to track how long you meditate
- Daily feelings tracking
- Streak tracking
- Achievements! (Todo) 
- Pattern-tracking / Statistics (Todo) 


## Tech Stack

**Client:** Flutter, using GetX for State Management, Localization, and Navigation. 

**Server:** Firebase Auth, Firebase Firestore, Firebase Storage


## Screenshots

![App Screenshot](https://drive.google.com/uc?export=download&id=1swgJf3GoKf8Qwk6cqcBX4MDvGA9izla_)
![App Screenshot](https://drive.google.com/uc?export=download&id=1qyQaf3zYJFInlpLypD5ummF3vpOsJwr2)
![App Screenshot](https://drive.google.com/uc?export=download&id=1VZ_HrilQ34x7GYEJ2c3CrVAg4Y51QBRU)
![App Screenshot](https://drive.google.com/uc?export=download&id=1VRXabfwsl7k-5r7pjw-p-0B2r8LSPgz9)
![App Screenshot](https://drive.google.com/uc?export=download&id=11k5eGJWULmQ7AjPW4FqcUsm0_ScecLdr)
![App Screenshot](https://drive.google.com/uc?export=download&id=12zO9ulVVMrtIxD6bIj-VvbrQVpb0J4hp)


## TODO
- Achievements
- Pattern-tracking / Statistics
- iOS configuration and testing.
## Support Me
If you'd like to support me, you can always [buy me a coffee](https://www.buymeacoffee.com/basharSH)
## License

[MIT](https://choosealicense.com/licenses/mit/)

MIT License

Copyright (c) [2022] [Bashar Shihab Eedin]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.